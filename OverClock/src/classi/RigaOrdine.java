package classi;

public class RigaOrdine {

    private Dispositivo dispositivo;
    private int quantita = 1;
    private float subTotale = 0;

    public RigaOrdine(Dispositivo d) {
        this.dispositivo = d;
        this.subTotale = d.getPrezzoAcquisto() * quantita;
    }

    public void setQuantita(int q) {
        this.quantita = q;
        this.subTotale = dispositivo.getPrezzoAcquisto() * this.quantita;
    }

    public Dispositivo getDispositivo() {
        return this.dispositivo;
    }

    public int getQuantita() {
        return quantita;
    }

    public float getSubTotale() {
        return this.subTotale;
    }
}
