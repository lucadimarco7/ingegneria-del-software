package gui;

import classi.CartaFedeltaDAO;
import classi.Cliente;
import classi.Dispositivo;
import classi.Dispositivo.tipoArticolo;
import classi.DispositivoDAO;
import classi.OverClock;
import classi.SchedaAcquistoDAO;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

public class InserisciAcquistoUsato extends javax.swing.JPanel {

    private OverClock overClock;
    private Boolean erroriCliente = true;
    private Boolean erroriDispositivo = true;
    private Cliente c = null;
    private Dispositivo d = null;
    private int punti = 0;
    private SchedaAcquistoDAO schedaAcquistoDAO = new SchedaAcquistoDAO();
    private CartaFedeltaDAO cartaFedeltaDAO = new CartaFedeltaDAO();
    private DispositivoDAO dispositivoDAO = new DispositivoDAO();
    private OperazioneRiuscita frameOperazioneRiuscita;

    public InserisciAcquistoUsato(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dispositivo = new javax.swing.JTextField();
        pulsanteConferma = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cliente = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        gradoDanni = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        prezzoProposto = new javax.swing.JTextField();
        calcolaPunti = new javax.swing.JButton();

        dispositivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dispositivoActionPerformed(evt);
            }
        });

        pulsanteConferma.setText("Conferma");
        pulsanteConferma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteConfermaActionPerformed(evt);
            }
        });

        jLabel4.setForeground(new java.awt.Color(255, 0, 0));
        jLabel4.setText("");
        jLabel4.setVisible(false);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Nuovo Acquisto Usato");

        jLabel2.setText("E-Mail o Numero Cliente");

        cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clienteActionPerformed(evt);
            }
        });

        jLabel3.setText("Codice Modello o Nome Dispositivo");

        jLabel5.setText("Grado Danneggiamento");

        gradoDanni.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Come Nuovo", "Danni Leggeri", "Danni Evidenti" }));
        gradoDanni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gradoDanniActionPerformed(evt);
            }
        });

        jLabel6.setText("Punti Proposti");

        prezzoProposto.setEditable(false);

        calcolaPunti.setText("Calcola Punti");
        calcolaPunti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calcolaPuntiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(prezzoProposto, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(calcolaPunti)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pulsanteConferma))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(gradoDanni, 0, 357, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dispositivo)
                            .addComponent(cliente))))
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(dispositivo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(gradoDanni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prezzoProposto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pulsanteConferma)
                    .addComponent(calcolaPunti))
                .addContainerGap(22, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void pulsanteConfermaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteConfermaActionPerformed
        if (overClock.getClienteMail(cliente.getText()) != null
                && overClock.getClienteMail(cliente.getText()).getCartaFedelta() != null) {
            c = overClock.getClienteMail(cliente.getText());
            cliente.setBackground(new java.awt.Color(204, 255, 153));
            erroriCliente = false;
        } else {
            c = null;
            cliente.setBackground(new java.awt.Color(255, 153, 102));
            jLabel4.setText("Il Cliente non è registrato o non possiede una Carta Fedeltà");
            erroriCliente = true;
        }
        if (overClock.getDispositivo(dispositivo.getText()) != null
                && overClock.getDispositivo(dispositivo.getText()).getTipo() != tipoArticolo.pezzo) {
            d = overClock.getDispositivo(dispositivo.getText());
            dispositivo.setBackground(new java.awt.Color(204, 255, 153));
            float moltiplicatore = overClock.getMoltiplicatore(overClock.getDanni(String.valueOf(gradoDanni.getSelectedItem())));
            punti = (int) Math.floor(d.getPrezzo() * moltiplicatore);
            prezzoProposto.setText(String.valueOf(punti));
            jLabel4.setVisible(false);
            erroriDispositivo = false;
        } else {
            d = null;
            dispositivo.setBackground(new java.awt.Color(255, 153, 102));
            jLabel4.setText("Dispositivo non presente nel catalogo");
            erroriDispositivo = true;
        }
        if (c == null && d == null) {
            jLabel4.setText("Riempire i campi correttamente");
        }
        if (!erroriDispositivo && !erroriCliente) {
            try {

                overClock.aggiornaSaldo(String.valueOf(c.getCartaFedelta().getID()), punti);

            } catch (IOException ex) {
                Logger.getLogger(InserisciAcquistoUsato.class.getName()).log(Level.SEVERE, null, ex);
            }
            cartaFedeltaDAO.update(c.getCartaFedelta());
            schedaAcquistoDAO.add(c, d, overClock.getDanni(String.valueOf(gradoDanni.getSelectedItem())));
            Dispositivo cerca = overClock.getDispositivo(d.getCodice() + "-" + overClock.getDanni(String.valueOf(gradoDanni.getSelectedItem())));
            if (cerca != null) {
                dispositivoDAO.update(d.getNome() + " - " + overClock.getDanni(String.valueOf(gradoDanni.getSelectedItem())), cerca.getQuantita() + 1);
            } else {
                dispositivoDAO.addUsato(d, overClock.getDanni(String.valueOf(gradoDanni.getSelectedItem())));
            }
            schedaAcquistoDAO.findAll();
            cliente.setBackground(java.awt.Color.white);
            dispositivo.setBackground(java.awt.Color.white);
            frameOperazioneRiuscita = new OperazioneRiuscita();
            Timer timer;
            timer = new Timer(1000, (ActionEvent e) -> {
                frameOperazioneRiuscita.dispose();
            });
            timer.setRepeats(false);
            timer.start();
            frameOperazioneRiuscita.setVisible(true);
            frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
            cliente.setText("");
            dispositivo.setText("");
            cliente.setBackground(java.awt.Color.white);
            dispositivo.setBackground(java.awt.Color.white);
            jLabel4.setVisible(false);
            prezzoProposto.setText("0");
            c = null;
            d = null;
        } else {
            System.out.println(jLabel4.getText());
            jLabel4.setVisible(true);
        }
    }//GEN-LAST:event_pulsanteConfermaActionPerformed

    private void gradoDanniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gradoDanniActionPerformed
        if (overClock.getDispositivo(dispositivo.getText()) != null
                && overClock.getDispositivo(dispositivo.getText()).getTipo() != tipoArticolo.pezzo) {
            d = overClock.getDispositivo(dispositivo.getText());
            dispositivo.setBackground(new java.awt.Color(204, 255, 153));
            float moltiplicatore = overClock.getMoltiplicatore(overClock.getDanni(String.valueOf(gradoDanni.getSelectedItem())));
            punti = (int) Math.floor(d.getPrezzo() * moltiplicatore);
            prezzoProposto.setText(String.valueOf(punti));
            erroriDispositivo = false;
            jLabel4.setVisible(false);
        } else {
            d = null;
            dispositivo.setBackground(new java.awt.Color(255, 153, 102));
            jLabel4.setText("Dispositivo non presente nel catalogo");
            jLabel4.setVisible(true);
            erroriDispositivo = true;
        }
    }//GEN-LAST:event_gradoDanniActionPerformed

    private void calcolaPuntiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calcolaPuntiActionPerformed
        if (overClock.getDispositivo(dispositivo.getText()) != null
                && overClock.getDispositivo(dispositivo.getText()).getTipo() != tipoArticolo.pezzo) {
            d = overClock.getDispositivo(dispositivo.getText());
            dispositivo.setBackground(new java.awt.Color(204, 255, 153));
            float moltiplicatore = overClock.getMoltiplicatore(overClock.getDanni(String.valueOf(gradoDanni.getSelectedItem())));
            punti = (int) Math.floor(d.getPrezzo() * moltiplicatore);
            prezzoProposto.setText(String.valueOf(punti));
            erroriDispositivo = false;
            jLabel4.setVisible(false);
        } else {
            d = null;
            dispositivo.setBackground(new java.awt.Color(255, 153, 102));
            jLabel4.setText("Dispositivo non presente nel catalogo");
            jLabel4.setVisible(true);
            erroriDispositivo = true;
        }
    }//GEN-LAST:event_calcolaPuntiActionPerformed

    private void clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_clienteActionPerformed

    private void dispositivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dispositivoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dispositivoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton calcolaPunti;
    private javax.swing.JTextField cliente;
    private javax.swing.JTextField dispositivo;
    private javax.swing.JComboBox<String> gradoDanni;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JTextField prezzoProposto;
    private javax.swing.JButton pulsanteConferma;
    // End of variables declaration//GEN-END:variables
}
