package tests.classi;

import classi.Distributore;
import org.junit.Test;
import static org.junit.Assert.*;

public class DistributoreTest {

    Distributore distributore = new Distributore(0, "apple", "apple@gmail.it");

    public DistributoreTest() {
    }

    @Test
    public void testGetId() {
        int expResult = 0;
        int result = distributore.getId();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetId() {
        int expResult = 50;
        distributore.setId(50);
        int Result = distributore.getId();
        assertEquals(expResult, Result);
    }

    @Test
    public void testGetNome() {
        String expResult = "apple";
        String result = distributore.getNome();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetEmail() {
        String expResult = "apple@gmail.it";
        String Result = distributore.getEmail();
        assertEquals(expResult, Result);

    }

    @Test
    public void setNome() {
        String expResult = "samsung";
        distributore.setNome("samsung");
        String Result = distributore.getNome();
        assertEquals(expResult, Result);
    }

    @Test
    public void setEmail() {
        String expResult = "samsung@gmail.it";
        distributore.setEmail("samsung@gmail.it");
        String Result = distributore.getEmail();
        assertEquals(expResult, Result);
    }

}
