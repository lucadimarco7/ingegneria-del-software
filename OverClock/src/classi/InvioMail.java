package classi;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class InvioMail {

    public static void inviaMail(String oggetto, String messaggio, String destinatario) {

        // E-Mail Destinatario
        String to = destinatario;

        // E-Mail Mittente
        String from = "provamailjava10@gmail.com";

        // E-mail smtp
        String host = "smtp.gmail.com";

        // Get proprietà di sistema
        Properties properties = System.getProperties();

        // Setup E-Mail
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.ssl.enable", "true");
        properties.put("mail.smtp.auth", "true");

        // Get Object Sessione // username and password
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication("provamailjava10@gmail.com", "provamailjava");

            }

        });

        // Usato per problemi smtp
        session.setDebug(true);

        try {
            // crea un default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // set campo "da" dell'header.
            message.setFrom(new InternetAddress(from));

            // set campo "per" dell'header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // set campo oggetto
            message.setSubject(oggetto);

            // set messaggio
            message.setText(messaggio);

            System.out.println("invio in corso...");
            // invio messaggio
            Transport.send(message);
            System.out.println("Messaggio inviato correttamente....");
        } catch (MessagingException mex) {
        }

    }

}
