package tests.classi;

import classi.CartaFedelta;
import classi.Catalogo;
import classi.Cliente;
import classi.Dispositivo;
import classi.Distributore;
import classi.OverClock;
import classi.Prenotazione;
import classi.Promozione;
import classi.RigaDiVendita;
import classi.SchedaAcquisto;
import classi.SchedaDiVendita;
import classi.SchedaOrdine;
import classi.SchedaRiparazione;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class OverClockTest {

    CartaFedelta cf = new CartaFedelta(10, 100);
    CartaFedelta cf2 = new CartaFedelta(1, 100);
    Catalogo catalogo = new Catalogo();
    Cliente c = new Cliente("nome", "cognome", "domanismetto93@gmail.com", "3807662947", cf);
    Cliente c2 = new Cliente("nome2", "cognome2", "domanismetto@gmail.com", "3807662948", null);
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Distributore distributore = new Distributore(0, "apple", "apple@mail.it");
    Distributore distributore2 = new Distributore(1, "samsung", "samsung@mail.it");
    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    Dispositivo d2 = new Dispositivo("Iphone 12", "IPHONE12", "apple", (float) 799.99, 400, 100, tipo, distributore, true);
    Dispositivo d3 = new Dispositivo("Iphone 13 - danniLeggeri", "IPHONE13", "apple", (float) 899.99, 600, 100, tipo, distributore, true);
    Dispositivo d4 = new Dispositivo("Iphone 10 - comeNuovo", "IPHONE10", "apple", (float) 399.99, 300, 100, tipo, distributore, true);
    private List<Dispositivo> ld = new ArrayList();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    String data = dtf.format(now);
    Promozione p = new Promozione(0, d, 10, data);
    List<Promozione> lp = new ArrayList();
    List<Prenotazione> lprenotazioni = new ArrayList();
    Prenotazione.statoPrenotazione stato = Prenotazione.statoPrenotazione.ordinato;
    Prenotazione pr = new Prenotazione(0, c, d, data, stato);
    List<SchedaDiVendita> lv = new ArrayList();
    SchedaDiVendita sdv;
    SchedaDiVendita sdv2;
    List<Cliente> lc = new ArrayList();
    List<Distributore> ldis = new ArrayList();
    List<CartaFedelta> lcfe = new ArrayList();
    List<RigaDiVendita> lr1 = new ArrayList();
    List<RigaDiVendita> lr2 = new ArrayList();
    RigaDiVendita rgv = new RigaDiVendita(d);
    RigaDiVendita rgv2 = new RigaDiVendita(d2);
    List<SchedaAcquisto> la = new ArrayList();
    SchedaAcquisto sa = new SchedaAcquisto(0, c, d, data);
    SchedaAcquisto sa2 = new SchedaAcquisto(1, c, d, data);
    SchedaAcquisto.Danni danni = SchedaAcquisto.Danni.comeNuovo;
    SchedaRiparazione.statoRiparazioni stato1 = SchedaRiparazione.statoRiparazioni.riparazioneConclusa;
    SchedaRiparazione sr = new SchedaRiparazione(0, c, d, stato1, 100, data);
    SchedaRiparazione.statoRiparazioni stato2 = SchedaRiparazione.statoRiparazioni.preventivoInCorso;
    SchedaRiparazione sr2 = new SchedaRiparazione(1, c2, d, stato2, 90, data);
    List<SchedaRiparazione> lr = new ArrayList();
    OverClock oc = new OverClock();

    public OverClockTest() {
        lc.add(c);
        lc.add(c2);
        lcfe.add(cf);
        lcfe.add(cf2);
        oc.setListaCarte(lcfe);
        ld.add(d);
        ld.add(d2);
        ld.add(d3);
        ld.add(d4);
        lp.add(p);
        lr.add(sr);
        lr.add(sr2);
        ldis.add(distributore);
        ldis.add(distributore2);
        oc.setListaDistributori(ldis);
        catalogo.setListaDispositivi(ld);
        oc.setRiparazioni(lr);
        oc.setPromozioni(lp);
        lprenotazioni.add(pr);
        oc.setPrenotazioni(lprenotazioni);
        sdv = new SchedaDiVendita(0, rgv.getSubTotale(), data);
        sdv2 = new SchedaDiVendita(1, rgv2.getSubTotale(), data);
        lr1.add(rgv);
        lr2.add(rgv2);
        sdv.setListaRigheDiVendita(lr1);
        sdv2.setListaRigheDiVendita(lr2);
        lv.add(sdv);
        lv.add(sdv2);
        oc.setVendite(lv);
        la.add(sa);
        la.add(sa2);
        oc.setAcquisti(la);
        oc.setListaClienti(lc);

    }

    @Test
    public void testSetGetPromozioni() {
        Promozione expResult = p;
        assertEquals(expResult, oc.getPromozione(0));
    }

    @Test
    public void testVisualizzaPromozioni() {
        String ricerca = "";
        List<Promozione> expResult = lp;
        List<Promozione> result = oc.visualizzaPromozioni(ricerca);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPrenotazione() {
        int id = 0;
        Prenotazione expResult = pr;
        Prenotazione result = oc.getPrenotazione(id);
        assertEquals(expResult, result);
    }

    @Test
    public void testVisualizzaPrenotazioni() {
        String ricerca = "";
        List<Prenotazione> expResult = lprenotazioni;
        List<Prenotazione> result = oc.visualizzaPrenotazioni(ricerca);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPrenotazioneString() {
        String expResult = "Ordinato";
        String result = oc.getPrenotazioneString(stato.ordinato);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetStatoPrenotazione() {
        String s = "Ordinato";
        Prenotazione.statoPrenotazione expResult = stato.ordinato;
        Prenotazione.statoPrenotazione result = oc.getStatoPrenotazione(s);
        assertEquals(expResult, result);
    }

    @Test
    public void testVisualizzaVendite() {
        String ricerca = "";
        List<SchedaDiVendita> expResult = lv;
        List<SchedaDiVendita> result = oc.visualizzaVendite(ricerca);
        assertEquals(expResult, result);
    }

    @Test
    public void testVisualizzaAcquisti() {
        String ricerca = "";
        List<SchedaAcquisto> expResult = la;
        List<SchedaAcquisto> result = oc.visualizzaAcquisti(ricerca);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDanni() {
        String s = "Come Nuovo";
        SchedaAcquisto.Danni expResult = danni;
        SchedaAcquisto.Danni result = oc.getDanni(s);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDanniFormattati() {
        String s = "Usati - Come Nuovi";
        String expResult = "comeNuovo";
        String result = oc.getDanniFormattati(s);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetMoltiplicatore() {
        SchedaAcquisto.Danni d = danni;
        OverClock instance = new OverClock();
        float expResult = 0.8f;
        float result = instance.getMoltiplicatore(d);
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetCliente() {
        System.out.println("getCliente");
        String numero = "3807662947";
        Cliente expResult = c;
        Cliente result = oc.getCliente(numero);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetClienteCarta() {
        int id = 10;
        Cliente expResult = c;
        Cliente result = oc.getClienteCarta(id);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetClienteMail() {
        String mail = "domanismetto93@gmail.com";
        Cliente expResult = c;
        Cliente result = oc.getClienteMail(mail);
        assertEquals(expResult, result);
    }

    @Test
    public void testVisualizzaClienti() {
        String ricerca = "";
        List<Cliente> expResult = lc;
        List<Cliente> result = oc.visualizzaClienti(ricerca);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetStato() {
        String s = "riparazioneConclusa";
        SchedaRiparazione.statoRiparazioni expResult = stato1;
        SchedaRiparazione.statoRiparazioni result = oc.getStato(s);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetStatoStringa() {
        SchedaRiparazione.statoRiparazioni s = stato1;
        String expResult = "riparazioneConclusa";
        String result = oc.getStatoStringa(s);
        assertEquals(expResult, result);
    }

    @Test
    public void testFormattaStato() {
        String stato = "riparazioneConclusa";
        String expResult = "Riparazione Conclusa";
        String result = oc.formattaStato(stato);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetRiparazioni_String() {
        String email = "domanismetto93@gmail.com";
        SchedaRiparazione expResult = sr;
        List<SchedaRiparazione> result = oc.getRiparazioni(email);
        assertEquals(expResult, result.get(0));
    }

    @Test
    public void testGetSchedaRiparazione() {
        int id = 0;
        SchedaRiparazione expResult = sr;
        SchedaRiparazione result = oc.getSchedaRiparazione(id);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetRiparazioni_0args() {
        List<SchedaRiparazione> expResult = lr;
        List<SchedaRiparazione> result = oc.getRiparazioni();
        assertEquals(expResult, result);
    }

    @Test
    public void testRicercaRiparazione() {
        String ricerca = "";
        OverClock instance = new OverClock();
        List<SchedaRiparazione> expResult = lr;
        List<SchedaRiparazione> result = oc.RicercaRiparazione(ricerca);
        assertEquals(expResult, result);
    }

    @Test
    public void testFindIdDistributore() {
        int expResult = 2;
        int result = oc.findIdDistributore();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetListaDistributori() {
        List<Distributore> expResult = ldis;
        List<Distributore> result = oc.getListaDistributori();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDistributore() {
        int id = 0;
        Distributore expResult = distributore;
        Distributore result = oc.getDistributore(id);

    }

    @Test
    public void testRicercaDistributori() {
        String ricerca = "";
        List<Distributore> expResult = ldis;
        List<Distributore> result = oc.RicercaDistributori(ricerca);
        assertEquals(expResult, result);

    }

    @Test
    public void testAggiornaSaldo() throws Exception {
        String id = "0";
        int expResult = 110;
        int punti = 10;
        oc.aggiornaSaldo(id, punti);
        assertEquals(expResult, cf.getSaldoPunti());
    }

    @Test
    public void testGetListaCarteFedelta() {
        oc.setListaCarte(lcfe);
        List<CartaFedelta> expResult = lcfe;
        List<CartaFedelta> result = oc.getListaCarteFedelta();
        assertEquals(expResult, result);
    }

    @Test
    public void testRicercaCarta() {
        oc.setListaCarte(lcfe);
        String ricerca = "0";
        CartaFedelta expResult = cf;
        List<CartaFedelta> result = oc.ricercaCarta(ricerca);
        assertEquals(expResult, result.get(0));

    }

    @Test
    public void testFindID() {
        oc.setListaCarte(lcfe);
        int expResult = 1;
        int result = oc.findID();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCartaFedelta() {
        oc.setListaCarte(lcfe);
        int idCarta = 0;
        CartaFedelta expResult = cf;
        CartaFedelta result = oc.getCartaFedelta(idCarta);
        assertEquals(expResult, result);
    }
}
