package classi;

import java.sql.Connection;
import java.sql.SQLException;

public interface InterfacciaDAO {

    public void findAll();

    public Connection getConnection() throws SQLException;
}
