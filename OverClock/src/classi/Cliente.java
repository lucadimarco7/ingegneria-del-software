package classi;

public class Cliente {

    private String nome;
    private String cognome;
    private String email;
    private String numeroTelefono;
    private CartaFedelta cartaFedelta = null;

    public Cliente(String nome, String cognome, String email, String numeroTelefono, CartaFedelta carta) {
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.numeroTelefono = numeroTelefono;
        this.cartaFedelta = carta;
    }

    public Cliente(String nome, String cognome, String email, String numeroTelefono) {
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.numeroTelefono = numeroTelefono;
        this.cartaFedelta = null;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumero() {
        return numeroTelefono;
    }

    public void setNumero(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public CartaFedelta getCartaFedelta() {
        return cartaFedelta;
    }

    public void setCartaFedelta(CartaFedelta cartaFedelta) {
        this.cartaFedelta = cartaFedelta;
    }
}
