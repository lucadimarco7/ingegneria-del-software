package tests.classi;

import classi.CartaFedelta;
import classi.Cliente;
import classi.Dispositivo;
import classi.Distributore;
import classi.RigaDiVendita;
import classi.SchedaDiVendita;
import classi.SchedaRiparazione;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class SchedaRiparazioneTest {

    Cliente c = new Cliente("nome", "cognome", "email@mail.it", "3807662947", null);
    Distributore distributore = new Distributore(0, "antonino", "domanismetto93@gmail.com");
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    String data = dtf.format(now);
    SchedaRiparazione.statoRiparazioni stato1 = SchedaRiparazione.statoRiparazioni.riparazioneConclusa;
    SchedaRiparazione sr = new SchedaRiparazione(0, c, d, stato1, 100, data);
    SchedaRiparazione.statoRiparazioni stato2 = SchedaRiparazione.statoRiparazioni.preventivoInCorso;
    SchedaRiparazione sr2 = new SchedaRiparazione(1, c, d, stato2, 90, data);
    SchedaRiparazione.statoRiparazioni stato3 = SchedaRiparazione.statoRiparazioni.preventivoConcluso;
    SchedaRiparazione sr3 = new SchedaRiparazione(2, c, d, stato3, 80, data);
    SchedaRiparazione.statoRiparazioni stato4 = SchedaRiparazione.statoRiparazioni.riparazioneInCorso;
    SchedaRiparazione sr4 = new SchedaRiparazione(3, c, d, stato4, 70, data);
    private List<Dispositivo> pezzi1 = new ArrayList();
    private List<Dispositivo> pezzi2 = new ArrayList();
    private List<Dispositivo> pezzi3 = new ArrayList();
    private List<Dispositivo> pezzi4 = new ArrayList();
    Dispositivo.tipoArticolo tipo2 = Dispositivo.tipoArticolo.pezzo;
    Dispositivo d1 = new Dispositivo("Pezzo ", "Pezzo1", "apple", (float) 10.00, 8, 100, tipo2, distributore, true);
    Dispositivo d2 = new Dispositivo("Pezzo", "pezzo2", "apple", (float) 100.00, 8, 100, tipo2, distributore, true);
    Dispositivo d3 = new Dispositivo("Pezzo", "pezzo3", "apple", (float) 10.00, 8, 100, tipo2, distributore, true);
    Dispositivo d4 = new Dispositivo("Pezzo", "pezzo4", "apple", (float) 100.00, 8, 100, tipo2, distributore, true);

    public SchedaRiparazioneTest() {
        pezzi1.add(d1);
        pezzi2.add(d2);
        pezzi3.add(d3);
        pezzi4.add(d4);
        sr.setPezzi(pezzi1);
        sr2.setPezzi(pezzi2);
        sr3.setPezzi(pezzi3);
        sr4.setPezzi(pezzi4);
    }

    @Test
    public void testGetCliente() {
        Cliente expResult = c;
        Cliente result = sr.getCliente();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDispositivo() {
        Dispositivo expResult = d;
        Dispositivo result = sr.getDispositivo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetStato() {

        SchedaRiparazione.statoRiparazioni expResult = stato1;
        SchedaRiparazione.statoRiparazioni expResult2 = stato2;
        SchedaRiparazione.statoRiparazioni expResult3 = stato3;
        SchedaRiparazione.statoRiparazioni expResult4 = stato4;
        SchedaRiparazione.statoRiparazioni result = sr.getStato();
        SchedaRiparazione.statoRiparazioni result2 = sr2.getStato();
        SchedaRiparazione.statoRiparazioni result3 = sr3.getStato();
        SchedaRiparazione.statoRiparazioni result4 = sr4.getStato();
        assertEquals(expResult, result);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
    }

    @Test
    public void testNotificaCliente() {
        System.out.println("notificaCliente");
        String oggetto = "Test Riparazione";
        String messaggio = "Messaggio di testo riparazione";
        String Destinatatio = "domanismetto93@gmail.com";
        sr.notificaCliente(oggetto, messaggio, Destinatatio);
    }

    @Test
    public void testAggiungiPezzo() {
        sr.aggiungiPezzo(d2);
        List<Dispositivo> expResult = new ArrayList();
        expResult.add(d1);
        expResult.add(d2);
        assertEquals(expResult, sr.getPezzi());

    }

    @Test
    public void testSetGetPezzi() {

        sr.setPezzi(pezzi2);
        assertEquals(pezzi2, sr.getPezzi());
    }

    @Test
    public void testGetID() {
        int expResult = 0;
        int result = sr.getID();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCosto() {
        float expResult = 100.0F;
        float result = sr.getCosto();
        assertEquals(expResult, result, 0.0);
    }

}
