/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package tests.classi;

import classi.Catalogo;
import classi.Dispositivo;
import classi.Distributore;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class CatalogoTest {

    Catalogo catalogo = new Catalogo();
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Distributore distributore = new Distributore(0, "apple", "apple@gmail.it");

    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    Dispositivo d2 = new Dispositivo("Iphone 12", "IPHONE12", "apple", (float) 799.99, 500, 100, tipo, distributore, true);
    Dispositivo d3 = new Dispositivo("Iphone 13 - danniLeggeri", "IPHONE13", "apple", (float) 899.99, 0, 100, tipo, distributore, true);
    Dispositivo d4 = new Dispositivo("Iphone 10 - comeNuovo", "IPHONE10", "apple", (float) 399.99, 0, 100, tipo, distributore, true);
    private List<Dispositivo> ld = new ArrayList();

    public CatalogoTest() {
        ld.add(d);
        ld.add(d2);
        ld.add(d3);
        ld.add(d4);
        catalogo.setListaDispositivi(ld);
    }

    @Test
    public void testSetListaDispositivi() {
        List<Dispositivo> expResult = ld;
        catalogo.setListaDispositivi(ld);
        assertEquals(expResult, catalogo.getListaDispositivi(""));
    }

    @Test
    public void testGetDispositivo() {
        String codice = "IPHONE11";
        Dispositivo expResult = d;
        Dispositivo result = catalogo.getDispositivo(codice);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetListaDispositivi() {
        List<Dispositivo> expResult = ld;
        String ricerca = "";
        List<Dispositivo> result = catalogo.getListaDispositivi(ricerca);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetListaDispositiviUsati() {
        String ricerca = "Iphone 10 - comeNuovo";
        String ricerca2 = "Iphone 13 - danniLeggeri";
        List<Dispositivo> expResult = new ArrayList();
        List<Dispositivo> expResult2 = new ArrayList();
        expResult2.add(d3);
        expResult.add(d4);
        List<Dispositivo> result = catalogo.getListaDispositiviUsati(ricerca);;
        List<Dispositivo> result2 = catalogo.getListaDispositiviUsati(ricerca2);
        assertEquals(expResult, result);
        assertEquals(expResult2, result2);
    }

    @Test
    public void testGetDispositivoByDistributore() {
        System.out.println("getDispositivoByDistributore");
        List<Dispositivo> expResult = ld;
        List<Dispositivo> result = catalogo.getDispositivoByDistributore(0);
        assertEquals(expResult, result);
    }

}
