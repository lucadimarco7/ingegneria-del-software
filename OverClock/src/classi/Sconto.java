/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package classi;

/**
 *
 * @author Luca
 */
public abstract class Sconto {

    Sconto sconto = null;

    public void setSuccessivo(Sconto sconto) {
        this.sconto = sconto;
    }

    public abstract float check(float spesa);

}
