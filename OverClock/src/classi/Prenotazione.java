package classi;

import java.util.ArrayList;
import java.util.List;

public class Prenotazione implements Observer {

    private int id;
    private Cliente c;
    private Dispositivo d;
    private String data;

    public enum statoPrenotazione {
        ordinato,
        inAttesaDiRitiro,
        ritirato
    }
    private statoPrenotazione s;
    private PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();

    public Prenotazione(int id, Cliente c, Dispositivo d, String data, statoPrenotazione s) {
        this.id = id;
        this.c = c;
        this.d = d;
        this.data = data;
        this.s = s;
    }

    public int getID() {
        return this.id;
    }

    public Cliente getCliente() {
        return this.c;
    }

    public Dispositivo getDispositivo() {
        return this.d;
    }

    public String getData() {
        return this.data;
    }

    public statoPrenotazione getStato() {
        return this.s;
    }

    public void setStato(statoPrenotazione s) {
        this.s = s;
    }

    @Override
    public void update() {
        if (d.getQuantita() > 0) {
            d.removeObserver(this);
            this.s = statoPrenotazione.inAttesaDiRitiro;
            InvioMail.inviaMail("Articolo Ordinato", "Il tuo articolo: " + d.getNome() + " è disponibile in negozio", this.c.getEmail());
            d.setQuantita(d.getQuantita() - 1);
            prenotazioneDAO.update(this, "In Attesa di Ritiro");
        }
    }
}
