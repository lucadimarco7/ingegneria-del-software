package tests.classi;

import classi.Dispositivo;
import classi.Distributore;
import classi.RigaOrdine;
import classi.SchedaOrdine;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class SchedaOrdineTest {

    Distributore distributore = new Distributore(0, "apple", "domanismetto93@gmail.com");
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    Dispositivo d2 = new Dispositivo("Iphone 12", "IPHONE12", "apple", (float) 899.99, 500, 100, tipo, distributore, true);
    RigaOrdine rdo = new RigaOrdine(d);
    RigaOrdine rdo2 = new RigaOrdine(d2);
    SchedaOrdine sdo = new SchedaOrdine();
    List<RigaOrdine> righeOrdine = new ArrayList();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    String data = dtf.format(now);

    public SchedaOrdineTest() {
        righeOrdine.add(rdo);
        righeOrdine.add(rdo2);
        sdo.setRigheOrdine(righeOrdine);
        sdo.setData(data);
    }

    @Test
    public void testAggiungiOrdine() {;
        int expResult = 4;
        sdo.aggiungiOrdine(d, 3);
        //ci aspettiamo che aggiunga quantita all'ordine perció 3+1
        assertEquals(expResult, righeOrdine.get(0).getQuantita());
    }

    @Test
    public void testRimuoviOrdine() {
        int expResult = 1;
        sdo.rimuoviOrdine(d, 1);
        //Una rdo verra' eliminata per cui la dimensione della lista rdo da 2 diverra' 1
        assertEquals(expResult, sdo.getRigheOrdine().size());
    }

    @Test
    public void testRimuoviRigaOrdine() {
        int expResult = 1;
        sdo.rimuoviRigaOrdine(rdo);
        //stesso discorso precedente
        assertEquals(expResult, sdo.getRigheOrdine().size());
    }

    @Test
    public void testInviaOrdine() {
        sdo.inviaOrdine();
    }

    @Test
    public void testGetRigheOrdine() {
        List<RigaOrdine> expResult = righeOrdine;
        List<RigaOrdine> result = sdo.getRigheOrdine();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetOrdine() {

        RigaOrdine expResult = rdo;
        RigaOrdine result = sdo.getOrdine(d);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetID() {
        int expResult = 0;
        int result = sdo.getID();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetData() {
        String expResult = data;
        String result = sdo.getData();
        assertEquals(expResult, result);

    }

}
