package classi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SchedaOrdineDAO implements InterfacciaDAO {

    private OverClock overClock = OverClock.getIstanza();
    private String data;
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = Connessione.getInstance().getConnection();
        return conn;
    }

    public void add(SchedaOrdine s) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        this.data = dtf.format(now);
        int id = -1;
        for (int i = 0; i < overClock.getIstanzaOrdini().size(); i++) {
            if (i != overClock.getIstanzaOrdini().get(i).getID()) {
                id = i;
                break;
            }
        }
        if (id == -1) {
            id = overClock.getIstanzaOrdini().size();
        }
        try {
            String queryString = "INSERT INTO ordine(id,totale,data) VALUES(?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            ptmt.setFloat(2, s.getTotale());
            ptmt.setString(3, data);
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
        for (int i = 0; i < s.getRigheOrdine().size(); i++) {
            try {
                String queryString = "INSERT INTO articoliOrdinati(idOrdine,nome,quantita,subTotale) VALUES(?,?,?,?)";
                connection = getConnection();
                ptmt = connection.prepareStatement(queryString);
                ptmt.setInt(1, id);
                ptmt.setString(2, s.getRigheOrdine().get(i).getDispositivo().getNome());
                ptmt.setInt(3, s.getRigheOrdine().get(i).getQuantita());
                ptmt.setFloat(4, s.getRigheOrdine().get(i).getSubTotale());
                ptmt.executeUpdate();
                System.out.println("Data Added Successfully");
                findAll();
            } catch (SQLException e) {
            } finally {
                try {
                    if (ptmt != null) {
                        ptmt.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                } catch (Exception e) {
                }
            }
        }
    }

    public void update(String nome, int quantita) {

    }

    public void findAll() {
        List<SchedaOrdine> l = new ArrayList();
        try {
            String queryString = "SELECT * FROM ordine";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                SchedaOrdine so = new SchedaOrdine(resultSet.getInt("id"), resultSet.getFloat("totale"), resultSet.getString("data"));
                l.add(so);
            }
            overClock.setOrdini(l);
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public List<RigaOrdine> findArticoli(int id) {
        List<RigaOrdine> l = new ArrayList();
        try {
            String queryString = "SELECT * FROM articoliordinati WHERE idOrdine=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                Dispositivo d = overClock.getDispositivoByNome(resultSet.getString("nome"));
                RigaOrdine ro = new RigaOrdine(d);
                ro.setQuantita(resultSet.getInt("quantita"));
                l.add(ro);
            }
            return l;
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
        return null;
    }
}
