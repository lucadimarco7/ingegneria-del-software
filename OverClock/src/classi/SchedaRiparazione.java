package classi;

import java.util.ArrayList;
import java.util.List;

public class SchedaRiparazione {

    private int id;
    private Cliente cliente;
    private Dispositivo dispositivo;
    private List<Dispositivo> pezzi = new ArrayList();
    private String data;

    public enum statoRiparazioni {
        preventivoInCorso,
        preventivoConcluso,
        riparazioneInCorso,
        riparazioneConclusa
    }
    private statoRiparazioni stato = statoRiparazioni.preventivoInCorso;
    private float costo;

    public SchedaRiparazione(int id, Cliente cliente, Dispositivo dispositivo, statoRiparazioni stato, float costo, String data) {
        this.id = id;
        this.cliente = cliente;
        this.dispositivo = dispositivo;
        this.stato = stato;
        this.costo = costo;
        this.data = data;
        this.pezzi = null;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Dispositivo getDispositivo() {
        return this.dispositivo;
    }

    public statoRiparazioni getStato() {
        return this.stato;
    }

    public void aggiornamentoRiparazione(List<Dispositivo> pezzi, statoRiparazioni stato, float costo) {
        this.pezzi = pezzi;
        this.stato = stato;
        this.costo = this.costo + costo;
    }

    public void notificaCliente(String oggetto, String messaggio, String Destinatatio) {
        InvioMail.inviaMail(oggetto, messaggio, Destinatatio);
    }

    public void aggiungiPezzo(Dispositivo d) {
        this.pezzi.add(d);
    }

    public void setPezzi(List<Dispositivo> l) {
        this.pezzi = l;
    }

    public List<Dispositivo> getPezzi() {
        return this.pezzi;
    }

    public int getID() {
        return this.id;
    }

    public float getCosto() {
        return this.costo;
    }

}
