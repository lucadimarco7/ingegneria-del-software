package gui;

import classi.CartaFedelta;
import classi.CartaFedeltaDAO;
import classi.Cliente;
import classi.ClienteDAO;
import classi.OverClock;
import classi.SchedaAcquistoDAO;
import classi.SchedaDiVenditaDAO;
import java.awt.event.ActionEvent;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

public class InserisciEliminaCartaFedelta extends javax.swing.JFrame {

    private OverClock overClock;
    private OperazioneRiuscita frameOperazioneRiuscita;
    private SchedaAcquistoDAO acquistoDAO;
    private SchedaDiVenditaDAO venditaDAO;
    private CartaFedeltaDAO cartaDAO = new CartaFedeltaDAO();
    private ClienteDAO clienteDAO;

    public InserisciEliminaCartaFedelta(OverClock overClock) {
        this.clienteDAO = new ClienteDAO();
        this.acquistoDAO = new SchedaAcquistoDAO();
        this.venditaDAO = new SchedaDiVenditaDAO();
        this.overClock = overClock;
        initComponents();
    }

    private InserisciEliminaCartaFedelta() {
        this.clienteDAO = new ClienteDAO();
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Titolo = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        numeroCliente = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Titolo.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        Titolo.setText("Nuova Carta Fedeltà");

        jLabel1.setText("Telefono Cliente :");

        jButton1.setText("Crea Nuova Carta");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InserisciCartaActionPerformed(evt);
            }
        });

        jLabel2.setForeground(new java.awt.Color(255, 0, 0));
        jLabel2.setVisible(false);

        jButton2.setBackground(new java.awt.Color(255, 102, 102));
        jButton2.setForeground(new java.awt.Color(0, 0, 0));
        jButton2.setText("Elimina Carta");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminaCartaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Titolo)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(35, 35, 35)
                        .addComponent(numeroCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)
                        .addGap(50, 50, 50))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(Titolo)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(numeroCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void InserisciCartaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InserisciCartaActionPerformed
        Cliente cliente = overClock.getCliente(numeroCliente.getText());
        if (cliente == null) {
            jLabel2.setText("Nessun cliente associato al numero inserito");
            jLabel2.setVisible(true);
        } else {
            if (cliente.getCartaFedelta() == null) {
                jLabel2.setVisible(false);
                CartaFedelta carta = new CartaFedelta(overClock.findID(), 0);
                cliente.setCartaFedelta(carta);
                cartaDAO.add(carta);
                clienteDAO.updateCarta(cliente);
                frameOperazioneRiuscita = new OperazioneRiuscita();
                Timer timer;
                timer = new Timer(1000, (ActionEvent e) -> {
                    frameOperazioneRiuscita.dispose();
                });
                timer.setRepeats(false);
                timer.start();
                frameOperazioneRiuscita.setVisible(true);
                frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                numeroCliente.setText("");

            } else {
                jLabel2.setText("Il cliente ha gia una carta fedelta");
                jLabel2.setVisible(true);
            }

        }

    }//GEN-LAST:event_InserisciCartaActionPerformed

    private void EliminaCartaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminaCartaActionPerformed
        Cliente cliente = overClock.getCliente(numeroCliente.getText());
        if (cliente != null) {
            if (cliente.getCartaFedelta() == null) {
                jLabel2.setText("Nessuna carta fedelta associato al numero inserito");
                jLabel2.setVisible(true);
                System.out.print("errore: il cliente non ha una carta fedelta \n");
            } else {
                jLabel2.setVisible(false);
                cartaDAO.delete(cliente.getCartaFedelta().getID());
                venditaDAO.setID(cliente.getCartaFedelta().getID());
                acquistoDAO.setID(cliente.getCartaFedelta().getID());
                cliente.setCartaFedelta(null);
                clienteDAO.updateCarta(cliente);

                System.out.print(" Carta fedelta correttamente eliminata \n");
                frameOperazioneRiuscita = new OperazioneRiuscita();
                Timer timer;
                timer = new Timer(1000, (ActionEvent e) -> {
                    frameOperazioneRiuscita.dispose();
                });
                timer.setRepeats(false);
                timer.start();
                frameOperazioneRiuscita.setVisible(true);
                frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                numeroCliente.setText("");

            }
        } else {
            jLabel2.setVisible(false);
            jLabel2.setText("Il numero inserito non è associato a nessun cliente");
            jLabel2.setVisible(true);
        }
    }//GEN-LAST:event_EliminaCartaActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InserisciEliminaCartaFedelta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InserisciEliminaCartaFedelta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InserisciEliminaCartaFedelta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InserisciEliminaCartaFedelta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new InserisciEliminaCartaFedelta().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Titolo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField numeroCliente;
    // End of variables declaration//GEN-END:variables
}
