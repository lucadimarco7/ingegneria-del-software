package tests.classi;

import classi.Cliente;
import classi.Dispositivo;
import classi.Distributore;
import classi.SchedaAcquisto;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Test;
import static org.junit.Assert.*;

public class SchedaAcquistoTest {

    Cliente c = new Cliente("nome", "cognome", "email@mail.it", "3807662947", null);
    Distributore distributore = new Distributore(0, "antonino", "domanismetto93@gmail.com");
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    String data = dtf.format(now);
    SchedaAcquisto sa = new SchedaAcquisto(0, c, d, data);
    SchedaAcquisto.Danni danni = SchedaAcquisto.Danni.comeNuovo;

    public SchedaAcquistoTest() {
        sa.setDanni(danni);
    }

    @Test
    public void testGetID() {
        int expResult = 0;
        int result = sa.getID();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCliente() {
        Cliente expResult = c;
        Cliente result = sa.getCliente();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDispositivo() {
        Dispositivo expResult = d;
        Dispositivo result = sa.getDispositivo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetData() {
        String expResult = data;
        String result = sa.getData();
        assertEquals(expResult, result);
    }

}
