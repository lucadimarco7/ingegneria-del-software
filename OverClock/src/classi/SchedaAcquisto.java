package classi;

public class SchedaAcquisto {

    private int id;
    private Cliente cliente;
    private Dispositivo dispositivo;
    private String data;

    SchedaAcquisto(int id, Dispositivo dispositivo, String data) {
        this.id = id;
        this.cliente = null;
        this.dispositivo = dispositivo;
        this.data = data;

    }

    public enum Danni {
        comeNuovo,
        danniLeggeri,
        danniEvidenti
    }
    private Danni danni;

    public SchedaAcquisto(int id, Cliente cliente, Dispositivo dispositivo, String data) {
        this.id = id;
        this.cliente = cliente;
        this.dispositivo = dispositivo;
        this.data = data;
    }

    public int getID() {
        return this.id;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public Dispositivo getDispositivo() {
        return this.dispositivo;
    }

    public String getData() {
        return this.data;
    }

    public void setDanni(Danni danni) {
        this.danni = danni;
    }
}
