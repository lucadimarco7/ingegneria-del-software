package gui;

import classi.CartaFedeltaDAO;
import classi.DispositivoDAO;
import classi.ClienteDAO;
import classi.Distributore;
import classi.DistributoreDAO;
import classi.OverClock;
import classi.PrenotazioneDAO;
import classi.PromozioneDAO;
import classi.SchedaAcquistoDAO;
import classi.SchedaDiVenditaDAO;
import classi.SchedaOrdineDAO;
import classi.SchedaRiparazioneDAO;
import java.util.List;
import javax.swing.ImageIcon;

public class OverClockGui extends javax.swing.JFrame {

    private OverClock overClock;
    private DispositivoDAO dispositivoDAO = new DispositivoDAO();
    private ClienteDAO clienteDAO = new ClienteDAO();
    private CartaFedeltaDAO cartaFedeltaDAO = new CartaFedeltaDAO();
    private DistributoreDAO distributoreDAO = new DistributoreDAO();
    private PromozioneDAO promozioneDAO = new PromozioneDAO();
    private PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();
    private SchedaDiVenditaDAO schedaDiVenditaDAO = new SchedaDiVenditaDAO();
    private SchedaOrdineDAO schedaOrdineDAO = new SchedaOrdineDAO();
    private SchedaRiparazioneDAO schedaRiparazioneDAO = new SchedaRiparazioneDAO();
    private SchedaAcquistoDAO schedaAcquistoDAO = new SchedaAcquistoDAO();
    private InserisciDispositivo panelInserisciDispositivo;
    private InserisciEliminaCartaFedelta frameInserisciCartaFedelta;
    private NuovoOrdineDistributore panelNuovoOrdineDistributore;
    private VisualizzaCatalogo panelVisualizzaCatalogo;
    private VisualizzaCatalogoPezzi panelVisualizzaCatalogoPezzi;
    private InserisciVendita panelInserisciVendita;
    private InserisciCliente panelInserisciCliente;
    private InserisciDistributore panelInserisciDistributore;
    private VisualizzaClienti panelVisualizzaClienti;
    private VisualizzaCarteFedelta panelVisualizzaCarteFedelta;
    private VisualizzaVendite panelVisualizzaVendite;
    private VisualizzaDistributori panelVisualizzaDistributori;
    private VisualizzaOrdini panelVisualizzaOrdini;
    private VisualizzaRiparazioni panelVisualizzaRiparazioni;
    private NuovoPreventivo panelNuovoPreventivo;
    private AggiornaSchedaRiparazione panelAggiornaSchedaRiparazione;
    private InserisciAcquistoUsato panelInserisciAcquistoUsato;
    private VisualizzaAcquisti panelVisualizzaAcquisti;
    private InserisciPromozione panelInserisciPromozione;
    private VisualizzaPromozioni panelVisualizzaPromozioni;
    private InserisciPrenotazione panelInserisciPrenotazione;
    private VisualizzaPrenotazioni panelVisualizzaPrenotazioni;
    private Logo panelLogo;
    ImageIcon icona = new ImageIcon("./icona.png");
    List<Distributore> d;

    public OverClockGui() {
        overClock = OverClock.getIstanza();
        cartaFedeltaDAO.findAll();
        clienteDAO.findAll();
        distributoreDAO.findAll();
        dispositivoDAO.findAll();
        prenotazioneDAO.findAll();
        schedaOrdineDAO.findAll();
        schedaRiparazioneDAO.findAll();
        schedaDiVenditaDAO.findAll();
        schedaAcquistoDAO.findAll();
        this.setTitle("OverClock");
        this.setIconImage(icona.getImage());
        panelLogo = new Logo();
        this.setContentPane(panelLogo);
        this.pack();
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menuBar = new javax.swing.JMenuBar();
        catalogo = new javax.swing.JMenu();
        inserisciArticolo = new javax.swing.JMenuItem();
        visualizzaCatalogoDispositivi = new javax.swing.JMenuItem();
        visualizzaCatalogoPezzi = new javax.swing.JMenuItem();
        clienti = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        inserisciCliente = new javax.swing.JMenuItem();
        visualizzaCarte = new javax.swing.JMenuItem();
        visualizzaClienti = new javax.swing.JMenuItem();
        distributori = new javax.swing.JMenu();
        nuovoDistributore = new javax.swing.JMenuItem();
        nuovoOrdineDistributore = new javax.swing.JMenuItem();
        visualizzaOrdini = new javax.swing.JMenuItem();
        visualizzaDistributori = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        registroPrenotazioni = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        promozioniAttive = new javax.swing.JMenuItem();
        riparazioni = new javax.swing.JMenu();
        nuovoPreventivo = new javax.swing.JMenuItem();
        riparazioniInCorso = new javax.swing.JMenuItem();
        registroRiprazioni = new javax.swing.JMenuItem();
        vendite = new javax.swing.JMenu();
        inserisciVendita = new javax.swing.JMenuItem();
        visualizzaVendite = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        nuovoAcquisto = new javax.swing.JMenuItem();
        visualizzaAcquisti = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setName("OverClock"); // NOI18N

        menuBar.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
        menuBar.setBorder(null);

        catalogo.setText("Catalogo");

        inserisciArticolo.setMnemonic('o');
        inserisciArticolo.setText("Nuovo Articolo");
        inserisciArticolo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inserisciArticoloActionPerformed(evt);
            }
        });
        catalogo.add(inserisciArticolo);

        visualizzaCatalogoDispositivi.setMnemonic('t');
        visualizzaCatalogoDispositivi.setText("Visualizza Catalogo Dispositivi");
        visualizzaCatalogoDispositivi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visualizzaCatalogoDispositiviActionPerformed(evt);
            }
        });
        catalogo.add(visualizzaCatalogoDispositivi);

        visualizzaCatalogoPezzi.setText("Visualizza Catalogo Pezzi");
        visualizzaCatalogoPezzi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visualizzaCatalogoPezziActionPerformed(evt);
            }
        });
        catalogo.add(visualizzaCatalogoPezzi);

        menuBar.add(catalogo);

        clienti.setText("Clienti");

        jMenuItem1.setText("Crea/Elimina CartaFedelta");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NuovaCartaActionPerformed(evt);
            }
        });
        clienti.add(jMenuItem1);

        inserisciCliente.setMnemonic('a');
        inserisciCliente.setText("Nuovo Cliente");
        inserisciCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inserisciClienteActionPerformed(evt);
            }
        });
        clienti.add(inserisciCliente);

        visualizzaCarte.setMnemonic('p');
        visualizzaCarte.setText("Visualizza Carte Fedeltà");
        visualizzaCarte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visualizzaCarteActionPerformed(evt);
            }
        });
        clienti.add(visualizzaCarte);

        visualizzaClienti.setMnemonic('y');
        visualizzaClienti.setText("Visualizza Clienti");
        visualizzaClienti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visualizzaClientiActionPerformed(evt);
            }
        });
        clienti.add(visualizzaClienti);

        menuBar.add(clienti);

        distributori.setText("Distributori");

        nuovoDistributore.setText("Nuovo Distributore");
        nuovoDistributore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuovoDistributoreActionPerformed(evt);
            }
        });
        distributori.add(nuovoDistributore);

        nuovoOrdineDistributore.setText("Nuovo Ordine Distributore");
        nuovoOrdineDistributore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuovoOrdineDistributoreActionPerformed(evt);
            }
        });
        distributori.add(nuovoOrdineDistributore);

        visualizzaOrdini.setText("Registro Ordini");
        visualizzaOrdini.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visualizzaOrdiniActionPerformed(evt);
            }
        });
        distributori.add(visualizzaOrdini);

        visualizzaDistributori.setText("Visualizza Distributori");
        visualizzaDistributori.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visualizzaDistributoriActionPerformed(evt);
            }
        });
        distributori.add(visualizzaDistributori);

        menuBar.add(distributori);

        jMenu3.setText("Prenotazioni");

        jMenuItem3.setText("Nuova Prenotazione");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jMenuItem3);

        registroPrenotazioni.setText("Registro Prenotazioni");
        registroPrenotazioni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registroPrenotazioniActionPerformed(evt);
            }
        });
        jMenu3.add(registroPrenotazioni);

        menuBar.add(jMenu3);

        jMenu2.setText("Promozioni");

        jMenuItem2.setText("Nuova Promozione");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        promozioniAttive.setText("Promozioni Attive");
        promozioniAttive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                promozioniAttiveActionPerformed(evt);
            }
        });
        jMenu2.add(promozioniAttive);

        menuBar.add(jMenu2);

        riparazioni.setMnemonic('h');
        riparazioni.setText("Riparazioni");

        nuovoPreventivo.setMnemonic('c');
        nuovoPreventivo.setText("Nuovo Preventivo");
        nuovoPreventivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuovoPreventivoActionPerformed(evt);
            }
        });
        riparazioni.add(nuovoPreventivo);

        riparazioniInCorso.setMnemonic('a');
        riparazioniInCorso.setText("Riparazioni in Corso");
        riparazioniInCorso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                riparazioniInCorsoActionPerformed(evt);
            }
        });
        riparazioni.add(riparazioniInCorso);

        registroRiprazioni.setText("Registro Riparazioni");
        registroRiprazioni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registroRiprazioniActionPerformed(evt);
            }
        });
        riparazioni.add(registroRiprazioni);

        menuBar.add(riparazioni);

        vendite.setText("Vendite");

        inserisciVendita.setMnemonic('s');
        inserisciVendita.setText("Nuova Vendita");
        inserisciVendita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inserisciVenditaActionPerformed(evt);
            }
        });
        vendite.add(inserisciVendita);

        visualizzaVendite.setMnemonic('d');
        visualizzaVendite.setText("Registro Vendite");
        visualizzaVendite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visualizzaVenditeActionPerformed(evt);
            }
        });
        vendite.add(visualizzaVendite);

        menuBar.add(vendite);

        jMenu1.setText("Usato");

        nuovoAcquisto.setText("Nuovo Acquisto Usato");
        nuovoAcquisto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nuovoAcquistoActionPerformed(evt);
            }
        });
        jMenu1.add(nuovoAcquisto);

        visualizzaAcquisti.setText("Visualizza Acquisti Usato");
        visualizzaAcquisti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                visualizzaAcquistiActionPerformed(evt);
            }
        });
        jMenu1.add(visualizzaAcquisti);

        menuBar.add(jMenu1);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1158, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 607, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void visualizzaCatalogoDispositiviActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visualizzaCatalogoDispositiviActionPerformed
        panelVisualizzaCatalogo = new VisualizzaCatalogo(overClock);
        this.setContentPane(panelVisualizzaCatalogo);
        this.revalidate();
    }//GEN-LAST:event_visualizzaCatalogoDispositiviActionPerformed

    private void inserisciArticoloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inserisciArticoloActionPerformed
        panelInserisciDispositivo = new InserisciDispositivo(overClock);
        this.setContentPane(panelInserisciDispositivo);
        this.revalidate();
    }//GEN-LAST:event_inserisciArticoloActionPerformed

    private void inserisciVenditaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inserisciVenditaActionPerformed
        panelInserisciVendita = new InserisciVendita(overClock);
        this.setContentPane(panelInserisciVendita);
        this.revalidate();
    }//GEN-LAST:event_inserisciVenditaActionPerformed

    private void inserisciClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inserisciClienteActionPerformed
        panelInserisciCliente = new InserisciCliente(overClock);
        this.setContentPane(panelInserisciCliente);
        this.revalidate();
    }//GEN-LAST:event_inserisciClienteActionPerformed

    private void visualizzaClientiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visualizzaClientiActionPerformed
        panelVisualizzaClienti = new VisualizzaClienti(overClock);
        this.setContentPane(panelVisualizzaClienti);
        this.revalidate();
    }//GEN-LAST:event_visualizzaClientiActionPerformed

    private void visualizzaCarteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visualizzaCarteActionPerformed
        panelVisualizzaCarteFedelta = new VisualizzaCarteFedelta(overClock);
        this.setContentPane(panelVisualizzaCarteFedelta);
        this.revalidate();
    }//GEN-LAST:event_visualizzaCarteActionPerformed

    private void visualizzaVenditeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visualizzaVenditeActionPerformed
        panelVisualizzaVendite = new VisualizzaVendite(overClock);
        this.setContentPane(panelVisualizzaVendite);
        this.revalidate();
    }//GEN-LAST:event_visualizzaVenditeActionPerformed

    private void nuovoDistributoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuovoDistributoreActionPerformed
        panelInserisciDistributore = new InserisciDistributore(overClock);
        this.setContentPane(panelInserisciDistributore);
        this.revalidate();
    }//GEN-LAST:event_nuovoDistributoreActionPerformed

    private void visualizzaDistributoriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visualizzaDistributoriActionPerformed
        panelVisualizzaDistributori = new VisualizzaDistributori(overClock);
        this.setContentPane(panelVisualizzaDistributori);
        this.revalidate();
    }//GEN-LAST:event_visualizzaDistributoriActionPerformed

    private void visualizzaCatalogoPezziActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visualizzaCatalogoPezziActionPerformed
        panelVisualizzaCatalogoPezzi = new VisualizzaCatalogoPezzi(overClock);
        this.setContentPane(panelVisualizzaCatalogoPezzi);
        this.revalidate();
    }//GEN-LAST:event_visualizzaCatalogoPezziActionPerformed

    private void riparazioniInCorsoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_riparazioniInCorsoActionPerformed
        panelAggiornaSchedaRiparazione = new AggiornaSchedaRiparazione(overClock);
        this.setContentPane(panelAggiornaSchedaRiparazione);
        this.revalidate();
    }//GEN-LAST:event_riparazioniInCorsoActionPerformed

    private void nuovoPreventivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuovoPreventivoActionPerformed
        panelNuovoPreventivo = new NuovoPreventivo(overClock);
        this.setContentPane(panelNuovoPreventivo);
        this.revalidate();
    }//GEN-LAST:event_nuovoPreventivoActionPerformed

    private void nuovoOrdineDistributoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuovoOrdineDistributoreActionPerformed
        panelNuovoOrdineDistributore = new NuovoOrdineDistributore(overClock);
        this.setContentPane(panelNuovoOrdineDistributore);
        this.revalidate();
    }//GEN-LAST:event_nuovoOrdineDistributoreActionPerformed

    private void visualizzaOrdiniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visualizzaOrdiniActionPerformed
        panelVisualizzaOrdini = new VisualizzaOrdini(overClock);
        this.setContentPane(panelVisualizzaOrdini);
        this.revalidate();
    }//GEN-LAST:event_visualizzaOrdiniActionPerformed

    private void NuovaCartaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NuovaCartaActionPerformed
        frameInserisciCartaFedelta = new InserisciEliminaCartaFedelta(overClock);
        frameInserisciCartaFedelta.setVisible(true);
        frameInserisciCartaFedelta.setDefaultCloseOperation(HIDE_ON_CLOSE);
    }//GEN-LAST:event_NuovaCartaActionPerformed

    private void registroRiprazioniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registroRiprazioniActionPerformed
        panelVisualizzaRiparazioni = new VisualizzaRiparazioni(overClock);
        this.setContentPane(panelVisualizzaRiparazioni);
        this.revalidate();
    }//GEN-LAST:event_registroRiprazioniActionPerformed

    private void nuovoAcquistoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nuovoAcquistoActionPerformed
        panelInserisciAcquistoUsato = new InserisciAcquistoUsato(overClock);
        this.setContentPane(panelInserisciAcquistoUsato);
        this.revalidate();
    }//GEN-LAST:event_nuovoAcquistoActionPerformed

    private void visualizzaAcquistiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_visualizzaAcquistiActionPerformed
        panelVisualizzaAcquisti = new VisualizzaAcquisti(overClock);
        this.setContentPane(panelVisualizzaAcquisti);
        this.revalidate();
    }//GEN-LAST:event_visualizzaAcquistiActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        panelInserisciPromozione = new InserisciPromozione(overClock);
        this.setContentPane(panelInserisciPromozione);
        this.revalidate();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void promozioniAttiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_promozioniAttiveActionPerformed
        panelVisualizzaPromozioni = new VisualizzaPromozioni(overClock);
        this.setContentPane(panelVisualizzaPromozioni);
        this.revalidate();
    }//GEN-LAST:event_promozioniAttiveActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        panelInserisciPrenotazione = new InserisciPrenotazione(overClock);
        this.setContentPane(panelInserisciPrenotazione);
        this.revalidate();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void registroPrenotazioniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registroPrenotazioniActionPerformed
        panelVisualizzaPrenotazioni = new VisualizzaPrenotazioni(overClock);
        this.setContentPane(panelVisualizzaPrenotazioni);
        this.revalidate();
    }//GEN-LAST:event_registroPrenotazioniActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OverClockGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OverClockGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OverClockGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OverClockGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new OverClockGui().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu catalogo;
    private javax.swing.JMenu clienti;
    private javax.swing.JMenu distributori;
    private javax.swing.JMenuItem inserisciArticolo;
    private javax.swing.JMenuItem inserisciCliente;
    private javax.swing.JMenuItem inserisciVendita;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem nuovoAcquisto;
    private javax.swing.JMenuItem nuovoDistributore;
    private javax.swing.JMenuItem nuovoOrdineDistributore;
    private javax.swing.JMenuItem nuovoPreventivo;
    private javax.swing.JMenuItem promozioniAttive;
    private javax.swing.JMenuItem registroPrenotazioni;
    private javax.swing.JMenuItem registroRiprazioni;
    private javax.swing.JMenu riparazioni;
    private javax.swing.JMenuItem riparazioniInCorso;
    private javax.swing.JMenu vendite;
    private javax.swing.JMenuItem visualizzaAcquisti;
    private javax.swing.JMenuItem visualizzaCarte;
    private javax.swing.JMenuItem visualizzaCatalogoDispositivi;
    private javax.swing.JMenuItem visualizzaCatalogoPezzi;
    private javax.swing.JMenuItem visualizzaClienti;
    private javax.swing.JMenuItem visualizzaDistributori;
    private javax.swing.JMenuItem visualizzaOrdini;
    private javax.swing.JMenuItem visualizzaVendite;
    // End of variables declaration//GEN-END:variables

}
