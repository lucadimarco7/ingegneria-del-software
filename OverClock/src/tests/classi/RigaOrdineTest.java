package tests.classi;

import classi.Dispositivo;
import classi.Distributore;
import classi.RigaOrdine;
import org.junit.Test;
import static org.junit.Assert.*;

public class RigaOrdineTest {

    Distributore distributore = new Distributore(0, "apple", "apple@gmail.it");
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    RigaOrdine rdo = new RigaOrdine(d);

    public RigaOrdineTest() {

    }

    @Test
    public void testSetGetQuantita() {
        int expREsult = 2;
        rdo.setQuantita(2);
        assertEquals(expREsult, rdo.getQuantita());
    }

    @Test
    public void testGetDispositivo() {
        Dispositivo expResult = d;
        Dispositivo result = rdo.getDispositivo();
        assertEquals(expResult, result);;
    }

    @Test
    public void testGetSubTotale() {
        float expResult = 699.99F;
        float result = rdo.getSubTotale();
        assertEquals(expResult, result, 0.0);
    }

}
