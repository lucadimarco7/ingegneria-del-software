package gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Logo extends javax.swing.JPanel {

    private BufferedImage immagine;
    Dimension boundary = new Dimension(200, 200);

    public Logo() {
        try {
            immagine = ImageIO.read(new File("./logo.png"));
        } catch (IOException e) {
            System.out.println("Immagine non trovata");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    @Override
    protected void paintComponent(Graphics g) {
        double scalex = (double) getWidth() / immagine.getWidth();
        double scaley = (double) getHeight() / immagine.getHeight();
        double scale = Math.min(scalex, scaley);
        int w = (int) (immagine.getWidth() * scale);
        int h = (int) (immagine.getHeight() * scale);
        Image tmp = immagine.getScaledInstance(w, h, Image.SCALE_FAST);
        int x = (getWidth() - tmp.getWidth(null)) / 2;
        int y = (getHeight() - tmp.getHeight(null)) / 2;
        super.paintComponent(g);
        g.drawImage(tmp, x, y, this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(1080, 559);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
