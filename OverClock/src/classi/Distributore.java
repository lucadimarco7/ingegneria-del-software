package classi;

public class Distributore {

    private int id;
    private String nome;
    private String email;

    public Distributore(Integer ID, String nome, String email) {
        this.nome = nome;
        this.email = email;
        this.id = ID;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public String getEmail() {
        return this.email;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
