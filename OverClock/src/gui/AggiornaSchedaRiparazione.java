package gui;

import classi.Cliente;
import classi.Dispositivo;
import classi.Dispositivo.tipoArticolo;
import classi.DispositivoDAO;
import classi.SchedaRiparazione.statoRiparazioni;
import classi.OverClock;
import classi.Prenotazione.statoPrenotazione;
import classi.SchedaOrdine;
import classi.SchedaOrdineDAO;
import classi.SchedaRiparazione;
import classi.SchedaRiparazioneDAO;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

public class AggiornaSchedaRiparazione extends javax.swing.JPanel {

    private OverClock overClock;
    private DispositivoDAO dispositivoDAO = new DispositivoDAO();
    private SchedaRiparazioneDAO schedaRiparazioneDAO = new SchedaRiparazioneDAO();
    private SchedaOrdineDAO schedaOrdineDAO = new SchedaOrdineDAO();
    private DefaultTableModel dim;
    private statoRiparazioni stato = null;
    private float importoTotale = 0;
    private float importoTotaleOrdine = 0;
    private List<SchedaRiparazione> riparazioni;
    private List<Dispositivo> pezzi = new ArrayList();
    private SchedaOrdine schedaOrdine = new SchedaOrdine();
    private DefaultTableModel dim2;
    private String cambioStato = "";
    private String dati = "";
    private float costo = 0;
    private float t = 0.0f;
    private boolean ck = false;
    private Boolean Opzioni = true;
    private ConfermaOrdine frameConfermaOrdine;
    private OperazioneRiuscita frameOperazioneRiuscita;

    public AggiornaSchedaRiparazione(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        pulsanteConferma = new javax.swing.JButton();
        selezionaStato = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        pulsanteAggiungiPezzo = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        pulsanteRimuoviPezzo = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        totale = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        ricercaPezzo = new javax.swing.JTextField();
        pulsanteRicercaPezzi = new javax.swing.JButton();
        pulsanteRimuoviScheda = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        ricercaScheda = new javax.swing.JTextField();
        pulsanteRicercaScheda = new javax.swing.JButton();
        errore = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        manodopera = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        tot = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        errore2 = new javax.swing.JLabel();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Riparazioni");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "E-Mail", "Numero", "Dispositivo", "Pezzi", "Stato Riparazione", "Costo", "ID", "Data"
            }
        ));
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dim=(DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        riparazioni=overClock.getRiparazioni("");
        for(int i=riparazioni.size()-1;i>=0;i--)
        if(riparazioni.get(i).getStato()==statoRiparazioni.riparazioneConclusa)
        riparazioni.remove(i);
        for(int i=0;i<riparazioni.size();i++){
            int n = schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).size();
            costo=0;
            dati="";
            for(int j=0;j<n;j++){

                dati=dati+schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome()+" x"+schedaRiparazioneDAO.findQuantita(riparazioni.get(i).getID(), schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome())+"\n";
            }
            dim.addRow(new Object[]{riparazioni.get(i).getCliente().getEmail(),riparazioni.get(i).getCliente().getNumero(),riparazioni.get(i).getDispositivo().getNome(),dati,String.valueOf(riparazioni.get(i).getStato()),riparazioni.get(i).getCosto(),riparazioni.get(i).getID(),riparazioni.get(i).getData()});
        }

        DefaultTableModel dim=(DefaultTableModel) jTable1.getModel();
        jTable1.setDefaultEditor(Object.class, null);
        jTable1.getColumnModel().getColumn(3).setCellRenderer(new MultiLineTableCellRenderer());
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        pulsanteConferma.setText("Conferma");
        pulsanteConferma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteConfermaActionPerformed(evt);
            }
        });

        selezionaStato.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Preventivo Concluso", "Riparazione in Corso", "Riparazione Conclusa" }));
        selezionaStato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selezionaStatoActionPerformed(evt);
            }
        });
        if(jTable1.getSelectedRow()!=-1){
            if(!String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 4)).equals("preventivoConcluso"))
            selezionaStato.setEnabled(false);
            else
            selezionaStato.setEnabled(true);
        }else{
            selezionaStato.setEnabled(false);
        }

        jLabel2.setText("Seleziona i pezzi i ricambio:");

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Pezzo", "Prezzo", "Quantità", "Codice"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane3.setViewportView(jTable2);

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Pezzo", "Codice", "Prezzo", "Quantità"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable3.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        DefaultTableModel dim3=(DefaultTableModel) jTable3.getModel();
        dim3.setRowCount(0);
        List<Dispositivo> listaPezzi=overClock.visualizzaListaDispositivi("");
        for(int i=listaPezzi.size()-1;i>=0;i--){
            if(listaPezzi.get(i).getTipo()!=tipoArticolo.pezzo)
            listaPezzi.remove(i);}
        for(int i=0;i<listaPezzi.size();i++){
            if(listaPezzi.get(i).getTipo()==Dispositivo.tipoArticolo.pezzo)
            dim3.addRow(new Object[]{listaPezzi.get(i).getNome(), listaPezzi.get(i).getCodice(),listaPezzi.get(i).getPrezzo(),listaPezzi.get(i).getQuantita()});
        }
        jScrollPane4.setViewportView(jTable3);

        pulsanteAggiungiPezzo.setBackground(new java.awt.Color(204, 255, 102));
        pulsanteAggiungiPezzo.setText("Aggiungi Pezzo");
        pulsanteAggiungiPezzo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteAggiungiPezzoActionPerformed(evt);
            }
        });

        jLabel4.setText("Pezzi selezionati:");

        pulsanteRimuoviPezzo.setBackground(new java.awt.Color(255, 102, 102));
        pulsanteRimuoviPezzo.setText("Rimuovi Pezzo");
        pulsanteRimuoviPezzo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteRimuoviPezzoActionPerformed(evt);
            }
        });

        jLabel6.setText("Importo Totale Pezzi:");

        totale.setEditable(false);

        jLabel7.setText("Ricerca Pezzo:");

        pulsanteRicercaPezzi.setText("Ricerca");
        pulsanteRicercaPezzi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteRicercaPezziActionPerformed(evt);
            }
        });

        pulsanteRimuoviScheda.setBackground(new java.awt.Color(255, 102, 102));
        pulsanteRimuoviScheda.setText("Rimuovi Preventivo Selezionato");
        pulsanteRimuoviScheda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteRimuoviSchedaActionPerformed(evt);
            }
        });

        jLabel8.setText("Aggiorna Stato Preventivo Selezionato:");

        jLabel9.setText("Ricerca Riparazione Tramite E-Mail:");

        pulsanteRicercaScheda.setText("Ricerca");
        pulsanteRicercaScheda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteRicercaSchedaActionPerformed(evt);
            }
        });

        errore.setVisible(false);
        errore.setForeground(new java.awt.Color(255, 51, 51));
        errore.setText("Hai selezionato una riparazione");

        jLabel3.setText("Manodopera:");

        manodopera.setEditable(false);
        manodopera.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                manodoperaCaretPositionChanged(evt);
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            }
        });

        jLabel5.setText("+");

        jLabel10.setText("=");

        tot.setEditable(false);

        jLabel11.setText("Totale");

        errore2.setForeground(new java.awt.Color(255, 0, 0));
        errore2.setVisible(false);
        errore2.setText("Manodopera errata o mancante");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(totale))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(manodopera)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tot, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(53, 53, 53)
                                .addComponent(errore2, javax.swing.GroupLayout.PREFERRED_SIZE, 299, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(pulsanteConferma))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(pulsanteAggiungiPezzo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pulsanteRimuoviPezzo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel7)
                                    .addComponent(ricercaPezzo)
                                    .addComponent(pulsanteRicercaPezzi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pulsanteRimuoviScheda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(selezionaStato, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(ricercaScheda)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pulsanteRicercaScheda))
                            .addComponent(errore, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ricercaScheda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pulsanteRicercaScheda))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(selezionaStato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pulsanteRimuoviScheda)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(errore)
                        .addGap(17, 17, 17))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ricercaPezzo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pulsanteRicercaPezzi)
                        .addGap(18, 18, 18)
                        .addComponent(pulsanteAggiungiPezzo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pulsanteRimuoviPezzo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel3)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pulsanteConferma)
                    .addComponent(manodopera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel10)
                    .addComponent(tot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(errore2))
                .addGap(20, 20, 20))
        );

        totale.setText(""+0.0f);
        manodopera.getDocument().addDocumentListener(new DocumentListener() {

            public void removeUpdate(DocumentEvent e) {
                System.out.println("removeUpdate");
                updateTotale();
            }

            public void insertUpdate(DocumentEvent e) {
                System.out.println("insertUpdate");
                updateTotale();

            }

            public void changedUpdate(DocumentEvent e) {
                System.out.println("changedUpdate");
                updateTotale();
            }
        });
        tot.setText(""+0.0f);
    }// </editor-fold>//GEN-END:initComponents

    private void pulsanteConfermaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteConfermaActionPerformed
        int check = 0;
//se c'è una riga selezionata
        if (jTable1.getSelectedRowCount() > 0) {
            //cerca il cliente della riga
            Cliente c = overClock.getClienteMail(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 0)));
            //cerca la scheada riparazione della riga
            SchedaRiparazione s = overClock.getSchedaRiparazione(Integer.parseInt(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 6))));
            //se la combobox non è attiva significa che stato era preventivoInCorso e deve cambiare in preventivoConcluso
            if (!selezionaStato.isEnabled()) {
                stato = statoRiparazioni.preventivoConcluso;
                if (ck == true) {
                    schedaRiparazioneDAO.update(s, stato, pezzi, t);

                } else {
                    errore2.setVisible(true);
                    return;
                }
            }

            String statoStringa = overClock.formattaStato(overClock.getStatoStringa(stato));//formatta lo stato selezionato in una stringa
            if (!cambioStato.equals(String.valueOf(selezionaStato.getSelectedItem()))) {//se lo stato della riga selezionato è diverso dallo stato della combobox
                //mostra un messaggio di operazione riuscita
                frameOperazioneRiuscita = new OperazioneRiuscita();
                Timer timer;
                timer = new Timer(1000, (ActionEvent e) -> {
                    frameOperazioneRiuscita.dispose();
                });
                timer.setRepeats(false);
                timer.start();
                frameOperazioneRiuscita.setVisible(true);
                frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                //ricerca i pezzi della scheda riparazione
                List<Dispositivo> pezziSchedaC = schedaRiparazioneDAO.findPezzi(s.getID());
                String messaggio = "";
                //crea un messaggio contentente tutti i pezzi
                for (int j = 0; j < pezziSchedaC.size(); j++) {
                    messaggio = messaggio + schedaRiparazioneDAO.findPezzi(s.getID()).get(j).getNome() + " x" + schedaRiparazioneDAO.findQuantita(s.getID(), schedaRiparazioneDAO.findPezzi(s.getID()).get(j).getNome()) + "\n";
                }
                //avvisa il cliente dei pezzi necessari e dello stato
                s.notificaCliente("Aggiornamento Riparazione", "Pezzi Necessari:\n" + messaggio + "\nstato: " + statoStringa + "\nCosto Totale: " + s.getCosto(), c.getEmail());
            }
            //se lo stato della riga selezionata è diverso dallo stato della combobox e se lo stato della riga selezionata è preventivo concluso
            if (!cambioStato.equals(String.valueOf(selezionaStato.getSelectedItem()))
                    && String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 4)).equals("preventivoConcluso")) {
                //ricerca i pezzi associati alla riparazione selezionata
                List<Dispositivo> pezziSchedaC = schedaRiparazioneDAO.findPezzi(s.getID());
                SchedaOrdine o = new SchedaOrdine();//crea un ordine
                for (int i = 0; i < pezziSchedaC.size(); i++) {//per ogni pezzo ricercato
                    int pezziInCatalogo = overClock.getDispositivo(pezziSchedaC.get(i).getCodice()).getQuantita();//controlla quanti ce ne sono in catalogo
                    int pezziSchedaTotali = schedaRiparazioneDAO.findQuantita(s.getID(), pezziSchedaC.get(i).getNome());//controlla quanti ne servono
                    if (pezziInCatalogo >= pezziSchedaTotali) {//se tutti i pezzi sono presenti in catalogo
                        check++;
                    } else {//altrimenti aggiorna la quantità in catalogo a 0 e ordina i mancanti
                        schedaRiparazioneDAO.update(s, stato.preventivoConcluso, pezzi, t);
                        int quantitaDaOrdinare = pezziSchedaTotali - pezziInCatalogo;//controlla quanti ne devi ordinare
                        for (int j = 0; j < quantitaDaOrdinare; j++) {//aggiungine all'ordine tanti quanti ne servono
                            o.aggiungiOrdine(pezziSchedaC.get(i), 1);
                        }
                        //mostra un messaggio per confermare l'ordine
                        frameConfermaOrdine = new ConfermaOrdine(o);
                        frameConfermaOrdine.setVisible(true);
                        frameConfermaOrdine.setDefaultCloseOperation(HIDE_ON_CLOSE);
                    }

                }
                if (pezziSchedaC.size() == check) {
                    schedaRiparazioneDAO.update(s, stato.riparazioneInCorso, pezzi, t);
                    for (int i = 0; i < pezziSchedaC.size(); i++) {//per ogni pezzo ricercato
                        int pezziInCatalogo = overClock.getDispositivo(pezziSchedaC.get(i).getCodice()).getQuantita();//controlla quanti ce ne sono in catalogo
                        int pezziSchedaTotali = schedaRiparazioneDAO.findQuantita(s.getID(), pezziSchedaC.get(i).getNome());//controlla quanti ne servono  
                        dispositivoDAO.update(pezziSchedaC.get(i).getNome(), pezziInCatalogo - pezziSchedaTotali);
                    }

                }
            }
            //se lo stato della riga selezionata è diverso dallo stato della combobox e se lo stato della riga selezionata è RiparazioneInCorso
            if (!cambioStato.equals(String.valueOf(selezionaStato.getSelectedItem()))
                    && String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 4)).equals("riparazioneInCorso")) {
                schedaRiparazioneDAO.update(s, stato.riparazioneConclusa, pezzi, t);

            }
            //pulisci le righe della tabella e ricerca nuovamente le riparazioni aggiornate
            dim.setRowCount(0);
            riparazioni = overClock.getRiparazioni("");
            for (int i = riparazioni.size() - 1; i >= 0; i--) {
                if (riparazioni.get(i).getStato() == statoRiparazioni.riparazioneConclusa) {
                    riparazioni.remove(i);
                }
            }
            //per ogni riparazione ricerca i pezzi associati
            for (int i = 0; i < riparazioni.size(); i++) {
                List<Dispositivo> pezziScheda = schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID());
                dati = "";
                costo = 0;
                for (int j = 0; j < pezziScheda.size(); j++) {
                    costo = riparazioni.get(i).getCosto();
                    dati = dati + schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome() + " x" + schedaRiparazioneDAO.findQuantita(riparazioni.get(i).getID(), schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome()) + "\n";
                }
                //riaggiungi le righe alla tabella
                dim.addRow(new Object[]{riparazioni.get(i).getCliente().getEmail(), riparazioni.get(i).getCliente().getNumero(), riparazioni.get(i).getDispositivo().getNome(),
                    dati, String.valueOf(riparazioni.get(i).getStato()), costo, riparazioni.get(i).getID(), riparazioni.get(i).getData()});
            }
            //ripulisci tutte le variabili e ricarica la tabella dei pezzi
            if (dim2 != null) {
                dim2.setRowCount(0);
            }
            importoTotale = 0;
            pezzi.removeAll(pezzi);
            schedaOrdine = new SchedaOrdine();
            totale.setText(String.valueOf(importoTotale));
            manodopera.setText("" + 0.0f);
            tot.setText("" + 0.0f);
            DefaultTableModel dim3 = (DefaultTableModel) jTable3.getModel();
            dim3.setRowCount(0);
            List<Dispositivo> listaDispositivo = overClock.visualizzaListaDispositivi("");
            for (int i = 0; i < listaDispositivo.size(); i++) {
                if (listaDispositivo.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo) {
                    dim3.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
                }
            }
            jTable1.getColumnModel().getColumn(3).setCellRenderer(new MultiLineTableCellRenderer());
        }

    }//GEN-LAST:event_pulsanteConfermaActionPerformed

    private void pulsanteAggiungiPezzoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteAggiungiPezzoActionPerformed
        if (jTable3.getSelectedRow() != -1) {//se è selezionata una riga della tabella dei pezzi in catalogo
            Boolean inserita = false;
            dim2 = (DefaultTableModel) jTable2.getModel();
            //se la tabella dei pezzi inseriti è vuota 
            if (jTable2.getRowCount() == 0) {
                //aggiungi una nuova riga con quantità uno alla tabella dei pezzi 
                dim2.addRow(new Object[]{String.valueOf(jTable3.getValueAt(jTable3.getSelectedRow(), 0)), String.valueOf(jTable3.getValueAt(jTable3.getSelectedRow(), 2)), "1", String.valueOf(jTable3.getValueAt(jTable3.getSelectedRow(), 1))});
            } else {//altrimenti scorri le righe della tabella dei pezzi inseriti
                for (int i = 0; i < jTable2.getRowCount(); i++) {
                    //se ne trovi una con lo stesso codice di quello che vuoi inserire incrementa la quantita
                    if (String.valueOf(jTable3.getValueAt(jTable3.getSelectedRow(), 1)).equals(String.valueOf(jTable2.getValueAt(i, 3)))) {
                        dim2.setValueAt(Integer.parseInt(String.valueOf(jTable2.getValueAt(i, 2))) + 1, i, 2);
                        inserita = true;
                    }
                }
                if (!inserita) {//se scorrendo tutte le righe non ne è stata incrementata nessuna, inserisci una nuova riga
                    dim2.addRow(new Object[]{String.valueOf(jTable3.getValueAt(jTable3.getSelectedRow(), 0)), String.valueOf(jTable3.getValueAt(jTable3.getSelectedRow(), 2)), "1", String.valueOf(jTable3.getValueAt(jTable3.getSelectedRow(), 1))});
                }
            }
            //riempi un vettore con il pezzo appena inserito
            pezzi.add(overClock.getDispositivo(String.valueOf(jTable3.getValueAt(jTable3.getSelectedRow(), 1))));
            //imposta l'importo totale
            importoTotale = importoTotale + overClock.getDispositivo(String.valueOf(jTable3.getValueAt(jTable3.getSelectedRow(), 1))).getPrezzo();
            totale.setText(String.valueOf(importoTotale));
            updateTotale();
        }

    }//GEN-LAST:event_pulsanteAggiungiPezzoActionPerformed

    private void pulsanteRimuoviPezzoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteRimuoviPezzoActionPerformed
        if (jTable2.getSelectedRow() != -1) {//se una riga dei pezzi inseriti è selezionata
            for (int i = 0; i < pezzi.size(); i++) {//scorri la lista dei pezzi necessari
                //se ne trovi uno con lo stesso codice della riga selezionata rimuovilo
                if (overClock.getDispositivo(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 3))) == pezzi.get(i)) {
                    pezzi.remove(i);
                    break;
                }
            }
            //ricalcola il totale
            importoTotale = importoTotale - overClock.getDispositivo(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 3))).getPrezzo();
            totale.setText(String.valueOf(importoTotale));
            //se la quantità della riga selezionata era uno rimuovi la riga
            if (Integer.parseInt(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 2))) > 1) {
                dim2.setValueAt(Integer.parseInt(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 2))) - 1, jTable2.getSelectedRow(), 2);
            } else {
                dim2.removeRow(jTable2.getSelectedRow());
            }
            updateTotale();
        }
    }//GEN-LAST:event_pulsanteRimuoviPezzoActionPerformed

    private void selezionaStatoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selezionaStatoActionPerformed
        //imposta lo stato in base al valore della combobox
        if (Opzioni) {
            switch (selezionaStato.getSelectedIndex()) {
                default -> {
                }
                case 0 ->
                    stato = statoRiparazioni.preventivoConcluso;
                case 1 ->
                    stato = statoRiparazioni.riparazioneInCorso;
            }
        } else {
            switch (selezionaStato.getSelectedIndex()) {
                default -> {
                }
                case 0 ->
                    stato = statoRiparazioni.riparazioneInCorso;
                case 1 ->
                    stato = statoRiparazioni.riparazioneConclusa;
            }
        }
    }//GEN-LAST:event_selezionaStatoActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        //Al click su una riparazione;

        if (jTable1.getSelectedRow() != -1) {
            manodopera.setText("");
            if (String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 4)).equals("preventivoInCorso")) {
                manodopera.setEditable(true);
            } else {
                manodopera.setEditable(false);
            }
            //viene salvato lo stato attuale della riga selezionata
            cambioStato = overClock.formattaStato(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 4)));
            //se lo stato è preventivo concluso, tra le opzioni deve spuntare anche preventivo concluso ma non deve spuntare riparazione conclusa
            if (String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 4)).equals("preventivoConcluso")) {
                Opzioni = true;
                selezionaStato.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"Preventivo Concluso", "Riparazione in Corso"}));
            } else {//altrimenti se lo stato è riparazione in corso non deve essere possibile cambiare lo stato in preventivo concluso
                Opzioni = false;
                selezionaStato.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"Riparazione in Corso", "Riparazione Conclusa"}));
            }
            //se lo stato attuale della riparazione selezionata è diverso da preventivo concluso e riparazione in corso la combobox deve essere non selezionabile
            if (!String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 4)).equals("preventivoConcluso")
                    && !String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 4)).equals("riparazioneInCorso")) {
                selezionaStato.setEnabled(false);
                jTable2.setEnabled(true);
                jTable3.setEnabled(true);
                pulsanteAggiungiPezzo.setEnabled(true); //vale anche per i pulsanti per aggiungere/rimuovere pezzi, sono attivi solo se il preventivo è ancora in corso
                pulsanteRimuoviPezzo.setEnabled(true);
            } else {//altrimenti deve essere selezionabile
                selezionaStato.setEnabled(true);
                jTable2.setEnabled(false);
                jTable3.setEnabled(false);
                pulsanteAggiungiPezzo.setEnabled(false);
                pulsanteRimuoviPezzo.setEnabled(false);
            }
        } else {//la combobox deve essere non selezionabile
            selezionaStato.setEnabled(false);
            jTable2.setEnabled(true);
            jTable3.setEnabled(true);
            pulsanteAggiungiPezzo.setEnabled(true);
            pulsanteRimuoviPezzo.setEnabled(true);
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void pulsanteRicercaSchedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteRicercaSchedaActionPerformed
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        //ricerca le schede riparazioni salvate
        riparazioni = overClock.getRiparazioni(ricercaScheda.getText());
        for (int i = riparazioni.size() - 1; i >= 0; i--) {
            if (riparazioni.get(i).getStato() == statoRiparazioni.riparazioneConclusa) {
                riparazioni.remove(i);
            }
        }
        for (int i = 0; i < riparazioni.size(); i++) {//per ogni riparazione cerca i pezzi, il costo e crea una riga con tutti i dati nella tabella
            costo = 0;
            dati = "";
            int n = schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).size();
            for (int j = 0; j < n; j++) {

                costo = riparazioni.get(i).getCosto();
                dati = dati + schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome() + " x" + schedaRiparazioneDAO.findQuantita(riparazioni.get(i).getID(), schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome()) + "\n";
            }
            dim.addRow(new Object[]{riparazioni.get(i).getCliente().getEmail(), riparazioni.get(i).getCliente().getNumero(), riparazioni.get(i).getDispositivo().getNome(), dati, String.valueOf(riparazioni.get(i).getStato()), costo, riparazioni.get(i).getID(), riparazioni.get(i).getData()});
        }
        dim = (DefaultTableModel) jTable1.getModel();
        jTable1.setDefaultEditor(Object.class, null);
        jTable1.getColumnModel().getColumn(3).setCellRenderer(new MultiLineTableCellRenderer());
    }//GEN-LAST:event_pulsanteRicercaSchedaActionPerformed

    private void pulsanteRicercaPezziActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteRicercaPezziActionPerformed
        //stesso ragionamento della ricerca delle schede riparazione ma con i pezzi di ricambio
        DefaultTableModel dim3 = (DefaultTableModel) jTable3.getModel();
        dim3.setRowCount(0);
        List<Dispositivo> listaPezzi = overClock.visualizzaListaDispositivi(ricercaPezzo.getText());
        for (int i = listaPezzi.size() - 1; i >= 0; i--) {
            if (listaPezzi.get(i).getTipo() != tipoArticolo.pezzo) {
                listaPezzi.remove(i);
            }
        }
        for (int i = 0; i < listaPezzi.size(); i++) {
            if (listaPezzi.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo) {
                dim3.addRow(new Object[]{listaPezzi.get(i).getNome(), listaPezzi.get(i).getCodice(), listaPezzi.get(i).getPrezzo(), listaPezzi.get(i).getQuantita()});
            }
        }
    }//GEN-LAST:event_pulsanteRicercaPezziActionPerformed

    private void updateTotale() {
        errore2.setVisible(false);

        if (manodopera.getText().equals(""))
                            try {
            t = Float.parseFloat(totale.getText());
            tot.setText("" + t);
            manodopera.setBackground(new java.awt.Color(255, 255, 255));
            ck = false;
        } catch (Exception e) {
            manodopera.setBackground(new java.awt.Color(255, 153, 102));
            errore2.setVisible(true);
        }

        try {
            if (Float.parseFloat(manodopera.getText()) >= 0) {

                t = Float.parseFloat(totale.getText()) + Float.parseFloat(manodopera.getText());
                tot.setText("" + t);
                ck = true;
                manodopera.setBackground(new java.awt.Color(255, 255, 255));
            } else {
                manodopera.setBackground(new java.awt.Color(255, 153, 102));
                ck = false;
                errore2.setVisible(true);
            }

        } catch (Exception e) {
            errore2.setVisible(true);
            manodopera.setBackground(new java.awt.Color(255, 153, 102));
            errore2.setVisible(true);
        }

    }

    private void pulsanteRimuoviSchedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteRimuoviSchedaActionPerformed
        errore.setVisible(false);//imposta la label degli errori a non visibile
        if (jTable1.getSelectedRow() != -1) {//se è selezionata una riga delle schede riparazioni
            //ricercala nel sistema
            SchedaRiparazione s = overClock.getSchedaRiparazione(Integer.parseInt(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 6))));
            if (s.getStato() == stato.preventivoInCorso || s.getStato() == stato.preventivoConcluso) {//se lo stato è ancora di preventivo si può eliminare
                schedaRiparazioneDAO.delete(s);
                dim.setRowCount(0);
                riparazioni = overClock.getRiparazioni("");//e viene rimossa anche dal vettore locale e viene ricaricata la tabella con le nuove righe
                for (int i = riparazioni.size() - 1; i >= 0; i--) {
                    if (riparazioni.get(i).getStato() == statoRiparazioni.riparazioneConclusa) {
                        riparazioni.remove(i);
                    }
                }
                for (int i = 0; i < riparazioni.size(); i++) {
                    costo = 0;
                    dati = "";
                    pezzi = schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID());
                    for (int j = 0; j < pezzi.size(); j++) {
                        costo = riparazioni.get(i).getCosto();
                        dati = dati + schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome() + " x" + schedaRiparazioneDAO.findQuantita(riparazioni.get(i).getID(), schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome()) + "\n";
                    }
                    dim.addRow(new Object[]{riparazioni.get(i).getCliente().getEmail(), riparazioni.get(i).getCliente().getNumero(), riparazioni.get(i).getDispositivo().getNome(), dati,
                        String.valueOf(riparazioni.get(i).getStato()), costo, riparazioni.get(i).getID(), riparazioni.get(i).getData()});
                }
                //viene ricaricata anche la tabella dei pezzi di ricambio
                DefaultTableModel dim3 = (DefaultTableModel) jTable3.getModel();
                dim3.setRowCount(0);
                List<Dispositivo> listaPezzi = overClock.visualizzaListaDispositivi(ricercaPezzo.getText());
                for (int i = listaPezzi.size() - 1; i >= 0; i--) {//dalla lista vengono tolti tutti gli articoli che non sono pezzi
                    if (listaPezzi.get(i).getTipo() != tipoArticolo.pezzo) {
                        listaPezzi.remove(i);
                    }
                }
                for (int i = 0; i < listaPezzi.size(); i++) {
                    if (listaPezzi.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo) {
                        dim3.addRow(new Object[]{listaPezzi.get(i).getNome(), listaPezzi.get(i).getCodice(), listaPezzi.get(i).getPrezzo(), listaPezzi.get(i).getQuantita()});
                    }
                }
                s.notificaCliente("Annullamento Riparazione", "Il preventivo è stato annullato come concordato,\nIl prezzo base è di 10€", s.getCliente().getEmail());
                frameOperazioneRiuscita = new OperazioneRiuscita();
                Timer timer;
                timer = new Timer(1000, (ActionEvent e) -> {
                    frameOperazioneRiuscita.dispose();
                    // or maybe you'll need dialog.dispose() instead?
                });
                timer.setRepeats(false);
                timer.start();
                frameOperazioneRiuscita.setVisible(true);
                frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                importoTotale = 0;
                importoTotaleOrdine = 0;
                pezzi.removeAll(pezzi);
                totale.setText(String.valueOf(importoTotale));
            } else {
                errore.setVisible(true);
            }
        }

    }//GEN-LAST:event_pulsanteRimuoviSchedaActionPerformed

    private void manodoperaCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_manodoperaCaretPositionChanged
        updateTotale();
    }//GEN-LAST:event_manodoperaCaretPositionChanged

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel errore;
    private javax.swing.JLabel errore2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField manodopera;
    private javax.swing.JButton pulsanteAggiungiPezzo;
    private javax.swing.JButton pulsanteConferma;
    private javax.swing.JButton pulsanteRicercaPezzi;
    private javax.swing.JButton pulsanteRicercaScheda;
    private javax.swing.JButton pulsanteRimuoviPezzo;
    private javax.swing.JButton pulsanteRimuoviScheda;
    private javax.swing.JTextField ricercaPezzo;
    private javax.swing.JTextField ricercaScheda;
    private javax.swing.JComboBox<String> selezionaStato;
    private javax.swing.JTextField tot;
    private javax.swing.JTextField totale;
    // End of variables declaration//GEN-END:variables
}
