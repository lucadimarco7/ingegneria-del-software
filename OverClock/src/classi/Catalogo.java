package classi;

import java.util.ArrayList;
import java.util.List;

public class Catalogo {

    private static Catalogo singleton;
    private List<Dispositivo> listaDispositivi;

    public Catalogo() {
        this.listaDispositivi = new ArrayList();
    }

    public static Catalogo getIstanza() {
        if (singleton == null) {
            singleton = new Catalogo();
        }
        return singleton;
    }

    public Dispositivo getDispositivo(String codice) {
        for (int i = 0; i < listaDispositivi.size(); i++) {
            if (listaDispositivi.get(i).getCodice().equals(codice)) {
                return listaDispositivi.get(i);
            }
        }
        return null;
    }

    public Dispositivo getDispositivoByNome(String nome) {
        for (int i = 0; i < listaDispositivi.size(); i++) {
            if (listaDispositivi.get(i).getNome().equals(nome)) {
                return listaDispositivi.get(i);
            }
        }
        return null;
    }

    public List<Dispositivo> getListaDispositivi(String ricerca) {
        List<Dispositivo> dispositiviRicercati = new ArrayList<>();
        for (int i = 0; i < listaDispositivi.size(); i++) {
            if (listaDispositivi.get(i).getNome().regionMatches(true, 0, ricerca, 0, ricerca.length())) {
                dispositiviRicercati.add(listaDispositivi.get(i));
            }
        }
        return dispositiviRicercati;
    }

    public List<Dispositivo> getListaDispositiviByNome(String ricerca) {
        List<Dispositivo> dispositiviRicercati = new ArrayList<>();
        for (int i = 0; i < listaDispositivi.size(); i++) {
            if (ricerca.equals(listaDispositivi.get(i).getNome())) {
                dispositiviRicercati.add(listaDispositivi.get(i));
            }
        }
        return dispositiviRicercati;
    }

    public List<Dispositivo> getListaDispositiviUsati(String ricerca) {
        List<Dispositivo> dispositiviRicercati = new ArrayList<>();
        for (int i = 0; i < listaDispositivi.size(); i++) {
            if (listaDispositivi.get(i).getNome().regionMatches(true, 0, ricerca, 0, ricerca.length())
                    && (listaDispositivi.get(i).getNome().contains("comeNuovo") || listaDispositivi.get(i).getNome().contains("danni"))) {
                dispositiviRicercati.add(listaDispositivi.get(i));
            }
        }
        return dispositiviRicercati;
    }

    public void setListaDispositivi(List<Dispositivo> l) {
        this.listaDispositivi = l;
    }

    public List<Dispositivo> getDispositivoByDistributore(int id) {
        List<Dispositivo> dispositiviRicercati = new ArrayList<>();
        for (int i = 0; i < listaDispositivi.size(); i++) {
            if (listaDispositivi.get(i).getDistributore() != null) {
                if (listaDispositivi.get(i).getDistributore().getId() == id) {
                    dispositiviRicercati.add(listaDispositivi.get(i));
                }
            }
        }
        return dispositiviRicercati;
    }
}
