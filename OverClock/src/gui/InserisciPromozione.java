package gui;

import classi.Dispositivo;
import classi.Dispositivo.tipoArticolo;
import classi.OverClock;
import classi.PromozioneDAO;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

public class InserisciPromozione extends javax.swing.JPanel {

    private OverClock overClock;
    private DefaultTableModel dim;
    private List<Dispositivo> listaDispositivi;
    private PromozioneDAO promozioneDAO = new PromozioneDAO();
    private OperazioneRiuscita frameOperazioneRiuscita;

    public InserisciPromozione(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        sconto = new javax.swing.JTextField();
        pulsanteConferma = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        ricerca = new javax.swing.JTextField();
        ricercaDispositivo = new javax.swing.JButton();
        errori = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        filtro = new javax.swing.JComboBox<>();

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Nuova Promozione");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Dispositivo", "Prezzo"
            }
        ));
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dim=(DefaultTableModel) jTable1.getModel();
        listaDispositivi=overClock.visualizzaListaDispositivi("");
        for(int i=listaDispositivi.size()-1;i>=0;i--){
            if(listaDispositivi.get(i).getTipo()==tipoArticolo.pezzo)
            listaDispositivi.remove(i);
            if(listaDispositivi.get(i).getQuantita()==0 && listaDispositivi.get(i).getReperibilita()==false)
            listaDispositivi.remove(i);
        }
        for(int i=0;i<listaDispositivi.size();i++){
            if(listaDispositivi.get(i).getQuantita()>0)
            dim.addRow(new Object[]{listaDispositivi.get(i).getNome(),listaDispositivi.get(i).getPrezzo()});
        }
        jTable1.setDefaultEditor(Object.class, null);
        jScrollPane1.setViewportView(jTable1);

        jLabel2.setText("Inserisci Sconto Percentuale per il Dispositivo");

        pulsanteConferma.setText("Conferma");
        pulsanteConferma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteConfermaActionPerformed(evt);
            }
        });

        jLabel3.setText("Selezionato");

        jLabel6.setText("Nome Dispositivo:");

        ricercaDispositivo.setText("Ricerca Dispositivo");
        ricercaDispositivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ricercaDispositivoActionPerformed(evt);
            }
        });

        errori.setForeground(new java.awt.Color(255, 0, 0));
        errori.setVisible(false);

        jLabel4.setText("Filtra per:");

        filtro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tutti", "Nuovi", "Usati - Come Nuovi", "Usati - Danni Leggeri", "Usati - Danni Evidenti" }));
        filtro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(sconto)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(pulsanteConferma))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGap(135, 135, 135)
                            .addComponent(ricercaDispositivo)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(filtro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(errori)))
                .addGap(56, 56, 56))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ricercaDispositivo))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(56, 56, 56)
                                .addComponent(errori))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4)
                                .addGap(5, 5, 5)
                                .addComponent(filtro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(sconto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pulsanteConferma)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ricercaDispositivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ricercaDispositivoActionPerformed
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        listaDispositivi = overClock.visualizzaListaDispositivi(ricerca.getText());
        for (int i = listaDispositivi.size() - 1; i >= 0; i--) {
            if (listaDispositivi.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo || listaDispositivi.get(i).getQuantita() == 0) {
                listaDispositivi.remove(i);
            }
        }
        for (int i = 0; i < listaDispositivi.size(); i++) {
            dim.addRow(new Object[]{listaDispositivi.get(i).getNome(), listaDispositivi.get(i).getPrezzo(), listaDispositivi.get(i).getQuantita()});
        }
    }//GEN-LAST:event_ricercaDispositivoActionPerformed

    private void pulsanteConfermaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteConfermaActionPerformed
        errori.setVisible(false);
        if (jTable1.getSelectedRow() != -1) {
            try {
                Float.parseFloat(sconto.getText());
                if (sconto.getText().equals("")) {
                    sconto.setBackground(new java.awt.Color(255, 153, 102));
                    errori.setText("Inserisci uno sconto valido");
                    errori.setVisible(true);
                } else {
                    sconto.setBackground(Color.white);
                    errori.setVisible(false);
                }
            } catch (NumberFormatException e) {
                sconto.setBackground(new java.awt.Color(255, 153, 102));
                errori.setText("Inserisci uno sconto valido");
                errori.setVisible(true);
            }
            if (!errori.isVisible()) {
                Dispositivo d = listaDispositivi.get(jTable1.getSelectedRow());
                promozioneDAO.add(d, Integer.parseInt(sconto.getText()));
                frameOperazioneRiuscita = new OperazioneRiuscita();
                Timer timer;
                timer = new Timer(1000, (ActionEvent e) -> {
                    frameOperazioneRiuscita.dispose();
                });
                timer.setRepeats(false);
                timer.start();
                frameOperazioneRiuscita.setVisible(true);
                frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                sconto.setText("");
            }
        } else {
            errori.setText("Seleziona un Articolo");
            errori.setVisible(true);
        }
    }//GEN-LAST:event_pulsanteConfermaActionPerformed

    private void filtroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filtroActionPerformed
        String s = overClock.getDanniFormattati(String.valueOf(filtro.getSelectedItem()));
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Dispositivo> listaDispositivo = overClock.visualizzaListaDispositivi(ricerca.getText());
        for (int i = listaDispositivo.size() - 1; i >= 0; i--) {
            if (listaDispositivo.get(i).getReperibilita() == false) {
                listaDispositivo.remove(i);
            } else if (s != "nuovo") {
                if (!listaDispositivo.get(i).getNome().contains(s)) {
                    listaDispositivo.remove(i);
                }
            } else {
                if (listaDispositivo.get(i).getNome().contains("danniLeggeri") || listaDispositivo.get(i).getNome().contains("danniEvidenti")) {
                    listaDispositivo.remove(i);
                }
            }
        }
        for (int i = 0; i < listaDispositivo.size(); i++) {
            if (listaDispositivo.get(i).getTipo() != tipoArticolo.pezzo) {
                dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
            }
        }
    }//GEN-LAST:event_filtroActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel errori;
    private javax.swing.JComboBox<String> filtro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton pulsanteConferma;
    private javax.swing.JTextField ricerca;
    private javax.swing.JButton ricercaDispositivo;
    private javax.swing.JTextField sconto;
    // End of variables declaration//GEN-END:variables
}
