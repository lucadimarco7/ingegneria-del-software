/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package classi;

/**
 *
 * @author Luca
 */
public class ScontoAlto extends Sconto {

    float prezzo = 100;

    @Override
    public float check(float spesa) {
        if (spesa < 1000) {
            return sconto.check(spesa);
        } else {
            return prezzo;
        }
    }
}
