package classi;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SchedaDiVendita {

    private OverClock overClock = OverClock.getIstanza();
    private int idVendita;
    private String data;
    private float importoTotale = 0;
    private List<RigaDiVendita> listaRigheDiVendita = new ArrayList<>();
    private Cliente cliente;
    private CartaFedelta cartaFedelta = null;

    public SchedaDiVendita() {

    }

    public SchedaDiVendita(int id, float totale, int idCarta, String data) {
        this.idVendita = id;
        this.importoTotale = totale;
        this.cartaFedelta = overClock.getCartaFedelta(idCarta);
        this.data = data;
    }

    public SchedaDiVendita(int id, float totale, String data) {
        this.idVendita = id;
        this.importoTotale = totale;
        this.cartaFedelta = null;
        this.data = data;
    }

    public void aggiungiDispositivo(Dispositivo dispositivo) {
        Boolean aggiunto = false;
        for (int i = 0; i < listaRigheDiVendita.size(); i++) {
            if (dispositivo.getCodice().equals(listaRigheDiVendita.get(i).getDispositivo().getCodice())) {
                this.importoTotale = this.importoTotale - listaRigheDiVendita.get(i).getSubTotale();
                listaRigheDiVendita.get(i).setQuantita(listaRigheDiVendita.get(i).getQuantita() + 1);
                this.importoTotale = this.importoTotale + listaRigheDiVendita.get(i).getSubTotale();
                aggiunto = true;
            }
        }
        if (!aggiunto) {
            RigaDiVendita rv = new RigaDiVendita(dispositivo);
            this.importoTotale = this.importoTotale + rv.getSubTotale();
            listaRigheDiVendita.add(rv);
        }
    }

    public void rimuoviDispositivo(String nome) {
        for (int i = 0; i < listaRigheDiVendita.size(); i++) {
            if (listaRigheDiVendita.get(i).getDispositivo().getNome().equals(nome)) {
                if (listaRigheDiVendita.get(i).getQuantita() > 1) {
                    listaRigheDiVendita.get(i).setQuantita(listaRigheDiVendita.get(i).getQuantita() - 1);
                    this.importoTotale = this.importoTotale - listaRigheDiVendita.get(i).getDispositivo().getPrezzo();
                } else {
                    this.importoTotale = this.importoTotale - listaRigheDiVendita.get(i).getSubTotale();
                    listaRigheDiVendita.remove(i);
                }
            }
        }
    }

    //------Metodi Get,Set e toString-------------------------
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setID(int iD) {
        this.idVendita = iD;
    }

    public List<RigaDiVendita> getListaRigheDiVendita() {
        return listaRigheDiVendita;
    }

    public String getData() {
        return data;
    }

    public float getTotale() {
        return importoTotale;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setTotale(float totale) {
        this.importoTotale = totale;
    }

    public void setListaRigheDiVendita(List<RigaDiVendita> listaRigheDiVendita) {
        this.listaRigheDiVendita = listaRigheDiVendita;
    }

    public CartaFedelta getCartaFedelta() {
        return this.cartaFedelta;
    }

    public void setCartaFedelta(CartaFedelta cartaFedelta) {
        this.cartaFedelta = cartaFedelta;
    }

    public int getID() {
        return this.idVendita;
    }

    public void setQuantita(Dispositivo dispositivo, int quantita) {
        for (int i = 0; i < listaRigheDiVendita.size(); i++) {
            if (listaRigheDiVendita.get(i).getDispositivo().getCodice().equals(dispositivo.getCodice())) {
                listaRigheDiVendita.get(i).setQuantita(quantita);
            }
        }
    }

}
