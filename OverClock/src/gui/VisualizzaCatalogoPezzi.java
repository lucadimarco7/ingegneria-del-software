package gui;

import classi.Cliente;
import classi.Dispositivo;
import classi.Dispositivo.tipoArticolo;
import classi.DispositivoDAO;
import classi.OverClock;
import classi.Prenotazione;
import classi.SchedaRiparazione;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

public class VisualizzaCatalogoPezzi extends javax.swing.JPanel {

    private OverClock overClock;
    private ModificaDispositivo frameModificaDispositivo;
    private DefaultTableModel dim;
    private OperazioneRiuscita frameOperazioneRiuscita = new OperazioneRiuscita();
    private DispositivoDAO DAO = new DispositivoDAO();

    public VisualizzaCatalogoPezzi(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        ricerca = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        elimina = new javax.swing.JButton();
        Modifica = new javax.swing.JButton();
        errore = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Catalogo Pezzi di Ricambio");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Dispositivo", "Marca", "Codice", "Prezzo", "Disponibilità"
            }
        ));
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        DefaultTableModel dim=(DefaultTableModel) jTable1.getModel();
        List<Dispositivo> listaDispositivo=overClock.visualizzaListaDispositivi("");
        for(int i=0;i<listaDispositivo.size();i++){
            if(listaDispositivo.get(i).getTipo()==tipoArticolo.pezzo&&listaDispositivo.get(i).getReperibilita()==true)
            dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(),listaDispositivo.get(i).getPrezzo(),listaDispositivo.get(i).getQuantita()});
        }
        jTable1.setDefaultEditor(Object.class, null);
        jScrollPane6.setViewportView(jTable1);

        ricerca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ricercaActionPerformed(evt);
            }
        });

        jButton1.setText("Ricerca Dispositivo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        elimina.setBackground(new java.awt.Color(255, 102, 102));
        elimina.setText("Escludi Pezzo Selezionato");
        elimina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminaActionPerformed(evt);
            }
        });

        Modifica.setText("Modifica");
        Modifica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificaActionPerformed(evt);
            }
        });

        errore.setVisible(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(elimina, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Modifica, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ricerca)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)))
                        .addGap(50, 50, 50))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(errore, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Modifica)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(elimina))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 397, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(errore, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ricercaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ricercaActionPerformed

    }//GEN-LAST:event_ricercaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Dispositivo> listaDispositivo = overClock.visualizzaListaDispositivi(ricerca.getText());
        for (int i = 0; i < listaDispositivo.size(); i++) {
            if (listaDispositivo.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo) {
                dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void eliminaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminaActionPerformed
        dim = (DefaultTableModel) jTable1.getModel();
        boolean flag = false;
        errore.setVisible(false);
        if (jTable1.getSelectedRow() != -1) {
            String codice = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 2);
            Dispositivo d = overClock.getDispositivo(codice);
            if (d.getQuantita() != 0) {
                errore.setText("Impossibile eliminare un dipositivo disponibile in magazzino");
                errore.setVisible(true);
            } else {
                List<SchedaRiparazione> l = overClock.RicercaRiparazione("");

                for (int i = 0; i < l.size(); i++) {
                    System.out.print(i + ":   :" + l.get(i).getID() + "\n");
                    List<Dispositivo> lp = l.get(i).getPezzi();
                    System.out.print(i + ":   :" + lp.size() + "\n");
                    for (int g = 0; g < lp.size(); g++) {
                        System.out.print("2=" + lp.get(g).getCodice());
                        if (lp.get(g).getCodice().equals(d.getCodice())) {
                            System.out.print("3=" + lp.get(g).getCodice());
                            errore.setText("Impossibile eliminare un pezzo con riparazioni in corso associati");
                            errore.setVisible(true);
                            flag = true;
                            break;
                        }

                    }
                }
                if (flag == false) {
                    DAO.updateReperibilita((String) jTable1.getValueAt(jTable1.getSelectedRow(), 2));
                    dim.removeRow(jTable1.getSelectedRow());
                    frameOperazioneRiuscita = new OperazioneRiuscita();
                    Timer timer;
                    timer = new Timer(1000, (ActionEvent e) -> {
                        frameOperazioneRiuscita.dispose();
                    });
                    timer.setRepeats(false);
                    timer.start();
                    frameOperazioneRiuscita.setVisible(true);
                    frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                }
            }
        }
    }//GEN-LAST:event_eliminaActionPerformed

    private void ModificaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificaActionPerformed
        if (jTable1.getSelectedRow() != -1) {
            String codice = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 2);
            Dispositivo d = overClock.getDispositivo(codice);
            frameModificaDispositivo = new ModificaDispositivo(overClock, d, this);
            frameModificaDispositivo.setVisible(true);
            frameModificaDispositivo.setDefaultCloseOperation(HIDE_ON_CLOSE);
            frameModificaDispositivo.pack();
        }
    }//GEN-LAST:event_ModificaActionPerformed
    public void update() {
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Dispositivo> listaDispositivo = overClock.visualizzaListaDispositivi("");
        for (int i = 0; i < listaDispositivo.size(); i++) {
            if (listaDispositivo.get(i).getTipo() == tipoArticolo.pezzo && listaDispositivo.get(i).getReperibilita() == true) {
                dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
            }
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Modifica;
    private javax.swing.JButton elimina;
    private javax.swing.JLabel errore;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField ricerca;
    // End of variables declaration//GEN-END:variables
}
