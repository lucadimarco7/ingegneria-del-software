/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package classi;

/**
 *
 * @author Luca
 */
public class ScontoBasso extends Sconto {

    float prezzo = 20;

    @Override
    public float check(float spesa) {
        if (spesa < 200) {
            return -1;
        } else {
            return prezzo;
        }
    }
}
