package classi;

import java.util.ArrayList;
import java.util.List;

public class Dispositivo {

    private String codice;
    private String nome;
    private String marca;
    private float prezzo = 0;
    private float prezzoAcquisto = 0;
    private int quantita;
    private boolean reperibilita;

    public enum tipoArticolo {
        pc,
        tablet,
        smartphone,
        pezzo
    }
    private final tipoArticolo tipo;
    private final Distributore distributore;
    private List<Observer> observers = new ArrayList<>();

    public void addObserver(Observer channel) {
        this.observers.add(channel);
    }

    public void removeObserver(Observer channel) {
        this.observers.remove(channel);
    }

    public Dispositivo(String nome, String codice, String marca, float prezzo, float prezzoAcquisto, int quantita, tipoArticolo tipo, Distributore distributore, Boolean reperibilita) {
        this.nome = nome;
        this.codice = codice;
        this.marca = marca;
        this.prezzo = prezzo;
        this.prezzoAcquisto = prezzoAcquisto;
        this.quantita = quantita;
        this.tipo = tipo;
        this.distributore = distributore;
        this.reperibilita = reperibilita;
    }

    public String getNome() {
        return this.nome;
    }

    public String getCodice() {
        return this.codice;
    }

    public String getMarca() {
        return this.marca;
    }

    public float getPrezzo() {
        return this.prezzo;
    }

    public float getPrezzoAcquisto() {
        return this.prezzoAcquisto;
    }

    public boolean getReperibilita() {
        return this.reperibilita;
    }

    public int getQuantita() {
        return this.quantita;
    }

    public tipoArticolo getTipo() {
        return this.tipo;
    }

    public Distributore getDistributore() {
        return this.distributore;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setReperibilita(boolean rep) {
        this.reperibilita = rep;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setPrezzo(float prezzo) {
        this.prezzo = prezzo;
    }

    public void setPrezzoAcquisto(float prezzo) {
        this.prezzoAcquisto = prezzo;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
        if (quantita > 0) {
            for (int i = 0; i < this.observers.size(); i++) {
                observers.get(i).update();
            }
        }
    }

}
