package gui;

import classi.Dispositivo;
import classi.DispositivoDAO;
import classi.Distributore;
import classi.OverClock;
import classi.PromozioneDAO;
import java.awt.Color;
import java.util.List;

public class ModificaDispositivo extends javax.swing.JFrame {

    private OverClock overclock;
    private Dispositivo d;
    private VisualizzaCatalogo vc = null;
    private VisualizzaCatalogoPezzi vp = null;
    private DispositivoDAO DAO = new DispositivoDAO();
    private List<Distributore> ld;
    private boolean c2 = false;

    public ModificaDispositivo(OverClock overclock, Dispositivo d, VisualizzaCatalogo vc) {
        this.overclock = overclock;
        this.d = d;
        this.vc = vc;
        initComponents();

    }

    public ModificaDispositivo(OverClock overclock, Dispositivo d, VisualizzaCatalogoPezzi vp) {
        this.overclock = overclock;
        this.d = d;
        this.vp = vp;
        initComponents();

    }

    private ModificaDispositivo() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        codice = new javax.swing.JTextField();
        nome = new javax.swing.JTextField();
        marca = new javax.swing.JTextField();
        prezzo = new javax.swing.JTextField();
        quantita = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        ld=overclock.RicercaDistributori("");
        String[] distributori;
        if(d.getDistributore()!=null){
            distributori=new String[ld.size()];
            distributori[0]=d.getDistributore().getNome();
            int a=0;
            for(int i=0;i<ld.size();i++){
                if(ld.get(i).getNome().equals(distributori[0]))
                {}
                else{
                    distributori[a+1]=ld.get(i).getNome();
                    a++;  }
            }
        }

        else {
            distributori=new String[ld.size()+1];
            distributori[0]="Nessuno";
            int a=0;
            for(int i=0;i<ld.size();i++){

                distributori[a+1]=ld.get(i).getNome();
                a++;
            }
        }
        selezionaDistributore = new javax.swing.JComboBox<>();
        erroreGenerale = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        prezzoAcquisto = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Codice");

        jLabel2.setText("Nome");

        jLabel3.setText("Marca");

        jLabel4.setText("Prezzo");

        jLabel5.setText("Quantita");

        codice.setEditable(false);
        codice.setText(d.getCodice());

        nome.setEditable(false);
        nome.setText(d.getNome());

        marca.setEditable(false);
        marca.setText(d.getMarca());

        prezzo.setText(""+d.getPrezzo());
        prezzo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prezzoActionPerformed(evt);
            }
        });
        if(overclock.getPromozioneby(d.getCodice())){
            prezzo.setEnabled(false);
            c2=true;
        }
        else{
            c2=false;
            prezzo.setEnabled(true);}

        quantita.setText(""+d.getQuantita());

        jButton1.setText("Conferma Modifiche");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConfermaActionPerformed(evt);
            }
        });

        jLabel7.setText("Distributore");

        if(distributori!=null)
        selezionaDistributore.setModel(new javax.swing.DefaultComboBoxModel<>
            (distributori));

        erroreGenerale.setForeground(new java.awt.Color(255, 0, 0));
        erroreGenerale.setText(null);
        erroreGenerale.setVisible(false);

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel6.setText("Modifica Dispositivo");

        prezzoAcquisto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prezzoAcquistoActionPerformed(evt);
            }
        });

        jLabel8.setText("Prezzo Acquisto");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addGap(82, 82, 82)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nome)
                            .addComponent(marca)
                            .addComponent(codice)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(0, 364, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(erroreGenerale, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7))
                        .addGap(57, 57, 57)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(quantita)
                            .addComponent(selezionaDistributore, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(prezzoAcquisto)
                            .addComponent(prezzo))))
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel6)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel1))
                    .addComponent(codice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel2))
                    .addComponent(nome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel3))
                    .addComponent(marca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel4))
                    .addComponent(prezzo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(prezzoAcquisto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel5))
                    .addComponent(quantita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel7))
                    .addComponent(selezionaDistributore, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(erroreGenerale))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        prezzoAcquisto.setText(""+d.getPrezzoAcquisto());

        setSize(new java.awt.Dimension(716, 446));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void ConfermaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConfermaActionPerformed
        float oldPrezzo = d.getPrezzo();
        float oldPrezzoAcquisto = d.getPrezzoAcquisto();
        int oldQuantita = d.getQuantita();

        boolean mod = false;
        boolean okPrezzo = true;
        boolean okPrezzoA = true;
        boolean okQuantita = true;
        String oldDistributore = null;
        if (null != d.getDistributore()) {
            oldDistributore = d.getDistributore().getNome();
        } else {
            oldDistributore = "Nessuno";
        }
        List<Distributore> currentd = null;
        boolean m1, m2, m3, m4, c1 = false, c3 = false;
        erroreGenerale.setVisible(false);

        try {
            Integer.parseInt(quantita.getText());
            if (quantita.getText().equals("") || Integer.parseInt(quantita.getText()) < 0) {
                quantita.setBackground(new java.awt.Color(255, 153, 102));
            } else {
                quantita.setBackground(Color.white);
                c1 = true;
            }
        } catch (Exception e) {
            quantita.setBackground(new java.awt.Color(255, 153, 102));
            okQuantita = false;
        }

        try {
            Float.parseFloat(prezzo.getText());
            if (prezzo.getText().equals("") || Float.parseFloat(prezzo.getText()) < 0) {
                prezzo.setBackground(new java.awt.Color(255, 153, 102));
            } else {
                prezzo.setBackground(Color.white);
                c2 = true;
            }
        } catch (Exception e) {
            prezzo.setBackground(new java.awt.Color(255, 153, 102));
            okPrezzo = false;
        }

        try {
            Float.parseFloat(prezzoAcquisto.getText());
            if (prezzoAcquisto.getText().equals("") || Float.parseFloat(prezzoAcquisto.getText()) < 0) {
                prezzoAcquisto.setBackground(new java.awt.Color(255, 153, 102));
            } else {
                prezzoAcquisto.setBackground(Color.white);
                c3 = true;
            }
        } catch (Exception e) {
            prezzoAcquisto.setBackground(new java.awt.Color(255, 153, 102));
            okPrezzoA = false;
        }

        if (quantita.getText().length() != 0 && okQuantita == true) {
            if (Integer.parseInt(quantita.getText()) == oldQuantita) {
                m1 = false;
            } else {
                m1 = true;
            }
        } else {
            m1 = true;
        }

        if (prezzo.getText().length() != 0 && okPrezzo == true) {
            if (Float.parseFloat(prezzo.getText()) == oldPrezzo) {
                m2 = false;
            } else {
                m2 = true;
            }
        } else {
            m2 = true;
        }

        if (prezzoAcquisto.getText().length() != 0 && okPrezzoA == true) {
            if (Float.parseFloat(prezzoAcquisto.getText()) == oldPrezzoAcquisto) {
                m4 = false;
            } else {
                m4 = true;
            }
        } else {
            m4 = true;
        }

        if (String.valueOf(selezionaDistributore.getSelectedItem()).equals(oldDistributore)) {
            m3 = false;
        } else {
            m3 = true;
            currentd = overclock.RicercaDistributori(String.valueOf(selezionaDistributore.getSelectedItem()));
        }

        if (c1 == false || c2 == false || c3 == false) {

            if (c3 == false) {
                erroreGenerale.setText("Formato prezzo Acquisto inserito errato");
                erroreGenerale.setVisible(true);
            }
            if (c2 == false) {
                erroreGenerale.setText("Formato prezzo inserito errato");
                erroreGenerale.setVisible(true);
            }
            if (c1 == false) {
                erroreGenerale.setText("Formato quantita inserito errato");
                erroreGenerale.setVisible(true);
            }
            if (c1 == false && c3 == false) {
                erroreGenerale.setText("I formati quantita e prezzo Acquisto inseriti sono errati");
                erroreGenerale.setVisible(true);
            }
            if (c1 == false && c2 == false) {
                erroreGenerale.setText("I formati prezzo e quantita inseriti sono errati");
                erroreGenerale.setVisible(true);
            }
            if (c3 == false && c2 == false) {
                erroreGenerale.setText("I formati prezzo Acquisto e prezzo inseriti sono errati");
                erroreGenerale.setVisible(true);
            }

            if (c1 == false && c2 == false && c3 == false) {
                erroreGenerale.setText("I formati prezzo, prezzo acquisto e quantita inseriti sono errati");
                erroreGenerale.setVisible(true);
            }
            c2 = false;
        } else {
            if (m1 == true && c1 == true) {
                d.setQuantita(Integer.parseInt(quantita.getText()));
                DAO.updateQuantita(d.getQuantita(), d.getCodice());
                mod = true;
            }

            if (m2 == true && c2 == true) {
                DAO.updatePrezzo(Float.parseFloat(prezzo.getText()), d.getCodice());
                mod = true;
            }

            if (m3 == true) {
                DAO.updateDistributore2(currentd.get(0).getId(), d.getCodice());
                mod = true;
                if (oldDistributore.equalsIgnoreCase("Nessuno")) {
                    DAO.updateReperibilitaTrue(d.getCodice());
                }

            }

            if (m4 == true && c3 == true) {
                DAO.updatePrezzoAcquisto(Float.parseFloat(prezzoAcquisto.getText()), d.getCodice());
                mod = true;
            }
        }
        if (m1 == false && m2 == false && m4 == false) {
            erroreGenerale.setText("nessuna modifica inserita");
            erroreGenerale.setVisible(true);
        }

        if (mod == true) {
            this.dispose();
            if (vc != null) {
                vc.update();
            } else {
                vp.update();
            }
        }
    }//GEN-LAST:event_ConfermaActionPerformed

    private void prezzoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prezzoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_prezzoActionPerformed

    private void prezzoAcquistoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prezzoAcquistoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_prezzoAcquistoActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ModificaDispositivo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ModificaDispositivo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ModificaDispositivo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ModificaDispositivo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ModificaDispositivo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField codice;
    private javax.swing.JLabel erroreGenerale;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField marca;
    private javax.swing.JTextField nome;
    private javax.swing.JTextField prezzo;
    private javax.swing.JTextField prezzoAcquisto;
    private javax.swing.JTextField quantita;
    private javax.swing.JComboBox<String> selezionaDistributore;
    // End of variables declaration//GEN-END:variables
}
