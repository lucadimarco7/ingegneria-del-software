package classi;

import classi.SchedaRiparazione.statoRiparazioni;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SchedaRiparazioneDAO implements InterfacciaDAO {

    private OverClock overClock = OverClock.getIstanza();
    float totale = 0;
    Connection connection = null;
    PreparedStatement ptmt = null;
    PreparedStatement ptmt2 = null;
    PreparedStatement ptmt3 = null;
    ResultSet resultSet = null;
    private DispositivoDAO dispositivoDAO = new DispositivoDAO();
    private SchedaOrdineDAO schedaOrdineDAO = new SchedaOrdineDAO();
    private String data;

    public SchedaRiparazioneDAO() {
    }

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = Connessione.getInstance().getConnection();
        return conn;
    }

    public void add(Cliente cliente, Dispositivo dispositivo) {
        DispositivoDAO dispositivoDAO = new DispositivoDAO();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        this.data = dtf.format(now);
        int id = -1;
        for (int i = 0; i < overClock.getIstanzaRiparazioni().size(); i++) {
            if (i != overClock.getIstanzaRiparazioni().get(i).getID()) {
                id = i;
                break;
            }
        }
        if (id == -1) {
            id = overClock.getIstanzaRiparazioni().size();
        }
        try {
            String queryString = "INSERT INTO riparazione(id,cliente,dispositivo,stato,costo,data) VALUES(?,?,?,?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            ptmt.setString(2, cliente.getNumero());
            ptmt.setString(3, dispositivo.getCodice());
            ptmt.setString(4, "preventivoInCorso");
            ptmt.setInt(5, 0);
            ptmt.setString(6, data);
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
            totale = 0;
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void update(SchedaRiparazione s, statoRiparazioni stato, List<Dispositivo> pezzi, float costo) {
        System.out.println("Entra qua");
        for (int i = 0; i < overClock.getIstanzaRiparazioni().size(); i++) {
            if (overClock.getIstanzaRiparazioni().get(i).getCliente() == s.getCliente() && overClock.getIstanzaRiparazioni().get(i).getDispositivo() == s.getDispositivo()) {
                overClock.getIstanzaRiparazioni().get(i).aggiornamentoRiparazione(pezzi, stato, costo);
            }
        }
        String stringa = overClock.getStatoStringa(stato);
        try {
            String queryString = "UPDATE riparazione SET stato=?, costo=? WHERE id=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, stringa);
            ptmt.setFloat(2, s.getCosto());
            ptmt.setInt(3, s.getID());
            ptmt.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
        for (int i = 0; i < pezzi.size(); i++) {
            Boolean aggiunto = false;
            try {
                String queryString = "SELECT * FROM pezzo";
                connection = getConnection();
                ptmt = connection.prepareStatement(queryString);
                resultSet = ptmt.executeQuery();
                while (resultSet.next()) {
                    if (resultSet.getString("nome").equals(pezzi.get(i).getNome()) && resultSet.getInt("idRiparazione") == s.getID()) {
                        String queryString2 = "UPDATE pezzo SET quantita=quantita+1 WHERE nome=? AND idRiparazione=?";
                        connection = getConnection();
                        ptmt2 = connection.prepareStatement(queryString2);
                        ptmt2.setString(1, pezzi.get(i).getNome());
                        ptmt2.setInt(2, s.getID());
                        ptmt2.executeUpdate();
                        System.out.println("Data Added Successfully");
                        aggiunto = true;
                    }
                }
                if (!aggiunto) {
                    String queryString3 = "INSERT INTO pezzo(idRiparazione,nome,quantita,subTotale) VALUES(?,?,?,?)";
                    connection = getConnection();
                    ptmt3 = connection.prepareStatement(queryString3);
                    ptmt3.setInt(1, s.getID());
                    ptmt3.setString(2, pezzi.get(i).getNome());
                    ptmt3.setInt(3, 1);
                    ptmt3.setFloat(4, pezzi.get(i).getPrezzo());
                    ptmt3.executeUpdate();
                    System.out.println("Data Added Successfully");
                }
            } catch (SQLException e) {
            } finally {
                try {
                    if (ptmt != null) {
                        ptmt.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                } catch (Exception e) {
                }
            }
        }
        findAll();
    }

    public void delete(SchedaRiparazione s) {
        for (int i = 0; i < overClock.getIstanzaRiparazioni().size(); i++) {
            if (overClock.getIstanzaRiparazioni().get(i).getCliente() == s.getCliente() && overClock.getIstanzaRiparazioni().get(i).getDispositivo() == s.getDispositivo()) {
                overClock.getIstanzaRiparazioni().remove(i);
            }
        }
        try {
            String queryString = "DELETE FROM riparazione WHERE id=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, s.getID());
            ptmt.executeUpdate();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
        findAll();
    }

    public void findAll() {
        List<SchedaRiparazione> l = new ArrayList();
        try {
            String queryString = "SELECT * FROM riparazione";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                Cliente c = overClock.getCliente(resultSet.getString("cliente"));
                Dispositivo d = overClock.getDispositivo(resultSet.getString("dispositivo"));
                statoRiparazioni stato = overClock.getStato(resultSet.getString("stato"));
                SchedaRiparazione sr = new SchedaRiparazione(resultSet.getInt("id"), c, d, stato, resultSet.getFloat("costo"), resultSet.getString("data"));
                l.add(sr);

            }
            overClock.setRiparazioni(l);

            for (int i = 0; i < l.size(); i++) {
                l.get(i).setPezzi(findPezzi(l.get(i).getID()));
            }

        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public List<Dispositivo> findPezzi(int id) {
        List<Dispositivo> l = new ArrayList();
        try {
            String queryString = "SELECT * FROM pezzo WHERE idRiparazione=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                Dispositivo d = overClock.getDispositivoByNome(resultSet.getString("nome"));
                l.add(d);
            }
            return l;
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
        return null;
    }

    public int findQuantita(int id, String nome) {
        try {
            String queryString = "SELECT quantita FROM pezzo WHERE idRiparazione=? AND nome=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            ptmt.setString(2, nome);
            resultSet = ptmt.executeQuery();
            int quantita = -1;
            while (resultSet.next()) {
                quantita = resultSet.getInt("quantita");
            }
            return quantita;
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
        return -1;
    }

}
