package gui;

import classi.CartaFedeltaDAO;
import classi.Cliente;
import classi.ClienteDAO;
import classi.OverClock;
import classi.Prenotazione;
import classi.PrenotazioneDAO;
import classi.SchedaAcquistoDAO;
import classi.SchedaDiVenditaDAO;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

public class VisualizzaClienti extends javax.swing.JPanel {

    private OverClock overClock;
    private DefaultTableModel dim;
    private OperazioneRiuscita frameOperazioneRiuscita;
    private ClienteDAO clienteDAO = new ClienteDAO();
    private CartaFedeltaDAO cartaDAO = new CartaFedeltaDAO();
    private SchedaAcquistoDAO acquistoDAO = new SchedaAcquistoDAO();
    private SchedaDiVenditaDAO venditaDAO = new SchedaDiVenditaDAO();
    private PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();
    private ModificaCliente frameModificaCliente;

    public VisualizzaClienti(OverClock overclock) {
        this.overClock = overclock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ricerca = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        pulsanteElimina = new javax.swing.JButton();
        errori = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        jButton1.setText("Ricerca Cliente");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Clienti");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Cognome", "Email", "Numero", "Carta Fedeltà"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        DefaultTableModel dim=(DefaultTableModel) jTable1.getModel();
        List<Cliente> listaClienti=overClock.visualizzaClienti("");
        String idCarta="";
        for(int i=0;i<listaClienti.size();i++){
            if(listaClienti.get(i).getCartaFedelta()!=null)
            idCarta=String.valueOf(listaClienti.get(i).getCartaFedelta().getID());
            else
            idCarta="Nessuna";
            dim.addRow(new Object[]{listaClienti.get(i).getNome(), listaClienti.get(i).getCognome(),
                listaClienti.get(i).getEmail(), listaClienti.get(i).getNumero(),idCarta});
    }
    jTable1.setDefaultEditor(Object.class, null);
    jScrollPane1.setViewportView(jTable1);
    if (jTable1.getColumnModel().getColumnCount() > 0) {
        jTable1.getColumnModel().getColumn(0).setResizable(false);
        jTable1.getColumnModel().getColumn(1).setResizable(false);
        jTable1.getColumnModel().getColumn(2).setResizable(false);
        jTable1.getColumnModel().getColumn(3).setResizable(false);
        jTable1.getColumnModel().getColumn(4).setResizable(false);
    }

    pulsanteElimina.setBackground(new java.awt.Color(255, 102, 102));
    pulsanteElimina.setText("Elimina Cliente Selezionato");
    pulsanteElimina.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            pulsanteEliminaActionPerformed(evt);
        }
    });

    errori.setForeground(new java.awt.Color(255, 0, 0));
    errori.setText("Errori");
    errori.setVisible(false);

    jButton2.setText("Modifica Cliente Selezionato");
    jButton2.setToolTipText("");
    jButton2.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            ModificaActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addGap(50, 50, 50)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(errori)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(layout.createSequentialGroup()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(ricerca)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButton1))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 379, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(pulsanteElimina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGap(50, 50, 50))))
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addGap(20, 20, 20)
            .addComponent(jLabel1)
            .addGap(35, 35, 35)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jButton1))
            .addGap(18, 18, 18)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jButton2)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(pulsanteElimina)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(errori)
            .addGap(20, 20, 20))
    );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Cliente> listaClienti = overClock.visualizzaClienti(ricerca.getText());
        for (int i = 0; i < listaClienti.size(); i++) {
            dim.addRow(new Object[]{listaClienti.get(i).getNome(), listaClienti.get(i).getCognome(),
                listaClienti.get(i).getEmail(), listaClienti.get(i).getNumero(),
                listaClienti.get(i).getCartaFedelta().getID()});
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void pulsanteEliminaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteEliminaActionPerformed
        dim = (DefaultTableModel) jTable1.getModel();
        errori.setVisible(false);
        if (jTable1.getSelectedRow() != -1) {
            String numero = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 3);
            System.out.print(numero);
            Cliente cliente = overClock.getCliente(numero);
            if (overClock.getRiparazioni(cliente.getEmail()).size() == 0) {
                if (cliente.getCartaFedelta() == null) {
                    List<Prenotazione> l = overClock.visualizzaPrenotazioni(cliente.getEmail());
                    if (l != null) {
                        for (int i = 0; i < l.size(); i++) {
                            prenotazioneDAO.delete(l.get(i).getID());
                        }
                    }
                    clienteDAO.delete(numero);
                    dim.removeRow(jTable1.getSelectedRow());
                    frameOperazioneRiuscita = new OperazioneRiuscita();
                    Timer timer;
                    timer = new Timer(1000, (ActionEvent e) -> {
                        frameOperazioneRiuscita.dispose();
                    });
                    timer.setRepeats(false);
                    timer.start();
                    frameOperazioneRiuscita.setVisible(true);
                    frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                } else {
                    cartaDAO.delete(cliente.getCartaFedelta().getID());
                    venditaDAO.setID(cliente.getCartaFedelta().getID());
                    acquistoDAO.setID(cliente.getCartaFedelta().getID());
                    List<Prenotazione> l = overClock.visualizzaPrenotazioni(cliente.getEmail());
                    if (l != null) {
                        for (int i = 0; i < l.size(); i++) {
                            prenotazioneDAO.delete(l.get(i).getID());
                        }
                    }
                    clienteDAO.delete(numero);
                    dim.removeRow(jTable1.getSelectedRow());
                    frameOperazioneRiuscita = new OperazioneRiuscita();
                    Timer timer;
                    timer = new Timer(1000, (ActionEvent e) -> {
                        frameOperazioneRiuscita.dispose();
                    });
                    timer.setRepeats(false);
                    timer.start();
                    frameOperazioneRiuscita.setVisible(true);
                    frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                }

            } else {
                errori.setVisible(true);
                errori.setText("Impossibile eliminare il Cliente poichè ha Riparazioni in sospeso");
            }
        }
    }//GEN-LAST:event_pulsanteEliminaActionPerformed
    public void update() {
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Cliente> listaClienti = overClock.visualizzaClienti("");
        for (int i = 0; i < listaClienti.size(); i++) {
            if (listaClienti.get(i).getCartaFedelta() == null) {
                dim.addRow(new Object[]{listaClienti.get(i).getNome(), listaClienti.get(i).getCognome(),
                    listaClienti.get(i).getEmail(), listaClienti.get(i).getNumero(),
                    "Nessuna"});
            } else {
                dim.addRow(new Object[]{listaClienti.get(i).getNome(), listaClienti.get(i).getCognome(),
                    listaClienti.get(i).getEmail(), listaClienti.get(i).getNumero(),
                    listaClienti.get(i).getCartaFedelta().getID()});
            }
        }
    }
    private void ModificaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificaActionPerformed
        if (jTable1.getSelectedRow() != -1) {
            String numero = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 3);
            Cliente cliente = overClock.getCliente(numero);
            frameModificaCliente = new ModificaCliente(overClock, cliente, this);
            frameModificaCliente.setVisible(true);
            frameModificaCliente.setDefaultCloseOperation(HIDE_ON_CLOSE);
            frameModificaCliente.pack();
        }
    }//GEN-LAST:event_ModificaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel errori;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton pulsanteElimina;
    private javax.swing.JTextField ricerca;
    // End of variables declaration//GEN-END:variables
}
