package classi;

public class RigaDiVendita {

    private int quantita = 1;
    private Dispositivo dispositivo;
    private float subTotale;

    public RigaDiVendita(Dispositivo dispositivo) {
        this.dispositivo = dispositivo;
        this.subTotale = dispositivo.getPrezzo() * quantita;
    }

    public RigaDiVendita(Dispositivo dispositivo, float tot) {
        this.dispositivo = dispositivo;
        this.subTotale = tot;
    }

    public float getSubTotale() {
        return subTotale;
    }

    public int getQuantita() {
        return quantita;
    }

    public Dispositivo getDispositivo() {
        return dispositivo;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
        this.subTotale = dispositivo.getPrezzo() * quantita;
    }

    public void setDispositivo(Dispositivo dispositivo) {
        this.dispositivo = dispositivo;
    }

}
