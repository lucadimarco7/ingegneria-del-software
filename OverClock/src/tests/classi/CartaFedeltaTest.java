package tests.classi;

import classi.CartaFedelta;
import org.junit.Test;
import static org.junit.Assert.*;

public class CartaFedeltaTest {

    CartaFedelta cf = new CartaFedelta(10, 100);

    public CartaFedeltaTest() {
    }

    @Test
    public void testGetSaldoPunti() {
        int expResult = 100;
        assertEquals(expResult, cf.getSaldoPunti());
    }

    @Test
    public void testSetPunti() {
        int expResult = 0;
        cf.setPunti(expResult);
        assertEquals(expResult, cf.getSaldoPunti());
    }

    @Test
    public void testGetID() {
        int expResult = 10;
        assertEquals(expResult, cf.getID());
    }

    @Test
    public void testSetID() {
        int expResult = 0;
        cf.setID(expResult);
        assertEquals(expResult, cf.getID());
    }

}
