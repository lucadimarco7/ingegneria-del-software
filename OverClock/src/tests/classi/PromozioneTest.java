package tests.classi;

import classi.Dispositivo;
import classi.Distributore;
import classi.Promozione;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Test;
import static org.junit.Assert.*;

public class PromozioneTest {

    Distributore distributore = new Distributore(0, "apple", "apple@gmail.it");
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    String data = dtf.format(now);
    Promozione p = new Promozione(0, d, 10, data);

    public PromozioneTest() {

    }

    @Test
    public void testGetID() {
        int expResult = 0;
        int result = p.getID();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDispostivo() {
        Dispositivo expResult = d;
        Dispositivo result = p.getDispostivo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetSconto() {
        int expResult = 10;
        int result = p.getSconto();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetData() {
        String expResult = data;
        String result = p.getData();
        assertEquals(expResult, result);
    }

}
