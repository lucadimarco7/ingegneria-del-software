package gui;

import classi.CartaFedelta;
import classi.CartaFedeltaDAO;
import classi.Dispositivo;
import classi.Dispositivo.tipoArticolo;
import classi.DispositivoDAO;
import classi.OverClock;
import classi.SchedaDiVendita;
import classi.SchedaDiVenditaDAO;
import classi.ScontoAlto;
import classi.ScontoBasso;
import classi.ScontoMedio;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

public class InserisciVendita extends javax.swing.JPanel {

    private OverClock overClock;
    private DispositivoDAO dispositivoDAO = new DispositivoDAO();
    private CartaFedeltaDAO cartaDAO = new CartaFedeltaDAO();
    private SchedaDiVenditaDAO schedaDiVenditaDAO = new SchedaDiVenditaDAO();
    List<Dispositivo> listaDispositivi;
    SchedaDiVendita schedaDiVendita = new SchedaDiVendita();
    private float importoTotale = 0;
    String idCarta = "-1";
    private IDNonValido frameIDNonValido;
    private OperazioneRiuscita frameOperazioneRiuscita;
    private Sconto frameSconto;
    DefaultTableModel dim;
    DefaultTableModel dim2;
    CartaFedelta carta = null;
    int sconto = 0;
    boolean scontoApplicato = false;
    private ScontoBasso scontoBasso = new ScontoBasso();
    private ScontoMedio scontoMedio = new ScontoMedio();
    private ScontoAlto scontoAlto = new ScontoAlto();

    public InserisciVendita(OverClock overClock) {
        this.overClock = overClock;
        scontoAlto.setSuccessivo(scontoMedio);
        scontoMedio.setSuccessivo(scontoBasso);
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        Remove = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        IDCarta = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cartaInserita = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        pulsanteApplicaSconto = new javax.swing.JButton();
        pulsanteAssociaCarta = new javax.swing.JButton();
        aggiungiAlCarrello = new javax.swing.JButton();
        ricerca = new javax.swing.JTextField();
        ricercaDispositivo = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListDisponibilita = new javax.swing.JList<>();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        pulsanteConfermaVendita = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        totale = new javax.swing.JTextField();
        usatiCheckBox = new javax.swing.JCheckBox();
        jLabel9 = new javax.swing.JLabel();

        setAutoscrolls(true);

        jLabel6.setText("Nome Dispositivo:");

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Dispositivo", "Prezzo", "Quantita"
            }
        ));
        jTable2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable2.setDefaultEditor(Object.class, null);
        jScrollPane1.setViewportView(jTable2);

        Remove.setBackground(new java.awt.Color(255, 102, 102));
        Remove.setText("Rimuovi Articolo Selezionato");
        Remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RemoveActionPerformed(evt);
            }
        });

        jLabel5.setText("ID Carta Fedeltà:");

        jLabel4.setText("Carrello");

        cartaInserita.setBackground(new java.awt.Color(255, 153, 102));
        cartaInserita.setText("Nessuna");
        cartaInserita.setEditable(false);

        jLabel3.setText("Carta Fedeltà Inserita:");

        pulsanteApplicaSconto.setText("Applica Sconto");
        pulsanteApplicaSconto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteApplicaScontoActionPerformed(evt);
            }
        });

        pulsanteAssociaCarta.setText("Associa Carta Fedeltà");
        pulsanteAssociaCarta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteAssociaCartaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1)
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addComponent(cartaInserita)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(pulsanteApplicaSconto))
                        .addComponent(jLabel3)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(IDCarta, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pulsanteAssociaCarta))))
                    .addComponent(Remove, javax.swing.GroupLayout.Alignment.TRAILING)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(IDCarta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pulsanteAssociaCarta))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cartaInserita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pulsanteApplicaSconto))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                        .addComponent(Remove))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        aggiungiAlCarrello.setBackground(new java.awt.Color(204, 255, 102));
        aggiungiAlCarrello.setText("Aggiungi al Carrello");
        aggiungiAlCarrello.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aggiungiAlCarrelloActionPerformed(evt);
            }
        });

        ricercaDispositivo.setText("Ricerca Dispositivo");
        ricercaDispositivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ricercaDispositivoActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Inserisci Vendita");

        DefaultListModel dim2=new DefaultListModel();
        List<Dispositivo> listaDispositivi2=overClock.visualizzaListaDispositivi("");
        for(int i=0;i<listaDispositivi2.size();i++){
            dim2.addElement(listaDispositivi2.get(i).getQuantita());
        }
        jListDisponibilita.setModel(dim2);
        jScrollPane3.setViewportView(jListDisponibilita);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Dispositivo", "Prezzo", "Disponibilità"
            }
        ));
        jTable1.setAutoscrolls(false);
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dim=(DefaultTableModel) jTable1.getModel();
        listaDispositivi=overClock.visualizzaListaDispositivi("");
        for(int i=listaDispositivi.size()-1;i>=0;i--)
        if(listaDispositivi.get(i).getTipo()==tipoArticolo.pezzo||listaDispositivi.get(i).getQuantita()==0)
        listaDispositivi.remove(i);
        for(int i=0;i<listaDispositivi.size();i++){
            dim.addRow(new Object[]{listaDispositivi.get(i).getNome(),listaDispositivi.get(i).getPrezzo(),listaDispositivi.get(i).getQuantita()});
        }
        jTable1.setDefaultEditor(Object.class, null);
        jScrollPane5.setViewportView(jTable1);

        jLabel2.setText("Totale");

        pulsanteConfermaVendita.setText("Conferma Vendita");
        pulsanteConfermaVendita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteConfermaVenditaActionPerformed(evt);
            }
        });

        jLabel7.setForeground(new java.awt.Color(255, 0, 0));
        jLabel7.setText("La Scheda di Vendita è Vuota");
        jLabel7.setVisible(false);

        jLabel8.setForeground(new java.awt.Color(255, 0, 0));
        jLabel8.setText("Articolo non Disponibile");
        jLabel8.setVisible(false);

        totale.setEditable(false);
        totale.setText("0.0");
        totale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                totaleActionPerformed(evt);
            }
        });

        usatiCheckBox.setText("Visualizza Soltanto Usati");
        usatiCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usatiCheckBoxActionPerformed(evt);
            }
        });

        jLabel9.setForeground(new java.awt.Color(0, 106, 0));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane5)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(170, 170, 170)
                                .addComponent(ricercaDispositivo))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(aggiungiAlCarrello, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel8)
                                    .addComponent(usatiCheckBox)))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(totale, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pulsanteConfermaVendita))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ricercaDispositivo))
                        .addGap(18, 18, 18)
                        .addComponent(aggiungiAlCarrello)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(usatiCheckBox)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(pulsanteConfermaVendita)
                            .addComponent(jLabel7)
                            .addComponent(totale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void RemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RemoveActionPerformed
        if (jTable2.getSelectedRow() != -1) {//se c'è una riga selezionata nella tabella del carrello
            if (Integer.parseInt(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 2))) > 1) {//se la sua quantità è maggiore di uno decrementala
                schedaDiVendita.rimuoviDispositivo(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 0)));
                dim2.setValueAt(Integer.parseInt(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 2))) - 1, jTable2.getSelectedRow(), 2);
            } else {//altrimenti rimuovi la riga
                schedaDiVendita.rimuoviDispositivo(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 0)));
                dim2.removeRow(jTable2.getSelectedRow());
            }
            importoTotale = schedaDiVendita.getTotale();//ricalcola il totale
            float sconto = scontoAlto.check(importoTotale);
            if (sconto != -1) {
                importoTotale = importoTotale - sconto;
                jLabel9.setText("Sconto di " + sconto + " € applicato su spesa minima");
            } else {
                jLabel9.setText("Spesa minima troppo bassa per applicare uno sconto");
            }
            importoTotale = ((int) ((importoTotale + 0.005f) * 100)) / 100f;//approssima il totale alla seconda cifra decimale
            totale.setText(String.valueOf(importoTotale));
        }
    }//GEN-LAST:event_RemoveActionPerformed

    private void pulsanteConfermaVenditaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteConfermaVenditaActionPerformed
        if (dim2 != null) {//se la tabella del carrello non è vuota
            importoTotale = ((int) ((importoTotale + 0.005f) * 100)) / 100f;//approssima il totale alla seconda cifra decimale
            schedaDiVendita.setTotale(importoTotale);//imposta il totale della scheda di vendita
            for (int i = 0; i < dim2.getRowCount(); i++) {//scorri le righe del carrello e prendi i nomi e le quantita
                String nome = String.valueOf(dim2.getValueAt(i, 0));
                int quantita = Integer.parseInt(String.valueOf(dim2.getValueAt(i, 2)));
                List<Dispositivo> dispositivi = overClock.visualizzaListaDispositivi(nome);
                if (!dispositivi.isEmpty()) {//scala la quantità dal magazzino
                    dispositivoDAO.update(nome, dispositivi.get(0).getQuantita() - quantita);
                }
            }
            if (dim2.getRowCount() > 0) {
                int punti = (int) Math.floor(importoTotale);//i punti vengono calcolati per difetto dal totale
                try {//se era stata associata una carta fedeltà aggiorna il saldo
                    if (!idCarta.equals("-1")) {
                        overClock.aggiornaSaldo(idCarta, punti - sconto * 10);
                        cartaDAO.update(carta);
                    } else {
                        carta = null;
                    }
                    schedaDiVenditaDAO.add(schedaDiVendita.getListaRigheDiVendita(), Integer.parseInt(idCarta), importoTotale);//salva la scheda di vendita
                } catch (IOException ex) {
                    Logger.getLogger(InserisciVendita.class.getName()).log(Level.SEVERE, null, ex);
                }
                frameOperazioneRiuscita = new OperazioneRiuscita();
                Timer timer;
                timer = new Timer(1000, (ActionEvent e) -> {
                    frameOperazioneRiuscita.dispose();
                });
                timer.setRepeats(false);
                timer.start();
                frameOperazioneRiuscita.setVisible(true);
                frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                //resetta tutte le variabili
                dim2.setRowCount(0);
                importoTotale = 0;
                totale.setText("0");
                idCarta = "-1";
                sconto = 0;
                scontoApplicato = false;
                cartaInserita.setText("Nessuna");
                cartaInserita.setBackground(new java.awt.Color(255, 153, 102));
                IDCarta.setEnabled(true);
                pulsanteAssociaCarta.setEnabled(true);
                pulsanteApplicaSconto.setEnabled(true);
                schedaDiVendita = new SchedaDiVendita();
                jLabel7.setVisible(false);
                aggiornaCatalogoVendita();
            } else {
                jLabel7.setVisible(true);
            }
        } else {
            jLabel7.setVisible(true);
        }
    }//GEN-LAST:event_pulsanteConfermaVenditaActionPerformed

    private void aggiungiAlCarrelloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aggiungiAlCarrelloActionPerformed
        if (jTable1.getSelectedRow() != -1) {//se è selezionata una riga dalla tabella dei dispositivi
            Boolean inserita = false;
            dim2 = (DefaultTableModel) jTable2.getModel();
            for (int i = 0; i < jTable2.getRowCount(); i++) {//scorri le righe della tabella del carrello
                //se c'è già un dispositivo uguale (stesso nome)
                if (String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 0)).equals(String.valueOf(jTable2.getValueAt(i, 0)))) {
                    //se la quantità in carrello non supera la quantità in magazzino, incrementala
                    if (Integer.parseInt(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 2))) > Integer.parseInt(String.valueOf(jTable2.getValueAt(i, 2)))) {
                        dim2.setValueAt(Integer.parseInt(String.valueOf(jTable2.getValueAt(i, 2))) + 1, i, 2);
                        schedaDiVendita.aggiungiDispositivo(listaDispositivi.get(jTable1.getSelectedRow()));
                        inserita = true;
                        jLabel8.setVisible(false);
                    } else {//altrimenti mostra un messaggio di errore
                        jLabel8.setVisible(true);
                        inserita = true;
                    }
                }
            }
            //se dopo lo scorrimento non è stata incrementata nessuna riga e non sono stati mostrati errori aggiungi una nuova riga al carrello
            if (!inserita) {
                dim2.addRow(new Object[]{String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 0)), String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 1)), "1"});
                jLabel8.setVisible(false);
                schedaDiVendita.aggiungiDispositivo(listaDispositivi.get(jTable1.getSelectedRow()));
            }
            importoTotale = schedaDiVendita.getTotale();//ricalcola il totale
            //calcola gli sconti sul totale e applicali
            float sconto = scontoAlto.check(importoTotale);
            if (sconto != -1) {
                jLabel9.setText("Sconto di " + sconto + " € applicato su spesa minima");
                importoTotale = importoTotale - sconto;
            } else {
                jLabel9.setText("Spesa minima troppo bassa per applicare uno sconto");
            }
            importoTotale = ((int) ((importoTotale + 0.005f) * 100)) / 100f;//approssima il totale alla seconda cifra decimale
            totale.setText(String.valueOf(importoTotale));
        }
    }//GEN-LAST:event_aggiungiAlCarrelloActionPerformed

    private void ricercaDispositivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ricercaDispositivoActionPerformed
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        listaDispositivi = overClock.visualizzaListaDispositivi(ricerca.getText());
        //viene ricercata la lista dei dispositivi dal catalogo e vengono rimossi i pezzi o quelli con quantità zero
        for (int i = listaDispositivi.size() - 1; i >= 0; i--) {
            if (listaDispositivi.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo || listaDispositivi.get(i).getQuantita() == 0) {
                listaDispositivi.remove(i);
            }
        }
        for (int i = 0; i < listaDispositivi.size(); i++) {
            dim.addRow(new Object[]{listaDispositivi.get(i).getNome(), listaDispositivi.get(i).getPrezzo(), listaDispositivi.get(i).getQuantita()});
        }
    }//GEN-LAST:event_ricercaDispositivoActionPerformed

    public void aggiornaCatalogoVendita() {
        //stessa cosa di ricerca ma viene richiamata dopo una vendita per mostrare il nuovo elenco
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        listaDispositivi = overClock.visualizzaListaDispositivi(ricerca.getText());
        for (int i = listaDispositivi.size() - 1; i >= 0; i--) {
            if (listaDispositivi.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo || listaDispositivi.get(i).getQuantita() == 0) {
                listaDispositivi.remove(i);
            }
        }
        for (int i = 0; i < listaDispositivi.size(); i++) {
            dim.addRow(new Object[]{listaDispositivi.get(i).getNome(), listaDispositivi.get(i).getPrezzo(), listaDispositivi.get(i).getQuantita()});
        }
    }

    private void pulsanteAssociaCartaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteAssociaCartaActionPerformed
        if (!IDCarta.getText().equals("") && !overClock.ricercaCarta(IDCarta.getText()).isEmpty()) {//se il campo di testo non è vuoto e viene trovata una carta con questo id
            idCarta = IDCarta.getText();
            carta = overClock.getCartaFedelta(Integer.parseInt(idCarta));
            schedaDiVendita.setCartaFedelta(carta);
            cartaInserita.setText(idCarta);
            cartaInserita.setBackground(new java.awt.Color(204, 255, 153));
            frameOperazioneRiuscita = new OperazioneRiuscita();
            Timer timer;
            timer = new Timer(1000, (ActionEvent e) -> {
                frameOperazioneRiuscita.dispose();
            });
            timer.setRepeats(false);
            timer.start();
            frameOperazioneRiuscita.setVisible(true);
            frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
            IDCarta.setEnabled(false);
            pulsanteAssociaCarta.setEnabled(false);
        } else {
            idCarta = "-1";
            cartaInserita.setText("Nessuna");
            cartaInserita.setBackground(new java.awt.Color(255, 153, 102));
            frameIDNonValido = new IDNonValido();
            Timer timer;
            timer = new Timer(1000, (ActionEvent e) -> {
                frameIDNonValido.dispose();
            });
            timer.setRepeats(false);
            timer.start();
            frameIDNonValido.setVisible(true);
            frameIDNonValido.setDefaultCloseOperation(HIDE_ON_CLOSE);
        }
    }//GEN-LAST:event_pulsanteAssociaCartaActionPerformed

    private void pulsanteApplicaScontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteApplicaScontoActionPerformed
        if (!idCarta.equals("-1")) {
            int saldo = overClock.getCartaFedelta(Integer.parseInt(idCarta)).getSaldoPunti();
            if (saldo > 100 && !scontoApplicato) {//se il saldo sulla carta è maggiore di 100 punti
                int moltiplicatore = (int) Math.floor(saldo / 100);//si divide per 100 arrotondando per difetto e si moltiplica per 10 (10€ di sconto ogni 100 punti)
                sconto = 10 * moltiplicatore;
                if (importoTotale < sconto) {
                    sconto = (int) Math.round(importoTotale);
                    importoTotale = 0;
                } else {
                    importoTotale = importoTotale - sconto;
                    importoTotale = ((int) ((importoTotale + 0.005f) * 100)) / 100f;//approssima il totale alla seconda cifra decimale
                }
                scontoApplicato = true;//possibilità di applicare lo sconto una sola volta
                totale.setText(String.valueOf(importoTotale));
                schedaDiVendita.setTotale(importoTotale);
                frameOperazioneRiuscita = new OperazioneRiuscita();
                Timer timer;
                timer = new Timer(1000, (ActionEvent e) -> {
                    frameOperazioneRiuscita.dispose();
                });
                timer.setRepeats(false);
                timer.start();
                frameOperazioneRiuscita.setVisible(true);
                pulsanteApplicaSconto.setEnabled(false);
            } else {
                frameSconto = new Sconto();
                Timer timer;
                timer = new Timer(1000, (ActionEvent e) -> {
                    frameSconto.dispose();
                });
                timer.setRepeats(false);
                timer.start();
                frameSconto.setVisible(true);
                frameSconto.setDefaultCloseOperation(HIDE_ON_CLOSE);
            }
        } else {//messsaggio di errore se i punti sono insufficienti
            frameSconto = new Sconto();
            Timer timer;
            timer = new Timer(1000, (ActionEvent e) -> {
                frameSconto.dispose();
            });
            timer.setRepeats(false);
            timer.start();
            frameSconto.setVisible(true);
            frameSconto.setDefaultCloseOperation(HIDE_ON_CLOSE);
        }

    }//GEN-LAST:event_pulsanteApplicaScontoActionPerformed

    private void usatiCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usatiCheckBoxActionPerformed
        if (usatiCheckBox.isSelected()) {
            dim = (DefaultTableModel) jTable1.getModel();
            dim.setRowCount(0);
            listaDispositivi = overClock.visualizzaListaDispositiviUsati(ricerca.getText());
            //stessa cosa di ricerca ma rimuove dalla lista tutti i dispositivi che non sono usati
            for (int i = listaDispositivi.size() - 1; i >= 0; i--) {
                if (listaDispositivi.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo || listaDispositivi.get(i).getQuantita() == 0) {
                    listaDispositivi.remove(i);
                }
            }
            for (int i = 0; i < listaDispositivi.size(); i++) {
                dim.addRow(new Object[]{listaDispositivi.get(i).getNome(), listaDispositivi.get(i).getPrezzo(), listaDispositivi.get(i).getQuantita()});
            }
        } else {
            dim = (DefaultTableModel) jTable1.getModel();
            dim.setRowCount(0);
            listaDispositivi = overClock.visualizzaListaDispositivi(ricerca.getText());
            for (int i = listaDispositivi.size() - 1; i >= 0; i--) {
                if (listaDispositivi.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo || listaDispositivi.get(i).getQuantita() == 0) {
                    listaDispositivi.remove(i);
                }
            }
            for (int i = 0; i < listaDispositivi.size(); i++) {
                dim.addRow(new Object[]{listaDispositivi.get(i).getNome(), listaDispositivi.get(i).getPrezzo(), listaDispositivi.get(i).getQuantita()});
            }
        }
    }//GEN-LAST:event_usatiCheckBoxActionPerformed

    private void totaleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_totaleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_totaleActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField IDCarta;
    private javax.swing.JButton Remove;
    private javax.swing.JButton aggiungiAlCarrello;
    private javax.swing.JTextField cartaInserita;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JList<String> jListDisponibilita;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JButton pulsanteApplicaSconto;
    private javax.swing.JButton pulsanteAssociaCarta;
    private javax.swing.JButton pulsanteConfermaVendita;
    private javax.swing.JTextField ricerca;
    private javax.swing.JButton ricercaDispositivo;
    private javax.swing.JTextField totale;
    private javax.swing.JCheckBox usatiCheckBox;
    // End of variables declaration//GEN-END:variables
}
