package classi;

import classi.Prenotazione.statoPrenotazione;
import classi.SchedaAcquisto.Danni;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DispositivoDAO implements InterfacciaDAO {

    private OverClock overClock = OverClock.getIstanza();
    public List<Distributore> ld = overClock.getIstanzaDistributori();
    Catalogo c = Catalogo.getIstanza();
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;
    PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();
    PromozioneDAO promozioneDAO = new PromozioneDAO();

    public DispositivoDAO() {
    }

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = Connessione.getInstance().getConnection();
        return conn;
    }

    public void add(Dispositivo dispositivo) {
        try {
            String queryString = "INSERT INTO dispositivo(codice,nome,marca,prezzo,acquisto,quantita,tipo,distributore) VALUES(?,?,?,?,?,?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, dispositivo.getCodice());
            ptmt.setString(2, dispositivo.getNome());
            ptmt.setString(3, dispositivo.getMarca());
            ptmt.setFloat(4, dispositivo.getPrezzo());
            ptmt.setFloat(5, dispositivo.getPrezzoAcquisto());
            ptmt.setInt(6, dispositivo.getQuantita());
            ptmt.setString(7, String.valueOf(dispositivo.getTipo()));
            ptmt.setInt(8, dispositivo.getDistributore().getId());

            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void addUsato(Dispositivo dispositivo, Danni danni) {
        float moltiplicatore = overClock.getMoltiplicatore(danni);
        try {
            String queryString = "INSERT INTO dispositivo(codice,nome,marca,prezzo,acquisto,quantita,tipo,distributore,reperibilita) VALUES(?,?,?,?,?,?,?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, dispositivo.getCodice() + "-" + danni);
            ptmt.setString(2, dispositivo.getNome() + " - " + danni);
            ptmt.setString(3, dispositivo.getMarca());
            ptmt.setFloat(4, dispositivo.getPrezzo() * moltiplicatore);
            ptmt.setString(5, null);
            ptmt.setInt(6, 1);
            ptmt.setString(7, String.valueOf(dispositivo.getTipo()));
            ptmt.setInt(8, dispositivo.getDistributore().getId());
            ptmt.setBoolean(9, dispositivo.getReperibilita());
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void update(String nome, int quantita) {
        try {
            String queryString = "UPDATE dispositivo SET quantita=? WHERE nome=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, quantita);
            ptmt.setString(2, nome);
            ptmt.executeUpdate();
            Dispositivo dispositivo = overClock.getDispositivoByNome(nome);
            dispositivo.setQuantita(quantita);
            System.out.println("Tabella Aggiornata Correttamente");

        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updateQuantita(int q, String codice) {
        try {
            String queryString = "UPDATE dispositivo SET quantita=? WHERE codice=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, q);
            ptmt.setString(2, codice);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updatePrezzoAcquisto(float prezzo, String codice) {
        try {
            String queryString = "UPDATE dispositivo SET acquisto=? WHERE codice=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setFloat(1, prezzo);
            ptmt.setString(2, codice);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updatePrezzo(float prezzo, String codice) {
        try {
            String queryString = "UPDATE dispositivo SET prezzo=? WHERE codice=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setFloat(1, prezzo);
            ptmt.setString(2, codice);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updateDistributore2(int idDistributore, String codice) {
        try {
            String queryString = "UPDATE dispositivo SET distributore=?, Reperibilita=? WHERE codice=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, idDistributore);
            ptmt.setBoolean(2, true);
            ptmt.setString(3, codice);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updateDistributore(int id) {
        try {
            String queryString = "UPDATE dispositivo SET distributore=?, Reperibilita=? WHERE distributore=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, null);
            ptmt.setBoolean(2, false);
            ptmt.setInt(3, id);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updateReperibilitaTrue(String codice) {
        try {
            String queryString = "UPDATE dispositivo SET  Reperibilita=? WHERE codice=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setBoolean(1, true);
            ptmt.setString(2, codice);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updateReperibilita(String codice) {
        try {
            String queryString = "UPDATE dispositivo SET  Reperibilita=? WHERE codice=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setBoolean(1, false);
            ptmt.setString(2, codice);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void findAll() {
        List<Dispositivo> listaDispositivi = new ArrayList();
        try {

            String queryString = "SELECT * FROM dispositivo";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                Dispositivo.tipoArticolo tipo = null;
                switch (resultSet.getString("tipo")) {
                    default -> {
                    }
                    case "pc" ->
                        tipo = Dispositivo.tipoArticolo.pc;
                    case "tablet" ->
                        tipo = Dispositivo.tipoArticolo.tablet;
                    case "smartphone" ->
                        tipo = Dispositivo.tipoArticolo.smartphone;
                    case "pezzo" ->
                        tipo = Dispositivo.tipoArticolo.pezzo;
                }
                if (resultSet.getString("distributore") == null) {
                    Dispositivo d = new Dispositivo(resultSet.getString("nome"), resultSet.getString("codice"),
                            resultSet.getString("marca"), resultSet.getFloat("prezzo"), resultSet.getFloat("acquisto"),
                            resultSet.getInt("quantita"), tipo, null, resultSet.getBoolean("reperibilita"));
                    listaDispositivi.add(d);
                } else {
                    for (int i = 0; i < overClock.getListaDistributori().size(); i++) {
                        if (overClock.getListaDistributori().get(i).getId() == resultSet.getInt("distributore")) {
                            Dispositivo d = new Dispositivo(resultSet.getString("nome"), resultSet.getString("codice"),
                                    resultSet.getString("marca"), resultSet.getFloat("prezzo"), resultSet.getFloat("acquisto"),
                                    resultSet.getInt("quantita"), tipo, overClock.getListaDistributori().get(i), resultSet.getBoolean("reperibilita"));
                            listaDispositivi.add(d);
                        }
                    }
                }
            }
            c.setListaDispositivi(listaDispositivi);
            prenotazioneDAO.findAllObservers();
            promozioneDAO.findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }
}
