package gui;

import classi.Dispositivo;
import classi.Dispositivo.tipoArticolo;
import classi.OverClock;
import classi.SchedaOrdine;
import classi.SchedaOrdineDAO;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

public class NuovoOrdineDistributore extends javax.swing.JPanel {

    private OverClock overClock;
    private DefaultTableModel dim;
    private DefaultTableModel dim2;
    private List<Dispositivo> listaDispositivi;
    private tipoArticolo tipo = tipoArticolo.pc;
    private SchedaOrdine schedaOrdine = new SchedaOrdine();
    private SchedaOrdineDAO schedaOrdineDAO = new SchedaOrdineDAO();
    private float importoTotale = 0;
    private OperazioneRiuscita frameOperazioneRiuscita;

    public NuovoOrdineDistributore(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        selezionaTipo = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        pulsanteAggiungi = new javax.swing.JButton();
        pulsanteRimuovi = new javax.swing.JButton();
        pulsanteConferma = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        totale = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        ricerca = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        pulsanteRicerca = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        quantita = new javax.swing.JTextField();
        pulsanteConfermaQuantita = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        quantitaR = new javax.swing.JTextField();
        pulsanteRimuoviQuantita = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Nuovo Ordine Distributore");

        jLabel2.setText("Tipo Articolo:");

        selezionaTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pc", "Tablet", "Smartphone", "Pezzo di Ricambio" }));
        selezionaTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selezionaTipoActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Codice", "Distributore", "Disponibilità", "Costo"
            }
        ));
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        listaDispositivi = overClock.visualizzaListaDispositivi("");
        for(int i=listaDispositivi.size()-1;i>=0;i--)
        if(listaDispositivi.get(i).getTipo()!=tipo||!listaDispositivi.get(i).getReperibilita()
            ||listaDispositivi.get(i).getNome().contains("comeNuovo")||
            listaDispositivi.get(i).getNome().contains("danniLeggeri")||listaDispositivi.get(i).getNome().contains("danniEvidenti"))
        listaDispositivi.remove(i);
        for (int i = 0; i < listaDispositivi.size(); i++) {
            if(listaDispositivi.get(i).getDistributore()!=null)
            dim.addRow(new Object[]{listaDispositivi.get(i).getNome(),listaDispositivi.get(i).getCodice(),listaDispositivi.get(i).getDistributore().getNome(), listaDispositivi.get(i).getQuantita(), listaDispositivi.get(i).getPrezzoAcquisto()});
        }
        jTable1.setDefaultEditor(Object.class, null);
        jScrollPane1.setViewportView(jTable1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Codice", "Distributore", "Quantità", "Costo"
            }
        ));
        jTable2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable2.setDefaultEditor(Object.class, null);
        jScrollPane2.setViewportView(jTable2);

        pulsanteAggiungi.setText("Aggiungi all'Ordine");
        pulsanteAggiungi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteAggiungiActionPerformed(evt);
            }
        });

        pulsanteRimuovi.setBackground(new java.awt.Color(255, 102, 102));
        pulsanteRimuovi.setText("Rimuovi Articolo Selezionato");
        pulsanteRimuovi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteRimuoviActionPerformed(evt);
            }
        });

        pulsanteConferma.setText("Conferma");
        pulsanteConferma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteConfermaActionPerformed(evt);
            }
        });

        jLabel3.setText("Totale:");

        totale.setEditable(false);

        jLabel4.setText("Ordine:");

        jLabel5.setText("Ricerca Articolo:");

        pulsanteRicerca.setText("Ricerca");
        pulsanteRicerca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteRicercaActionPerformed(evt);
            }
        });

        jLabel6.setForeground(new java.awt.Color(255, 0, 0));
        jLabel6.setText("Ordine Vuoto");
        jLabel6.setVisible(false);

        jLabel7.setText("Aggiungi Quantità:");

        pulsanteConfermaQuantita.setText("Conferma");
        pulsanteConfermaQuantita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteConfermaQuantitaActionPerformed(evt);
            }
        });

        jLabel8.setText("Rimuovi Quantità:");

        quantitaR.setHighlighter(null);

        pulsanteRimuoviQuantita.setText("Rimuovi");
        pulsanteRimuoviQuantita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteRimuoviQuantitaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(totale, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(pulsanteConferma))
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 371, Short.MAX_VALUE)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                        .addComponent(jLabel1))
                                    .addGap(35, 35, 35)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(pulsanteRimuovi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel5)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(ricerca)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(pulsanteRicerca))
                                        .addComponent(pulsanteAggiungi, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel2)
                                        .addComponent(selezionaTipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel6)
                                        .addComponent(jLabel7)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(quantita)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(pulsanteConfermaQuantita))
                                        .addComponent(jLabel8)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(quantitaR)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(pulsanteRimuoviQuantita))))))
                        .addGap(41, 41, 41))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(selezionaTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pulsanteRicerca))
                        .addGap(18, 18, 18)
                        .addComponent(pulsanteAggiungi)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(quantita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pulsanteConfermaQuantita))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(quantitaR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(pulsanteRimuoviQuantita))
                        .addGap(18, 18, 18)
                        .addComponent(pulsanteRimuovi)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pulsanteConferma)
                    .addComponent(totale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void pulsanteRimuoviActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteRimuoviActionPerformed
        if (jTable2.getSelectedRow() != -1) {
            Dispositivo d = overClock.getDispositivoByNome(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 0)));
            schedaOrdine.rimuoviRigaOrdine(schedaOrdine.getOrdine(d));
            dim2.removeRow(jTable2.getSelectedRow());
            importoTotale = schedaOrdine.getTotale();
            importoTotale = ((int) ((importoTotale + 0.005f) * 100)) / 100f;//approssima il totale alla seconda cifra decimale
            totale.setText(String.valueOf(importoTotale));
        }
    }//GEN-LAST:event_pulsanteRimuoviActionPerformed

    private void selezionaTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selezionaTipoActionPerformed
        switch (selezionaTipo.getSelectedIndex()) {
            case 0 ->
                tipo = Dispositivo.tipoArticolo.pc;
            case 1 ->
                tipo = Dispositivo.tipoArticolo.tablet;
            case 2 ->
                tipo = Dispositivo.tipoArticolo.smartphone;
            case 3 ->
                tipo = Dispositivo.tipoArticolo.pezzo;
        }
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        listaDispositivi = overClock.visualizzaListaDispositivi("");
        for (int i = listaDispositivi.size() - 1; i >= 0; i--) {
            if (listaDispositivi.get(i).getTipo() != tipo || !listaDispositivi.get(i).getReperibilita()
                    || listaDispositivi.get(i).getNome().contains("comeNuovo")
                    || listaDispositivi.get(i).getNome().contains("danniLeggeri") || listaDispositivi.get(i).getNome().contains("danniEvidenti")) {
                listaDispositivi.remove(i);
            }
        }
        for (int i = 0; i < listaDispositivi.size(); i++) {
            dim.addRow(new Object[]{listaDispositivi.get(i).getNome(), listaDispositivi.get(i).getCodice(), listaDispositivi.get(i).getDistributore().getNome(), listaDispositivi.get(i).getQuantita(), listaDispositivi.get(i).getPrezzo()});
        }
    }//GEN-LAST:event_selezionaTipoActionPerformed

    private void pulsanteRicercaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteRicercaActionPerformed
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        listaDispositivi = overClock.visualizzaListaDispositivi(ricerca.getText());
        for (int i = listaDispositivi.size() - 1; i >= 0; i--) {
            if (listaDispositivi.get(i).getTipo() != tipo || !listaDispositivi.get(i).getReperibilita()
                    || listaDispositivi.get(i).getNome().contains("comeNuovo")
                    || listaDispositivi.get(i).getNome().contains("danniLeggeri") || listaDispositivi.get(i).getNome().contains("danniEvidenti")) {
                listaDispositivi.remove(i);
            }
        }
        for (int i = 0; i < listaDispositivi.size(); i++) {
            dim.addRow(new Object[]{listaDispositivi.get(i).getNome(), listaDispositivi.get(i).getCodice(), listaDispositivi.get(i).getDistributore().getNome(), listaDispositivi.get(i).getQuantita(), listaDispositivi.get(i).getPrezzo()});
        }
    }//GEN-LAST:event_pulsanteRicercaActionPerformed

    private void pulsanteAggiungiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteAggiungiActionPerformed
        if (jTable1.getSelectedRow() != -1) {
            dim2 = (DefaultTableModel) jTable2.getModel();
            Boolean inserita = false;
            for (int i = 0; i < jTable2.getRowCount(); i++) {
                if (String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 1)).equals(String.valueOf(jTable2.getValueAt(i, 1)))) {
                    dim2.setValueAt(Integer.parseInt(String.valueOf(jTable2.getValueAt(i, 3))) + 1, i, 3);
                    inserita = true;
                }
            }
            if (inserita != true) {
                dim2.addRow(new Object[]{String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 0)), String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 1)),
                    String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 2)), "1", String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 4))});
            }

            schedaOrdine.aggiungiOrdine(listaDispositivi.get(jTable1.getSelectedRow()), 1);

            importoTotale = schedaOrdine.getTotale();
            importoTotale = ((int) ((importoTotale + 0.005f) * 100)) / 100f;//approssima il totale alla seconda cifra decimale
            totale.setText(String.valueOf(importoTotale));
        }
    }//GEN-LAST:event_pulsanteAggiungiActionPerformed

    private void pulsanteConfermaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteConfermaActionPerformed
        if (dim2 != null) {
            if (dim2.getRowCount() > 0) {
                schedaOrdineDAO.add(schedaOrdine);
                schedaOrdine.inviaOrdine();
                frameOperazioneRiuscita = new OperazioneRiuscita();
                Timer timer;
                timer = new Timer(1000, (ActionEvent e) -> {
                    frameOperazioneRiuscita.dispose();
                });
                timer.setRepeats(false);
                timer.start();
                frameOperazioneRiuscita.setVisible(true);
                dim2.setRowCount(0);
                importoTotale = 0;
                totale.setText("0.0");
                schedaOrdine = new SchedaOrdine();
                jLabel6.setVisible(false);
            } else {
                jLabel6.setVisible(true);
            }
        } else {
            jLabel6.setVisible(true);
        }
    }//GEN-LAST:event_pulsanteConfermaActionPerformed

    private void pulsanteConfermaQuantitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteConfermaQuantitaActionPerformed
        if (jTable2.getSelectedRow() != -1) {//se è selezionata una riga nel carrello
            try {
                Integer.parseInt(quantita.getText());//verifica che la quantità del campo di testo sia un numero intero
                //incrementa di quella quantità la riga selezionata
                jTable2.setValueAt(Integer.parseInt(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 3))) + Integer.parseInt(quantita.getText()), jTable2.getSelectedRow(), 3);
                //aggiungi all'ordine la quantita di dispositivi selezionata
                schedaOrdine.aggiungiOrdine(overClock.getDispositivo(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 1))), Integer.parseInt(quantita.getText()));
                importoTotale = schedaOrdine.getTotale();
                importoTotale = ((int) ((importoTotale + 0.005f) * 100)) / 100f;//approssima il totale alla seconda cifra decimale
                totale.setText(String.valueOf(importoTotale));
                quantita.setBackground(Color.WHITE);
            } catch (NumberFormatException e) {
                quantita.setBackground(new java.awt.Color(255, 153, 102));
            }
        }
    }//GEN-LAST:event_pulsanteConfermaQuantitaActionPerformed

    private void pulsanteRimuoviQuantitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteRimuoviQuantitaActionPerformed
        if (jTable2.getSelectedRow() != -1) {
            try {
                Integer.parseInt(quantitaR.getText());
                Dispositivo d = overClock.getDispositivo(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 1)));
                if (Integer.parseInt(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 3))) - Integer.parseInt(quantitaR.getText()) > 1) {
                    schedaOrdine.rimuoviOrdine(d, Integer.parseInt(quantitaR.getText()));
                    dim2.setValueAt(Integer.parseInt(String.valueOf(jTable2.getValueAt(jTable2.getSelectedRow(), 3))) - Integer.parseInt(quantitaR.getText()), jTable2.getSelectedRow(), 3);
                } else {
                    schedaOrdine.rimuoviRigaOrdine(schedaOrdine.getOrdine(d));
                    dim2.removeRow(jTable2.getSelectedRow());
                }
                importoTotale = schedaOrdine.getTotale();
                importoTotale = ((int) ((importoTotale + 0.005f) * 100)) / 100f;//approssima il totale alla seconda cifra decimale
                totale.setText(String.valueOf(importoTotale));
                quantitaR.setBackground(Color.WHITE);
            } catch (NumberFormatException e) {
                quantitaR.setBackground(new java.awt.Color(255, 153, 102));
            }
        }
    }//GEN-LAST:event_pulsanteRimuoviQuantitaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JButton pulsanteAggiungi;
    private javax.swing.JButton pulsanteConferma;
    private javax.swing.JButton pulsanteConfermaQuantita;
    private javax.swing.JButton pulsanteRicerca;
    private javax.swing.JButton pulsanteRimuovi;
    private javax.swing.JButton pulsanteRimuoviQuantita;
    private javax.swing.JTextField quantita;
    private javax.swing.JTextField quantitaR;
    private javax.swing.JTextField ricerca;
    private javax.swing.JComboBox<String> selezionaTipo;
    private javax.swing.JTextField totale;
    // End of variables declaration//GEN-END:variables
}
