package classi;

import classi.Prenotazione.statoPrenotazione;
import classi.SchedaAcquisto.Danni;
import classi.SchedaRiparazione.statoRiparazioni;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class OverClock {

    private List<CartaFedelta> listaCarteFedelta;
    private List<Cliente> listaClienti;
    private List<Distributore> listaDistributori;
    private static OverClock singleton;
    private Catalogo catalogo = Catalogo.getIstanza();
    private List<Promozione> promozioni;
    private List<Prenotazione> prenotazioni;
    private List<SchedaDiVendita> vendite;
    private List<SchedaAcquisto> acquisti;
    private List<SchedaOrdine> ordini;
    private List<SchedaRiparazione> riparazioni;

    //-----Pattern GOF: Singleton-----------
    public static OverClock getIstanza() {
        if (singleton == null) {
            singleton = new OverClock();
        }
        return singleton;
    }

    public List<Promozione> getIstanzaPromozioni() {
        if (promozioni == null) {
            promozioni = new ArrayList();
        }
        return promozioni;
    }

    public List<Prenotazione> getIstanzaPrenotazioni() {
        if (prenotazioni == null) {
            prenotazioni = new ArrayList();
        }
        return prenotazioni;
    }

    public List<SchedaDiVendita> getIstanzaVendite() {
        if (vendite == null) {
            vendite = new ArrayList();
        }
        return vendite;
    }

    public List<SchedaAcquisto> getIstanzaAcquisti() {
        if (acquisti == null) {
            acquisti = new ArrayList();
        }
        return acquisti;
    }

    public List<SchedaOrdine> getIstanzaOrdini() {
        if (ordini == null) {
            ordini = new ArrayList();
        }
        return ordini;
    }

    public List<SchedaRiparazione> getIstanzaRiparazioni() {
        if (riparazioni == null) {
            riparazioni = new ArrayList();
        }
        return riparazioni;
    }

    public List<Cliente> getIstanzaClienti() {
        if (listaClienti == null) {
            listaClienti = new ArrayList();
        }
        return listaClienti;
    }

    public List<Distributore> getIstanzaDistributori() {

        if (listaDistributori == null) {
            listaDistributori = new ArrayList();
        }
        return listaDistributori;
    }

    public List<CartaFedelta> getIstanzaCarteFedelta() {
        if (listaCarteFedelta == null) {
            listaCarteFedelta = new ArrayList<>();
        }
        return listaCarteFedelta;
    }

    //-----Funzioni Promozione-----------
    public void setPromozioni(List<Promozione> l) {
        this.promozioni = l;
    }

    public Promozione getPromozione(int id) {
        for (int i = 0; i < promozioni.size(); i++) {
            if (promozioni.get(i).getID() == id) {
                return promozioni.get(i);
            }
        }
        return null;
    }

    public boolean getPromozioneby(String ricerca) {
        for (int i = 0; i < promozioni.size(); i++) {
            if (String.valueOf(promozioni.get(i).getDispostivo().getCodice()).equals(ricerca)) {
                return true;
            }
        }
        return false;
    }

    public List<Promozione> visualizzaPromozioni(String ricerca) {
        List<Promozione> promozioniRicercate = new ArrayList();
        for (int i = 0; i < promozioni.size(); i++) {
            if (String.valueOf(promozioni.get(i).getDispostivo().getNome().toLowerCase()).startsWith(ricerca)
                    || String.valueOf(promozioni.get(i).getDispostivo().getNome().toUpperCase()).startsWith(ricerca)) {
                promozioniRicercate.add(promozioni.get(i));
            }
        }
        return promozioniRicercate;
    }

    //-----Funzioni Prenotazione-----------
    public void setPrenotazioni(List<Prenotazione> l) {
        this.prenotazioni = l;
    }

    public Prenotazione getPrenotazione(int id) {
        for (int i = 0; i < prenotazioni.size(); i++) {
            if (prenotazioni.get(i).getID() == id) {
                return prenotazioni.get(i);
            }
        }
        return null;
    }

    public List<Prenotazione> visualizzaPrenotazioni(String ricerca) {
        List<Prenotazione> prenotazioniRicercate = new ArrayList();
        for (int i = 0; i < prenotazioni.size(); i++) {
            if (String.valueOf(prenotazioni.get(i).getCliente().getEmail().toLowerCase()).startsWith(ricerca)
                    || String.valueOf(prenotazioni.get(i).getCliente().getEmail().toUpperCase()).startsWith(ricerca)) {
                prenotazioniRicercate.add(prenotazioni.get(i));
            }
        }
        return prenotazioniRicercate;
    }

    public String getPrenotazioneString(statoPrenotazione s) {
        if (s == s.ordinato) {
            return "Ordinato";
        }
        if (s == s.inAttesaDiRitiro) {
            return "In Attesa di Ritiro";
        }
        if (s == s.ritirato) {
            return "Ritirato";
        }
        return "";
    }

    public statoPrenotazione getStatoPrenotazione(String s) {
        statoPrenotazione stato = null;
        switch (s) {
            default -> {
            }
            case "Ordinato" ->
                stato = statoPrenotazione.ordinato;
            case "In Attesa di Ritiro" ->
                stato = statoPrenotazione.inAttesaDiRitiro;
            case "Ritirato" ->
                stato = statoPrenotazione.ritirato;
        }
        return stato;
    }

    //-----Funzioni Vendita-----------
    public void setVendite(List<SchedaDiVendita> l) {
        this.vendite = l;
    }

    public List<SchedaDiVendita> visualizzaVendite(String ricerca) {
        List<SchedaDiVendita> sdvRicercate = new ArrayList();
        for (int i = 0; i < vendite.size(); i++) {
            if (String.valueOf(vendite.get(i).getID()).startsWith(ricerca)) {
                sdvRicercate.add(vendite.get(i));
            }
        }
        return sdvRicercate;
    }

    //-----Funzioni Acquisti-----------
    public void setAcquisti(List<SchedaAcquisto> l) {
        this.acquisti = l;
    }

    public List<SchedaAcquisto> visualizzaAcquisti(String ricerca) {
        List<SchedaAcquisto> saRicercate = new ArrayList();
        for (int i = 0; i < acquisti.size(); i++) {
            if (String.valueOf(acquisti.get(i).getID()).startsWith(ricerca)) {
                saRicercate.add(acquisti.get(i));
            }
        }
        return saRicercate;
    }

    public Danni getDanni(String s) {
        Danni d = null;
        switch (s) {
            case "Come Nuovo" ->
                d = Danni.comeNuovo;
            case "Danni Leggeri" ->
                d = Danni.danniLeggeri;
            case "Danni Evidenti" ->
                d = Danni.danniEvidenti;
        }
        return d;
    }

    public String getDanniFormattati(String s) {
        String d = "";
        switch (s) {
            case "Nuovi" ->
                d = "nuovo";
            case "Usati - Come Nuovi" ->
                d = "comeNuovo";
            case "Usati - Danni Leggeri" ->
                d = "danniLeggeri";
            case "Usati - Danni Evidenti" ->
                d = "danniEvidenti";
            case "Non Reperibili" ->
                d = "nonReperibili";
            case "Tutti" ->
                d = "Tutti";
        }
        return d;
    }

    public float getMoltiplicatore(Danni d) {
        if (d == Danni.comeNuovo) {
            return 0.8f;
        }
        if (d == Danni.danniLeggeri) {
            return 0.6f;
        }
        if (d == Danni.danniEvidenti) {
            return 0.4f;
        }
        return 0;
    }

    //-----Funzioni Cliente-----------
    public Cliente getCliente(String numero) {
        for (int i = 0; i < listaClienti.size(); i++) {
            if (listaClienti.get(i).getNumero().equals(numero)) {
                return listaClienti.get(i);
            }
        }
        return null;
    }

    public Cliente getClienteCarta(int id) {
        for (int i = 0; i < listaClienti.size(); i++) {
            if (listaClienti.get(i).getCartaFedelta() != null) {
                if (listaClienti.get(i).getCartaFedelta().getID() == id) {
                    return listaClienti.get(i);
                }
            }
        }
        return null;
    }

    public Cliente getClienteMail(String s) {
        for (int i = 0; i < listaClienti.size(); i++) {
            if (listaClienti.get(i).getEmail().equals(s) || listaClienti.get(i).getNumero().equals(s)) {
                return listaClienti.get(i);
            }
        }
        return null;
    }

    public void setListaClienti(List<Cliente> l) {
        this.listaClienti = l;
    }

    public List<Cliente> visualizzaClienti(String ricerca) {
        List<Cliente> clientiRicercati = new ArrayList<>();
        for (int i = 0; i < listaClienti.size(); i++) {
            if (listaClienti.get(i).getNome().regionMatches(true, 0, ricerca, 0, ricerca.length()) || listaClienti.get(i).getCognome().regionMatches(true, 0, ricerca, 0, ricerca.length())
                    || listaClienti.get(i).getNumero().regionMatches(true, 0, ricerca, 0, ricerca.length())) {
                clientiRicercati.add(listaClienti.get(i));
            }
        }
        return clientiRicercati;
    }

    //-----Funzioni Riparazioni-----------
    public void setRiparazioni(List<SchedaRiparazione> l) {
        this.riparazioni = l;
    }

    public statoRiparazioni getStato(String s) {
        statoRiparazioni stato = null;
        switch (s) {
            default -> {
            }
            case "preventivoInCorso" ->
                stato = statoRiparazioni.preventivoInCorso;
            case "preventivoConcluso" ->
                stato = statoRiparazioni.preventivoConcluso;
            case "riparazioneInCorso" ->
                stato = statoRiparazioni.riparazioneInCorso;
            case "riparazioneConclusa" ->
                stato = statoRiparazioni.riparazioneConclusa;
        }
        return stato;
    }

    public String getStatoStringa(statoRiparazioni s) {

        if (s == statoRiparazioni.preventivoInCorso) {
            return "preventivoInCorso";
        }
        if (s == statoRiparazioni.preventivoConcluso) {
            return "preventivoConcluso";
        }
        if (s == statoRiparazioni.riparazioneInCorso) {
            return "riparazioneInCorso";
        }
        if (s == statoRiparazioni.riparazioneConclusa) {
            return "riparazioneConclusa";
        }
        return "";

    }

    public String formattaStato(String stato) {
        String s = "";
        if (stato.equals("preventivoConcluso")) {
            s = "Preventivo Concluso";
        }
        if (stato.equals("riparazioneInCorso")) {
            s = "Riparazione in Corso";
        }
        if (stato.equals("riparazioneConclusa")) {
            s = "Riparazione Conclusa";
        }
        return s;
    }

    public List<SchedaRiparazione> getRiparazioni(String email) {
        List<SchedaRiparazione> l = new ArrayList();
        for (int i = 0; i < riparazioni.size(); i++) {
            if (riparazioni.get(i).getCliente().getEmail().startsWith(email)) {
                l.add(riparazioni.get(i));
            }
        }
        return l;
    }

    public SchedaRiparazione getSchedaRiparazione(int id) {
        for (int i = 0; i < riparazioni.size(); i++) {
            if (riparazioni.get(i).getID() == id) {
                return riparazioni.get(i);
            }
        }
        return null;
    }

    public List<SchedaRiparazione> getRiparazioni() {
        return this.riparazioni;
    }

    public List<SchedaRiparazione> RicercaRiparazione(String ricerca) {
        List<SchedaRiparazione> riparazioniRicercate = new ArrayList<>();
        for (int i = 0; i < getRiparazioni().size(); i++) {
            if (String.valueOf(getRiparazioni().get(i).getID()).regionMatches(true, 0, ricerca, 0, ricerca.length())) {
                riparazioniRicercate.add(getRiparazioni().get(i));
            }
        }
        return riparazioniRicercate;
    }
    //-----Funzioni Distributori-----------

    public void nuovoOrdineDistributore(SchedaOrdine s) {
        this.ordini.add(s);
    }

    public int findIdDistributore() {

        for (int i = 0; i < this.listaDistributori.size(); i++) {
            if (i != this.listaDistributori.get(i).getId()) {
                return i;
            }
        }
        return this.listaDistributori.size();

    }

    public List<Distributore> getListaDistributori() {
        return this.listaDistributori;
    }

    public void setListaDistributori(List<Distributore> listaDistributori) {
        this.listaDistributori = listaDistributori;
    }

    public Distributore getDistributore(int id) {
        for (int i = 0; i < listaDistributori.size(); i++) {
            if (listaDistributori.get(i).getId() == id) {
                return listaDistributori.get(i);
            }
        }
        return null;
    }

    public Distributore getDistributoreByNameMail(String ricerca) {
        for (int i = 0; i < getListaDistributori().size(); i++) {
            if (String.valueOf(getListaDistributori().get(i).getNome()).equalsIgnoreCase(ricerca)
                    || String.valueOf(getListaDistributori().get(i).getEmail()).equalsIgnoreCase(ricerca)) {
                return getListaDistributori().get(i);
            }
        }
        return null;
    }

    public List<Distributore> RicercaDistributori(String ricerca) {
        List<Distributore> distributoriRicercati = new ArrayList<>();
        for (int i = 0; i < getListaDistributori().size(); i++) {
            if (String.valueOf(getListaDistributori().get(i).getNome()).regionMatches(true, 0, ricerca, 0, ricerca.length())
                    || String.valueOf(getListaDistributori().get(i).getId()).regionMatches(true, 0, ricerca, 0, ricerca.length())
                    || String.valueOf(getListaDistributori().get(i).getEmail()).regionMatches(true, 0, ricerca, 0, ricerca.length())) {
                distributoriRicercati.add(getListaDistributori().get(i));
            }
        }
        return distributoriRicercati;
    }

    //-----Funzioni Dispositivi-----------
    public Dispositivo getDispositivo(String codice) {
        return catalogo.getDispositivo(codice);
    }

    public Dispositivo getDispositivoByNome(String nome) {
        return catalogo.getDispositivoByNome(nome);
    }

    public List<Dispositivo> visualizzaListaDispositivi(String nome) {
        return catalogo.getListaDispositivi(nome);
    }

    public List<Dispositivo> visualizzaListaDispositiviUsati(String nome) {
        return catalogo.getListaDispositiviUsati(nome);
    }

    public List<Dispositivo> getListaDispositiviByNome(String ricerca) {
        return catalogo.getListaDispositiviByNome(ricerca);
    }

    public List<Dispositivo> dispositiviByIdDistributore(int id) {
        return catalogo.getDispositivoByDistributore(id);
    }

    //-----Funzioni Ordini-----------
    public List<SchedaOrdine> visualizzaOrdini(String ricerca) {
        List<SchedaOrdine> soRicercate = new ArrayList();
        for (int i = 0; i < ordini.size(); i++) {
            if (String.valueOf(ordini.get(i).getID()).startsWith(ricerca)) {
                soRicercate.add(ordini.get(i));
            }
        }
        return soRicercate;
    }

    public void setOrdini(List<SchedaOrdine> l) {
        this.ordini = l;
    }

    //-----Funzioni CartaFedelta-----------
    public void setListaCarte(List<CartaFedelta> listaCarteFedelta) {
        this.listaCarteFedelta = listaCarteFedelta;
    }

    public void aggiornaSaldo(String id, int punti) throws IOException {

        for (int i = 0; i < listaCarteFedelta.size(); i++) {
            if (String.valueOf(listaCarteFedelta.get(i).getID()).equals(id)) {
                int saldo = listaCarteFedelta.get(i).getSaldoPunti();
                listaCarteFedelta.get(i).setPunti(saldo + punti);
            }
        }
    }

    public List<CartaFedelta> getListaCarteFedelta() {
        return this.listaCarteFedelta;
    }

    public List<CartaFedelta> ricercaCarta(String ricerca) {
        List<CartaFedelta> cartaRicercata = new ArrayList<>();
        for (int i = 0; i < listaCarteFedelta.size(); i++) {
            if (String.valueOf(listaCarteFedelta.get(i).getID()).startsWith(ricerca, 0)) {
                cartaRicercata.add(listaCarteFedelta.get(i));
            }
        }
        return cartaRicercata;

    }

    public int findID() {
        for (int i = 0; i < this.listaCarteFedelta.size(); i++) {
            if (i != this.listaCarteFedelta.get(i).getID()) {
                return i;
            }
        }
        return this.listaCarteFedelta.size();
    }

    public CartaFedelta getCartaFedelta(int idCarta) {
        for (int i = 0; i < listaCarteFedelta.size(); i++) {
            if (listaCarteFedelta.get(i).getID() == idCarta) {
                return listaCarteFedelta.get(i);
            }
        }
        return null;
    }

}
