package tests.classi;

import classi.Dispositivo;
import classi.Distributore;
import classi.Observer;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class DispositivoTest {

    Distributore distributore = new Distributore(0, "apple", "apple@gmail.it");
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);

    public DispositivoTest() {
    }

//    @Test
//    public void testAddObserver() {
//        System.out.println("addObserver");
//        Observer channel = null;
//        Dispositivo instance = null;
//        instance.addObserver(channel);
//    }
//    @Test
//    public void testRemoveObserver() {
//        System.out.println("removeObserver");
//        Observer channel = null;
//        Dispositivo instance = null;
//        instance.removeObserver(channel);
//        fail("The test case is a prototype.");
//    }
    @Test
    public void testGetNome() {
        String expResult = "Iphone 11";
        String result = d.getNome();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCodice() {
        String expResult = "IPHONE11";
        String result = d.getCodice();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetMarca() {
        String expResult = "apple";
        String result = d.getMarca();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPrezzo() {

        float expResult = 699.99F;
        float result = d.getPrezzo();
        assertEquals(expResult, result, 0.0);

    }

    @Test
    public void testGetReperibilita() {
        boolean expResult = true;
        boolean result = d.getReperibilita();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetQuantita() {
        int expResult = 100;
        int result = d.getQuantita();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetTipo() {
        Dispositivo.tipoArticolo expResult = Dispositivo.tipoArticolo.smartphone;
        Dispositivo.tipoArticolo result = d.getTipo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDistributore() {

        Distributore expResult = distributore;
        Distributore result = d.getDistributore();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetNome() {
        String expResult = "prova";
        d.setNome("prova");
        String Result = d.getNome();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSetReperibilita() {
        boolean expResult = false;
        d.setReperibilita(false);
        boolean Result = d.getReperibilita();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSetCodice() {
        String expResult = "NuovoCodice";
        d.setCodice("NuovoCodice");
        String Result = d.getCodice();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSetMarca() {
        String expResult = "samsung";
        d.setMarca("samsung");
        String Result = d.getMarca();
        assertEquals(expResult, Result);
    }

    @Test
    public void testSetPrezzo() {
        float expResult = 500.00f;
        d.setPrezzo(500.00f);
        float Result = d.getPrezzo();
        assertEquals(expResult, Result, 0.0);
    }

    @Test
    public void testSetQuantita() {
        int expResult = 50;
        d.setQuantita(50);
        int Result = d.getQuantita();
        assertEquals(expResult, Result);
    }

}
