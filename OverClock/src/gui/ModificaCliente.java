/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package gui;

import classi.Cliente;
import classi.ClienteDAO;
import classi.Dispositivo;
import classi.OverClock;
import classi.PrenotazioneDAO;
import java.awt.Color;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class ModificaCliente extends javax.swing.JFrame {

    private OverClock overclock;
    private Cliente Cliente;
    private VisualizzaClienti vc;
    private ClienteDAO clienteDAO = new ClienteDAO();
    private PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();

    public ModificaCliente(OverClock overclock, Cliente cliente, VisualizzaClienti vc) {
        this.overclock = overclock;
        this.Cliente = cliente;
        this.vc = vc;
        initComponents();
    }

    private ModificaCliente() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Nome = new javax.swing.JTextField();
        Cognome = new javax.swing.JTextField();
        Email = new javax.swing.JTextField();
        Telefono = new javax.swing.JTextField();
        IDcarta = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        ConfermaModifiche = new javax.swing.JButton();
        erroreGenerale = new javax.swing.JLabel();
        erroreMail = new javax.swing.JLabel();
        erroreNumero = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Nome.setEditable(false);
        Nome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NomeActionPerformed(evt);
            }
        });

        Cognome.setEditable(false);
        Cognome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CognomeActionPerformed(evt);
            }
        });

        IDcarta.setEditable(false);
        IDcarta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IDcartaActionPerformed(evt);
            }
        });

        jLabel1.setText("Nome");

        jLabel2.setText("Cognome");

        jLabel3.setText("Email");

        jLabel4.setText("Telefono");

        jLabel5.setText("IDcarta");

        ConfermaModifiche.setText("Conferma Modifiche");
        ConfermaModifiche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConfermaModificheActionPerformed(evt);
            }
        });

        erroreGenerale.setForeground(new java.awt.Color(255, 51, 102));
        erroreGenerale.setVisible(false);
        erroreGenerale.setText(null);

        erroreMail.setVisible(false);
        erroreMail.setForeground(new java.awt.Color(255, 51, 102));
        erroreMail.setText("La mail inserita é stata gia utilizzata da un cliente");

        erroreNumero.setVisible(false);
        erroreNumero.setForeground(new java.awt.Color(255, 51, 102));
        erroreNumero.setText("Il numero inserito é stata gia utilizzato da un cliente");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel6.setText("Modifica Cliente");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(35, 35, 35)
                        .addComponent(Cognome))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(55, 55, 55)
                        .addComponent(Nome))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(59, 59, 59)
                        .addComponent(Email))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(42, 42, 42)
                        .addComponent(Telefono))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(51, 51, 51)
                        .addComponent(IDcarta))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(erroreNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(erroreGenerale, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(erroreMail, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                        .addComponent(ConfermaModifiche)))
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel6)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel1))
                    .addComponent(Nome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel2))
                    .addComponent(Cognome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel3))
                    .addComponent(Email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel4))
                    .addComponent(Telefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel5))
                    .addComponent(IDcarta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(erroreNumero)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(erroreGenerale)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(erroreMail)
                                .addComponent(ConfermaModifiche)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        Nome.setText(Cliente.getNome());
        Cognome.setText(Cliente.getCognome());
        Email.setText(Cliente.getEmail());
        Telefono.setText(Cliente.getNumero());
        if(Cliente.getCartaFedelta()==null)
        IDcarta.setText("Nessuna");
        else
        IDcarta.setText(""+Cliente.getCartaFedelta().getID());

        setSize(new java.awt.Dimension(633, 341));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void NomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NomeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NomeActionPerformed

    private void CognomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CognomeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CognomeActionPerformed

    private void IDcartaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IDcartaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IDcartaActionPerformed

    private void ConfermaModificheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConfermaModificheActionPerformed
        String oldNumero, oldEmail;
        oldNumero = Cliente.getNumero();
        oldEmail = Cliente.getEmail();
        boolean m1, m2, c1 = false, c2 = false;
        erroreGenerale.setVisible(false);
        erroreMail.setVisible(false);
        erroreNumero.setVisible(false);

        if (oldNumero.equalsIgnoreCase(Telefono.getText())) {
            m2 = false;
        } else {
            m2 = true;
        }

        if (oldEmail.equalsIgnoreCase(Email.getText())) {
            m1 = false;
        } else {
            m1 = true;
        }

        if (Email.getText().equals("")) {
            Email.setBackground(new java.awt.Color(255, 153, 102));
        } else {
            try {
                InternetAddress e = new InternetAddress(Email.getText());
                e.validate();
                Email.setBackground(Color.white);
                c1 = true;
            } catch (AddressException ex) {
                Email.setBackground(new java.awt.Color(255, 153, 102));
            }
        }

        try {
            Float.parseFloat(Telefono.getText());
            if (Telefono.getText().equals("") || Telefono.getText().length() != 10) {
                Telefono.setBackground(new java.awt.Color(255, 153, 102));
            } else {
                Telefono.setBackground(Color.white);
                c2 = true;
            }
        } catch (NumberFormatException e) {
            Telefono.setBackground(new java.awt.Color(255, 153, 102));
        }
        if (m1 == true && c1 == true && m2 == true && c2 == true) {
            if (overclock.getClienteMail(Email.getText()) != null || overclock.getCliente(Telefono.getText()) != null) {
                if (overclock.getClienteMail(Email.getText()) != null) {
                    erroreMail.setVisible(true);
                    Email.setBackground(new java.awt.Color(255, 153, 102));
                }
                if (overclock.getCliente(Telefono.getText()) != null) {
                    erroreNumero.setVisible(true);
                    Telefono.setBackground(new java.awt.Color(255, 153, 102));
                }

            } else {
                clienteDAO.updateNumero(Telefono.getText(), oldNumero);
                clienteDAO.updateEmail(Email.getText(), oldEmail);
                prenotazioneDAO.updateEmail(Email.getText(), oldEmail);
                this.dispose();
                vc.update();

            }

        }

        if (m1 == true && c1 == true && m2 == false) {
            if (overclock.getClienteMail(Email.getText()) != null) {
                erroreMail.setVisible(true);
                Email.setBackground(new java.awt.Color(255, 153, 102));

            } else {
                clienteDAO.updateEmail(Email.getText(), oldEmail);
                prenotazioneDAO.updateEmail(Email.getText(), oldEmail);
                this.dispose();
                vc.update();
            }
        }
        if (m2 == true && c2 == true && m1 == false) {
            if (overclock.getCliente(Telefono.getText()) != null) {
                erroreNumero.setVisible(true);
                Telefono.setBackground(new java.awt.Color(255, 153, 102));

            } else {
                clienteDAO.updateNumero(Telefono.getText(), oldNumero);
                this.dispose();
                vc.update();
            }
        }
        if (m1 == false && m2 == false) {
            erroreGenerale.setText("nessuna modifica inserita");
            erroreGenerale.setVisible(true);
        }

        if (c1 == false) {
            erroreGenerale.setText("Il formato mail inserito é errato");
            erroreGenerale.setVisible(true);
        }
        if (c2 == false) {
            erroreGenerale.setText("Il formato numero inserito é errato");
            erroreGenerale.setVisible(true);
        }
        if (c1 == false && c2 == false) {
            erroreGenerale.setText("I formati mail e numero inseriti sono errati");
            erroreGenerale.setVisible(true);
        }

    }//GEN-LAST:event_ConfermaModificheActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ModificaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ModificaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ModificaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ModificaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new ModificaCliente().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Cognome;
    private javax.swing.JButton ConfermaModifiche;
    private javax.swing.JTextField Email;
    private javax.swing.JTextField IDcarta;
    private javax.swing.JTextField Nome;
    private javax.swing.JTextField Telefono;
    private javax.swing.JLabel erroreGenerale;
    private javax.swing.JLabel erroreMail;
    private javax.swing.JLabel erroreNumero;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    // End of variables declaration//GEN-END:variables
}
