package gui;

import classi.Distributore;
import classi.DistributoreDAO;
import classi.OverClock;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

public class InserisciDistributore extends javax.swing.JPanel {

    private OverClock overClock;
    private DistributoreDAO DistributoreDAO = new DistributoreDAO();
    private OperazioneRiuscita frameOperazioneRiuscita;

    public InserisciDistributore(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        nome = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        email = new javax.swing.JTextField();
        pulsanteConferma = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Inserisci Distributore");

        jLabel2.setText("Nome");

        jLabel3.setText("Email");

        pulsanteConferma.setText("Conferma");
        pulsanteConferma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteConfermaActionPerformed(evt);
            }
        });

        jLabel5.setForeground(new java.awt.Color(255, 0, 0));
        jLabel5.setText("");
        jLabel5.setVisible(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3))
                            .addGap(90, 90, 90)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(email, javax.swing.GroupLayout.DEFAULT_SIZE, 397, Short.MAX_VALUE)
                                .addComponent(nome)))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pulsanteConferma))))
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(pulsanteConferma))
                .addContainerGap(20, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void pulsanteConfermaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteConfermaActionPerformed
        jLabel5.setVisible(false);
        Boolean erroreNome = true;
        Boolean erroreMail = true;
        Boolean erroreInserimentoNome = true;
        Boolean erroreInserimentoEmail = true;
        if (nome.getText().equals("")) {
            nome.setBackground(new java.awt.Color(255, 153, 102));
            jLabel5.setVisible(true);
            erroreNome = true;
        } else {
            nome.setBackground(Color.white);
            erroreNome = false;
        }
        Distributore check = overClock.getDistributoreByNameMail(nome.getText());

        if (check == null) {
            System.out.print(overClock.getDistributoreByNameMail(nome.getText()));
            erroreInserimentoNome = false;
        }

        try {
            InternetAddress e = new InternetAddress(email.getText());
            e.validate();
            email.setBackground(Color.white);
            jLabel5.setVisible(false);
            erroreMail = false;
        } catch (AddressException ex) {
            email.setBackground(new java.awt.Color(255, 153, 102));
            erroreMail = true;
        }

        check = overClock.getDistributoreByNameMail(email.getText());

        if (check == null) {
            System.out.print(overClock.getDistributoreByNameMail(email.getText()));
            erroreInserimentoEmail = false;
        }

        if (!erroreMail && !erroreNome && !erroreInserimentoNome && !erroreInserimentoEmail) {
            Distributore d = new Distributore(overClock.findIdDistributore(), nome.getText(), email.getText());
            DistributoreDAO.add(d);
            frameOperazioneRiuscita = new OperazioneRiuscita();
            Timer timer;
            timer = new Timer(1000, (ActionEvent e) -> {
                frameOperazioneRiuscita.dispose();
            });
            timer.setRepeats(false);
            timer.start();
            frameOperazioneRiuscita.setVisible(true);
            frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
            nome.setText("");
            email.setText("");
        } else if (erroreMail || erroreNome) {
            jLabel5.setVisible(true);
            jLabel5.setText("Riempire i campi correttamente");

        } else if (erroreInserimentoNome || erroreInserimentoEmail) {
            jLabel5.setVisible(true);
            jLabel5.setText("Distributore gia esistente");
        }
    }//GEN-LAST:event_pulsanteConfermaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField email;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField nome;
    private javax.swing.JButton pulsanteConferma;
    // End of variables declaration//GEN-END:variables
}
