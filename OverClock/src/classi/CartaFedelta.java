package classi;

public class CartaFedelta {

    private int punti = 0;
    private int ID;

    public CartaFedelta(int id, int saldo) {
        this.ID = id;
        this.punti = saldo;
    }

    public int getSaldoPunti() {
        return punti;
    }

    public void setPunti(int punti) {
        this.punti = punti;
    }

    public void setID(int id) {
        this.ID = id;
    }

    public int getID() {
        return this.ID;
    }
}
