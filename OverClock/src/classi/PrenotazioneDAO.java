package classi;

import classi.Prenotazione.statoPrenotazione;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class PrenotazioneDAO implements InterfacciaDAO {

    private OverClock overClock = OverClock.getIstanza();
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;
    String data;
    int i = 0;

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = Connessione.getInstance().getConnection();
        return conn;
    }

    public void add(Cliente c, Dispositivo d) {
        DispositivoDAO dispositivoDAO = new DispositivoDAO();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        this.data = dtf.format(now);
        int id = -1;
        for (int i = 0; i < overClock.getIstanzaPrenotazioni().size(); i++) {
            if (i != overClock.getIstanzaPrenotazioni().get(i).getID()) {
                id = i;
                break;
            }
        }
        if (id == -1) {
            id = overClock.getIstanzaPrenotazioni().size();
        }
        String stato = overClock.getPrenotazioneString(statoPrenotazione.ordinato);
        try {
            String queryString = "INSERT INTO prenotazione(id,cliente,dispositivo,data,stato) VALUES(?,?,?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            ptmt.setString(2, c.getEmail());
            ptmt.setString(3, d.getCodice());
            ptmt.setString(4, data);
            ptmt.setString(5, stato);
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
            dispositivoDAO.findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                System.out.print(e);
            } catch (Exception e) {
                System.out.print(e);
            }
        }
    }

    public void update(Prenotazione p, String stato) {
        try {
            String queryString = "UPDATE prenotazione SET stato=? WHERE id=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, stato);
            ptmt.setInt(2, p.getID());
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updateEmail(String mail, String oldMail) {
        try {
            String queryString = "UPDATE prenotazione SET cliente=? WHERE cliente=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, mail);
            ptmt.setString(2, oldMail);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void delete(int id) {
        try {
            String queryString = "DELETE FROM prenotazione WHERE id=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void findAllObservers() {
        try {
            String queryString = "SELECT * FROM prenotazione WHERE stato='Ordinato'";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                Cliente c = overClock.getClienteMail(resultSet.getString("cliente"));
                Dispositivo d = overClock.getDispositivo(resultSet.getString("dispositivo"));
                statoPrenotazione s = overClock.getStatoPrenotazione(resultSet.getString("stato"));
                Prenotazione p = new Prenotazione(resultSet.getInt("id"), c, d, resultSet.getString("data"), s);
                Observer o = p;
                d.addObserver(o);
            }
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }

    }

    public void findAll() {
        List<Prenotazione> l = new ArrayList();
        try {

            String queryString = "SELECT * FROM prenotazione ";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                Cliente c = overClock.getClienteMail(resultSet.getString("cliente"));
                Dispositivo d = overClock.getDispositivo(resultSet.getString("dispositivo"));
                statoPrenotazione s = overClock.getStatoPrenotazione(resultSet.getString("stato"));
                Prenotazione p = new Prenotazione(resultSet.getInt("id"), c, d, resultSet.getString("data"), s);
                l.add(p);
            }

            overClock.setPrenotazioni(l);
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }
}
