package gui;

import classi.Dispositivo;
import classi.OverClock;
import classi.SchedaRiparazione;
import classi.SchedaRiparazioneDAO;
import java.util.List;
import javax.swing.table.DefaultTableModel;

public class VisualizzaRiparazioni extends javax.swing.JPanel {

    private OverClock overClock;
    private SchedaRiparazioneDAO schedaRiparazioneDAO = new SchedaRiparazioneDAO();

    public VisualizzaRiparazioni(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane6 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        ricerca = new javax.swing.JTextField();
        pulsanteRicerca = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "E-Mail", "Numero", "Dispositivo", "Pezzi", "Costo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<SchedaRiparazione> riparazioni = overClock.getRiparazioni("");
        for (int i = riparazioni.size() - 1; i >= 0; i--) {
            if (riparazioni.get(i).getStato() != SchedaRiparazione.statoRiparazioni.riparazioneConclusa) {
                riparazioni.remove(i);
            }
        }

        for (int i = 0; i < riparazioni.size(); i++) {
            float costo = 0;
            String dati = "";
            List<Dispositivo> pezzi = schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID());
            for (int j = 0; j < pezzi.size(); j++) {
                costo = riparazioni.get(i).getCosto();
                dati = dati + schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome() + " x" + schedaRiparazioneDAO.findQuantita(riparazioni.get(i).getID(), schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome()) + "\n";
            }
            dim.addRow(new Object[]{riparazioni.get(i).getCliente().getEmail(), riparazioni.get(i).getCliente().getNumero(), riparazioni.get(i).getDispositivo().getNome(), dati, costo});
        }
        jTable1.getColumnModel().getColumn(3).setCellRenderer(new MultiLineTableCellRenderer());
        jTable1.setDefaultEditor(Object.class, null);
        jScrollPane6.setViewportView(jTable1);

        ricerca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ricercaActionPerformed(evt);
            }
        });

        pulsanteRicerca.setText("Ricerca Tramite E-Mail");
        pulsanteRicerca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteRicercaActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Registro Riparazioni");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 117, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ricerca)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pulsanteRicerca)))
                        .addGap(50, 50, 50))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pulsanteRicerca))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ricercaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ricercaActionPerformed

    }//GEN-LAST:event_ricercaActionPerformed

    private void pulsanteRicercaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteRicercaActionPerformed
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<SchedaRiparazione> riparazioni = overClock.getRiparazioni(ricerca.getText());
        for (int i = riparazioni.size() - 1; i >= 0; i--) {
            if (riparazioni.get(i).getStato() != SchedaRiparazione.statoRiparazioni.riparazioneConclusa) {
                riparazioni.remove(i);
            }
        }

        for (int i = 0; i < riparazioni.size(); i++) {
            float costo = 0;
            String dati = "";
            List<Dispositivo> pezzi = schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID());
            for (int j = 0; j < pezzi.size(); j++) {
                costo = costo + schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getPrezzo() * schedaRiparazioneDAO.findQuantita(riparazioni.get(i).getID(), schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome());
                dati = dati + schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome() + " x" + schedaRiparazioneDAO.findQuantita(riparazioni.get(i).getID(), schedaRiparazioneDAO.findPezzi(riparazioni.get(i).getID()).get(j).getNome()) + "\n";
            }
            dim.addRow(new Object[]{riparazioni.get(i).getCliente().getEmail(), riparazioni.get(i).getCliente().getNumero(), riparazioni.get(i).getDispositivo().getNome(), dati, costo});
        }
        jTable1.getColumnModel().getColumn(3).setCellRenderer(new MultiLineTableCellRenderer());
    }//GEN-LAST:event_pulsanteRicercaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable1;
    private javax.swing.JButton pulsanteRicerca;
    private javax.swing.JTextField ricerca;
    // End of variables declaration//GEN-END:variables
}
