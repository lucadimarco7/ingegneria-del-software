package classi;

public class Promozione {

    private int id;
    private Dispositivo d;
    private int sconto;
    private String data;

    public Promozione(int id, Dispositivo d, int sconto, String data) {
        this.id = id;
        this.d = d;
        this.sconto = sconto;
        this.data = data;
    }

    public int getID() {
        return this.id;
    }

    public Dispositivo getDispostivo() {
        return this.d;
    }

    public int getSconto() {
        return this.sconto;
    }

    public String getData() {
        return this.data;
    }

}
