package classi;

public interface Observer {

    public void update();
}
