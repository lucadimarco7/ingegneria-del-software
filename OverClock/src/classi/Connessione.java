package classi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connessione {

    private String driverClassName = "com.mysql.cj.jdbc.Driver";
    private String connectionUrl = "jdbc:mysql://localhost:3306/overclock";
    private String dbUser = "root";
    private String dbPwd = "";

    private static Connessione connectionFactory = null;

    private Connessione() {
        try {
            Class.forName(driverClassName);
        } catch (ClassNotFoundException e) {
        }
    }

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = DriverManager.getConnection(connectionUrl, dbUser, dbPwd);
        return conn;
    }

    public static Connessione getInstance() {
        if (connectionFactory == null) {
            connectionFactory = new Connessione();
        }
        return connectionFactory;
    }
}
