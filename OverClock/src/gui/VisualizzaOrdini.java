package gui;

import classi.OverClock;
import classi.RigaOrdine;
import classi.SchedaOrdine;
import classi.SchedaOrdineDAO;
import java.util.List;
import javax.swing.table.DefaultTableModel;

public class VisualizzaOrdini extends javax.swing.JPanel {

    private OverClock overClock;
    private SchedaOrdineDAO schedaOrdineDAO = new SchedaOrdineDAO();

    public VisualizzaOrdini(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        ricerca = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Registro Ordini");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Articoli", "Importo", "Data"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<SchedaOrdine> listaOrdini = overClock.visualizzaOrdini("");
        for (int i = 0; i < listaOrdini.size(); i++) {
            String dati="";
            List<RigaOrdine> articoli=schedaOrdineDAO.findArticoli(listaOrdini.get(i).getID());
            for (int j = 0; j < articoli.size(); j++)
            dati = dati + articoli.get(j).getDispositivo().getNome() + " x" + articoli.get(j).getQuantita() + ": " + articoli.get(j).getSubTotale() + "€\n";
            dim.addRow(new Object[]{listaOrdini.get(i).getID(), dati, listaOrdini.get(i).getTotale(), listaOrdini.get(i).getData()});
        }
        jTable1.getColumnModel().getColumn(1).setCellRenderer(new MultiLineTableCellRenderer());
        jScrollPane6.setViewportView(jTable1);

        jButton1.setText("Ricerca Dispositivo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 304, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 429, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ricerca)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)))
                        .addGap(50, 50, 50))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<SchedaOrdine> listaOrdini = overClock.visualizzaOrdini(ricerca.getText());
        for (int i = 0; i < listaOrdini.size(); i++) {
            String dati = "";
            List<RigaOrdine> articoli = schedaOrdineDAO.findArticoli(listaOrdini.get(i).getID());
            for (int j = 0; j < articoli.size(); j++) {
                dati = dati + articoli.get(j).getDispositivo().getNome() + " x" + articoli.get(j).getQuantita() + ": " + articoli.get(j).getSubTotale() + "€\n";
            }
            dim.addRow(new Object[]{listaOrdini.get(i).getID(), dati, listaOrdini.get(i).getTotale(), listaOrdini.get(i).getData()});
        }
        jTable1.getColumnModel().getColumn(1).setCellRenderer(new MultiLineTableCellRenderer());
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField ricerca;
    // End of variables declaration//GEN-END:variables
}
