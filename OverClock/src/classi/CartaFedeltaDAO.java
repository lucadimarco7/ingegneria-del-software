package classi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CartaFedeltaDAO implements InterfacciaDAO {

    private OverClock overClock = OverClock.getIstanza();
    public List<CartaFedelta> l;

    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public CartaFedeltaDAO() {
        this.l = overClock.getIstanzaCarteFedelta();
    }

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = Connessione.getInstance().getConnection();
        return conn;
    }

    public void add(CartaFedelta carta) {
        try {
            String queryString = "INSERT INTO cartafedelta(id,saldo) VALUES(?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, carta.getID());
            ptmt.setInt(2, carta.getSaldoPunti());
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void update(CartaFedelta carta) {
        List<CartaFedelta> l = new ArrayList();
        l = overClock.getListaCarteFedelta();
        int saldo = 0;
        for (int i = 0; i < l.size(); i++) {
            if (carta.getID() == l.get(i).getID()) {
                saldo = l.get(i).getSaldoPunti();
            }
        }
        try {
            String queryString = "UPDATE cartafedelta SET saldo=? WHERE id=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, saldo);
            ptmt.setInt(2, carta.getID());
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void delete(int id) {
        try {
            String queryString = "DELETE FROM cartafedelta WHERE id=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void findAll() {

        List<CartaFedelta> listaCarteFedelta = new ArrayList();
        try {
            String queryString = "SELECT * FROM cartaFedelta";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                CartaFedelta carta = new CartaFedelta(resultSet.getInt("ID"), resultSet.getInt("Saldo"));
                listaCarteFedelta.add(carta);
            }
            overClock.setListaCarte(listaCarteFedelta);
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

}
