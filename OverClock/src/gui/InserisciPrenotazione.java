package gui;

import classi.Cliente;
import classi.Dispositivo;
import classi.OverClock;
import classi.PrenotazioneDAO;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

public class InserisciPrenotazione extends javax.swing.JPanel {

    private OverClock overClock;
    private DefaultTableModel dim;
    private List<Dispositivo> listaDispositivi;
    private Cliente c = null;
    private PrenotazioneDAO prenotazioneDAO = new PrenotazioneDAO();
    private OperazioneRiuscita frameOperazioneRiuscita;

    public InserisciPrenotazione(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        conferma = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        ricerca = new javax.swing.JTextField();
        ricercaDispositivo = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        errori = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cliente = new javax.swing.JTextField();
        associaCliente = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Dispositivo", "Prezzo"
            }
        ));
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        listaDispositivi = overClock.visualizzaListaDispositivi("");
        for (int i = listaDispositivi.size() - 1; i >= 0; i--) {
            if (listaDispositivi.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo||listaDispositivi.get(i).getReperibilita()==false||listaDispositivi.get(i).getQuantita()!=0||
                listaDispositivi.get(i).getNome().contains("comeNuovo")||listaDispositivi.get(i).getNome().contains("danniLeggeri")||listaDispositivi.get(i).getNome().contains("danniEvidenti")) {
                listaDispositivi.remove(i);
            }
        }
        for (int i = 0; i < listaDispositivi.size(); i++) {
            dim.addRow(new Object[]{listaDispositivi.get(i).getNome(), listaDispositivi.get(i).getPrezzo(), listaDispositivi.get(i).getQuantita()});
        }
        jTable1.setDefaultEditor(Object.class, null);
        jScrollPane1.setViewportView(jTable1);

        conferma.setText("Conferma");
        conferma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confermaActionPerformed(evt);
            }
        });

        jLabel6.setText("Nome Dispositivo:");

        ricercaDispositivo.setText("Ricerca Dispositivo");
        ricercaDispositivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ricercaDispositivoActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Nuova Prenotazione");

        errori.setForeground(new java.awt.Color(255, 0, 0));
        errori.setText("Errori");
        errori.setVisible(false);

        jLabel2.setText("Inserisci E-Mail Cliente:");

        associaCliente.setText("Associa Cliente");
        associaCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                associaClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(conferma)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(associaCliente))
                                .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ricerca)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ricercaDispositivo))
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(errori, javax.swing.GroupLayout.Alignment.LEADING))))
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ricercaDispositivo))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(associaCliente))
                        .addGap(18, 18, 18)
                        .addComponent(errori)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(conferma)))
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void confermaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confermaActionPerformed
        if (!errori.isVisible()) {
            if (jTable1.getSelectedRow() != -1 && c != null) {
                Dispositivo d = overClock.getDispositivoByNome(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 0)));
                prenotazioneDAO.add(c, d);
                frameOperazioneRiuscita = new OperazioneRiuscita();
                Timer timer;
                timer = new Timer(1000, (ActionEvent e) -> {
                    frameOperazioneRiuscita.dispose();
                });
                timer.setRepeats(false);
                timer.start();
                frameOperazioneRiuscita.setVisible(true);
                frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                d = null;
                c = null;
                cliente.setText("");
                cliente.setBackground(Color.white);
            } else {
                errori.setText("Seleziona un Articolo/Inserisci E-Mail");
                errori.setVisible(true);
            }
        }
    }//GEN-LAST:event_confermaActionPerformed

    private void ricercaDispositivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ricercaDispositivoActionPerformed
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        listaDispositivi = overClock.visualizzaListaDispositivi(ricerca.getText());
        for (int i = listaDispositivi.size() - 1; i >= 0; i--) {
            if (listaDispositivi.get(i).getTipo() == Dispositivo.tipoArticolo.pezzo || listaDispositivi.get(i).getQuantita() != 0
                    || listaDispositivi.get(i).getNome().contains("comeNuovo") || listaDispositivi.get(i).getNome().contains("danniLeggeri")
                    || listaDispositivi.get(i).getNome().contains("danniEvidenti")
                    || listaDispositivi.get(i).getReperibilita() == false) {
                listaDispositivi.remove(i);
            }
        }
        for (int i = 0; i < listaDispositivi.size(); i++) {
            dim.addRow(new Object[]{listaDispositivi.get(i).getNome(), listaDispositivi.get(i).getPrezzo(), listaDispositivi.get(i).getQuantita()});
        }
    }//GEN-LAST:event_ricercaDispositivoActionPerformed

    private void associaClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_associaClienteActionPerformed
        if (overClock.getClienteMail(cliente.getText()) != null) {
            c = overClock.getClienteMail(cliente.getText());
            cliente.setBackground(new java.awt.Color(204, 255, 153));
            errori.setVisible(false);
        } else {
            cliente.setBackground(new java.awt.Color(255, 153, 102));
            errori.setText("Cliente non registrato");
            errori.setVisible(true);
        }
    }//GEN-LAST:event_associaClienteActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton associaCliente;
    private javax.swing.JTextField cliente;
    private javax.swing.JButton conferma;
    private javax.swing.JLabel errori;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField ricerca;
    private javax.swing.JButton ricercaDispositivo;
    // End of variables declaration//GEN-END:variables
}
