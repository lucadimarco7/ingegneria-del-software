# Progetto Overclock
###### _Autori: Luca Dimarco - Antonino Salemi_
###### _Anno di realizzazione: 2022_

## Contenuto
- La documentazione del progetto è contenuta nella cartella Documentazione.
- Il codice del progetto è contenuto nella cartella OverClock.
- Le librerie che devono essere aggiunte sono contenute nella cartella Librerie.
- Il database da aggiungere OverClock.sql nella Cartella Database.
