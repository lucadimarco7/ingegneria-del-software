package tests.classi;

import classi.CartaFedelta;
import classi.Cliente;
import classi.Dispositivo;
import classi.Distributore;
import classi.Observer;
import classi.Prenotazione;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Test;
import static org.junit.Assert.*;

public class PrenotazioneTest {

    CartaFedelta cf = new CartaFedelta(10, 100);
    Cliente c = new Cliente("nome2", "cognome2", "domanismetto93@gmail.com", "3807662948", cf);
    Distributore distributore = new Distributore(0, "apple", "apple@gmail.it");
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Prenotazione.statoPrenotazione stato = Prenotazione.statoPrenotazione.ordinato;
    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    String data = dtf.format(now);
    Prenotazione p = new Prenotazione(0, c, d, data, stato);
    Observer o = p;

    public PrenotazioneTest() {
        d.addObserver(o);
    }

    @Test
    public void testGetID() {
        int expResult = 0;
        int result = p.getID();
        assertEquals(expResult, result);

    }

    @Test
    public void testGetCliente() {
        Cliente expResult = c;
        Cliente result = p.getCliente();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDispositivo() {
        Dispositivo expResult = d;
        Dispositivo result = p.getDispositivo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetData() {
        String expResult = data;
        String result = p.getData();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetStato() {
        Prenotazione.statoPrenotazione expResult = stato;
        Prenotazione.statoPrenotazione result = p.getStato();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetStato() {
        Prenotazione.statoPrenotazione expResult = Prenotazione.statoPrenotazione.inAttesaDiRitiro;
        p.setStato(Prenotazione.statoPrenotazione.inAttesaDiRitiro);
        Prenotazione.statoPrenotazione result = p.getStato();
        assertEquals(expResult, result);
    }

    @Test
    public void testUpdate() {
        p.update();
    }

}
