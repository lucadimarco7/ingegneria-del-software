package classi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO implements InterfacciaDAO {

    private OverClock overClock = OverClock.getIstanza();
    public List<CartaFedelta> lcf = overClock.getIstanzaCarteFedelta();

    /**
     * Lista Clienti
     */
    public List<Cliente> l;
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public ClienteDAO() {
        this.l = overClock.getIstanzaClienti();
    }

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = Connessione.getInstance().getConnection();
        return conn;
    }

    public void add(Cliente cliente) {
        try {
            String queryString = "INSERT INTO cliente(nome,cognome,email,telefono,IDcarta) VALUES(?,?,?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, cliente.getNome());
            ptmt.setString(2, cliente.getCognome());
            ptmt.setString(3, cliente.getEmail());
            ptmt.setString(4, cliente.getNumero());
            if (cliente.getCartaFedelta() != null) {
                ptmt.setInt(5, cliente.getCartaFedelta().getID());
            } else {
                ptmt.setNull(5, 0);
            }
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updateCarta(Cliente cliente) {
        try {
            String queryString = "UPDATE cliente SET IDcarta=? WHERE telefono=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            if (cliente.getCartaFedelta() != null) {
                ptmt.setInt(1, cliente.getCartaFedelta().getID());
            } else {
                ptmt.setString(1, null);
            }
            ptmt.setString(2, cliente.getNumero());
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updateEmail(String mail, String oldMail) {
        try {
            String queryString = "UPDATE cliente SET email=? WHERE email=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, mail);
            ptmt.setString(2, oldMail);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updateNumero(String numero, String oldNumero) {
        try {
            String queryString = "UPDATE cliente SET telefono=? WHERE telefono=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, numero);
            ptmt.setString(2, oldNumero);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void findAll() {

        List<Cliente> listaClienti = new ArrayList();
        Cliente c;
        try {
            String queryString = "SELECT * FROM cliente";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getObject("IDcarta") == null) {
                    c = new Cliente(resultSet.getString("nome"), resultSet.getString("cognome"),
                            resultSet.getString("email"), resultSet.getString("telefono"));
                } else {
                    c = new Cliente(resultSet.getString("nome"), resultSet.getString("cognome"),
                            resultSet.getString("email"), resultSet.getString("telefono"), overClock.getCartaFedelta(resultSet.getInt("IDcarta")));
                }
                listaClienti.add(c);

            }
            overClock.setListaClienti(listaClienti);
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void delete(String telefono) {
        try {
            String queryString = "DELETE FROM cliente WHERE telefono=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, telefono);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

}
