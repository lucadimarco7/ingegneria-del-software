package classi;

import java.util.ArrayList;
import java.util.List;

public class SchedaOrdine {

    private List<RigaOrdine> righeOrdine;
    private int id;
    private float totale = 0;
    private String data;

    public SchedaOrdine() {
        this.righeOrdine = new ArrayList();
    }

    public SchedaOrdine(int id, float totale, String data) {
        this.id = id;
        this.totale = totale;
        this.data = data;
    }

    public void aggiungiOrdine(Dispositivo d, int quantita) {
        Boolean aggiunto = false;
        for (int i = 0; i < righeOrdine.size(); i++) {
            if (d == righeOrdine.get(i).getDispositivo()) {
                this.totale = this.totale - righeOrdine.get(i).getSubTotale();
                righeOrdine.get(i).setQuantita(righeOrdine.get(i).getQuantita() + quantita);
                this.totale = this.totale + righeOrdine.get(i).getSubTotale();
                aggiunto = true;
            }
        }

        if (!aggiunto) {
            RigaOrdine r = new RigaOrdine(d);
            this.totale = this.totale + r.getSubTotale();
            righeOrdine.add(r);
        }

    }

    public void rimuoviOrdine(Dispositivo d, int quantita) {
        for (int i = 0; i < righeOrdine.size(); i++) {
            if (righeOrdine.get(i).getDispositivo() == d) {
                if (righeOrdine.get(i).getQuantita() > 1) {
                    righeOrdine.get(i).setQuantita(righeOrdine.get(i).getQuantita() - quantita);
                    this.totale = this.totale - righeOrdine.get(i).getDispositivo().getPrezzoAcquisto() * quantita;
                } else {
                    this.totale = this.totale - righeOrdine.get(i).getSubTotale();
                    righeOrdine.remove(i);
                }
            }
        }
    }

    public void rimuoviRigaOrdine(RigaOrdine r) {
        for (int i = 0; i < righeOrdine.size(); i++) {
            if (righeOrdine.get(i) == r) {
                this.totale = this.totale - righeOrdine.get(i).getSubTotale();
                righeOrdine.remove(i);
            }
        }
    }

    public void inviaOrdine() {
        List<String> listaEmail = new ArrayList();
        String listaArticoli = "";
        for (int i = 0; i < righeOrdine.size(); i++) {
            if (!listaEmail.contains(righeOrdine.get(i).getDispositivo().getDistributore().getEmail())) {
                listaEmail.add(righeOrdine.get(i).getDispositivo().getDistributore().getEmail());
            }
        }
        for (int i = 0; i < listaEmail.size(); i++) {
            for (int j = 0; j < righeOrdine.size(); j++) {
                if (righeOrdine.get(j).getDispositivo().getDistributore().getEmail().equals(listaEmail.get(i))) {
                    listaArticoli = listaArticoli + righeOrdine.get(j).getDispositivo().getNome() + " x" + righeOrdine.get(j).getQuantita() + "\n";
                }
            }
            InvioMail.inviaMail("Ordine Articoli", listaArticoli, listaEmail.get(i));
            listaArticoli = "";
        }
    }

    public List<RigaOrdine> getRigheOrdine() {
        return this.righeOrdine;
    }

    public void setRigheOrdine(List<RigaOrdine> l) {
        this.righeOrdine = l;

    }

    public void setData(String data) {
        this.data = data;

    }

    public RigaOrdine getOrdine(Dispositivo d) {
        for (int i = 0; i < righeOrdine.size(); i++) {
            if (righeOrdine.get(i).getDispositivo() == d) {
                return righeOrdine.get(i);
            }
        }
        return null;
    }

    public int getID() {
        return this.id;
    }

    public float getTotale() {
        return this.totale;
    }

    public String getData() {
        return this.data;
    }
}
