package gui;

import classi.CartaFedelta;
import classi.CartaFedeltaDAO;
import classi.Cliente;
import classi.OverClock;
import classi.ClienteDAO;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

public class InserisciCliente extends javax.swing.JPanel {

    ClienteDAO clienteDAO = new ClienteDAO();
    CartaFedeltaDAO cartaDAO = new CartaFedeltaDAO();
    private OverClock overClock;
    private OperazioneRiuscita frameOperazioneRiuscita;

    public InserisciCliente(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        nome = new javax.swing.JTextField();
        cognome = new javax.swing.JTextField();
        email = new javax.swing.JTextField();
        telefono = new javax.swing.JTextField();
        Conferma = new javax.swing.JButton();
        tessera = new javax.swing.JCheckBox();
        jLabel5 = new javax.swing.JLabel();
        erroreLabel = new javax.swing.JLabel();

        jLabel1.setText("Nome");

        jLabel2.setText("Cognome");

        jLabel3.setText("Email");

        jLabel4.setText("Telefono");

        telefono.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        Conferma.setText("Conferma");
        Conferma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConfermaActionPerformed(evt);
            }
        });

        tessera.setText("CartaFedeltà");
        tessera.setActionCommand("CartaFedelta");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel5.setText("Inserisci Cliente");

        erroreLabel.setForeground(new java.awt.Color(255, 0, 0));
        erroreLabel.setText("");
        erroreLabel.setVisible(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tessera, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(erroreLabel)
                                .addGap(18, 18, 18)
                                .addComponent(Conferma))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))
                                .addGap(35, 35, 35)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(telefono)
                                    .addComponent(email)
                                    .addComponent(nome)
                                    .addComponent(cognome))))
                        .addGap(50, 50, 50))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel5)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cognome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(telefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Conferma)
                    .addComponent(tessera)
                    .addComponent(erroreLabel))
                .addContainerGap(30, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ConfermaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConfermaActionPerformed
        boolean emailCorretta = false;
        erroreLabel.setText("");
        boolean telefonoCorretto = false;
        if (nome.getText().equals("")) {
            nome.setBackground(new java.awt.Color(255, 153, 102));
        } else {
            nome.setBackground(Color.white);
        }
        if (cognome.getText().equals("")) {
            cognome.setBackground(new java.awt.Color(255, 153, 102));
        } else {
            cognome.setBackground(Color.white);
        }
        if (email.getText().equals("")) {
            erroreLabel.setText("Riempire Tutti i Campi in Maniera Corretta");
            email.setBackground(new java.awt.Color(255, 153, 102));
        } else {
            try {
                InternetAddress e = new InternetAddress(email.getText());
                e.validate();
                email.setBackground(Color.white);
                emailCorretta = true;
                if (overClock.getClienteMail(email.getText()) != null) {
                    emailCorretta = false;
                    email.setBackground(new java.awt.Color(255, 153, 102));
                    erroreLabel.setText("Email gia esistente");
                }
            } catch (AddressException ex) {
                email.setBackground(new java.awt.Color(255, 153, 102));
                erroreLabel.setText("Riempire Tutti i Campi in Maniera Corretta");
            }
        }
        try {
            Float.parseFloat(telefono.getText());
            if (telefono.getText().equals("") || telefono.getText().length() != 10) {
                telefono.setBackground(new java.awt.Color(255, 153, 102));
                erroreLabel.setText("Riempire Tutti i Campi in Maniera Corretta");
            } else {
                telefono.setBackground(Color.white);
                telefonoCorretto = true;
                if (overClock.getCliente(telefono.getText()) != null) {
                    telefonoCorretto = false;
                    telefono.setBackground(new java.awt.Color(255, 153, 102));
                    erroreLabel.setText("Telefono gia esistente");
                }
            }
        } catch (NumberFormatException e) {
            telefono.setBackground(new java.awt.Color(255, 153, 102));
            erroreLabel.setText("Riempire Tutti i Campi in Maniera Corretta");
        }

        Cliente cliente;
        if (!nome.getText().equals("") && !cognome.getText().equals("") && !email.getText().equals("") && emailCorretta && !telefono.getText().equals("") && telefonoCorretto) {
            if (tessera.isSelected()) {
                int id = overClock.findID();
                CartaFedelta carta = new CartaFedelta(id, 0);
                cartaDAO.add(carta);
                cliente = new Cliente(nome.getText(), cognome.getText(), email.getText(), telefono.getText(), carta);
                clienteDAO.add(cliente);
            } else {
                cliente = new Cliente(nome.getText(), cognome.getText(), email.getText(), telefono.getText(), null);
                clienteDAO.add(cliente);
            }

            frameOperazioneRiuscita = new OperazioneRiuscita();
            Timer timer;
            timer = new Timer(1000, (ActionEvent e) -> {
                frameOperazioneRiuscita.dispose();
            });
            timer.setRepeats(false);
            timer.start();
            frameOperazioneRiuscita.setVisible(true);
            frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
            nome.setText("");
            cognome.setText("");
            email.setText("");
            telefono.setText("");
            erroreLabel.setVisible(false);
        } else {
            erroreLabel.setVisible(true);
        }
    }//GEN-LAST:event_ConfermaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Conferma;
    private javax.swing.JTextField cognome;
    private javax.swing.JTextField email;
    private javax.swing.JLabel erroreLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField nome;
    private javax.swing.JTextField telefono;
    private javax.swing.JCheckBox tessera;
    // End of variables declaration//GEN-END:variables
}
