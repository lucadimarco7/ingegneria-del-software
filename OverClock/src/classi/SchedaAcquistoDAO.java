package classi;

import classi.SchedaAcquisto.Danni;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SchedaAcquistoDAO implements InterfacciaDAO {

    private OverClock overClock = OverClock.getIstanza();
    private String data;
    DispositivoDAO dispositivoDAO = new DispositivoDAO();
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = Connessione.getInstance().getConnection();
        return conn;
    }

    public void add(Cliente cliente, Dispositivo dispositivo, Danni danni) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        this.data = dtf.format(now);
        int id = -1;
        for (int i = 0; i < overClock.getIstanzaAcquisti().size(); i++) {
            if (i != overClock.getIstanzaAcquisti().get(i).getID()) {
                id = i;
                break;
            }
        }
        if (id == -1) {
            id = overClock.getIstanzaAcquisti().size();
        }
        try {
            String queryString = "INSERT INTO acquisto(id,cliente,dispositivo,data) VALUES(?,?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            ptmt.setInt(2, cliente.getCartaFedelta().getID());
            ptmt.setString(3, dispositivo.getCodice() + "-" + danni);
            ptmt.setString(4, data);
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
            dispositivoDAO.findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void findAll() {
        List<SchedaAcquisto> l = new ArrayList();
        try {
            String queryString = "SELECT * FROM acquisto";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                Dispositivo d = overClock.getDispositivo(resultSet.getString("dispositivo"));
                if (resultSet.getString("cliente") == null) {
                    SchedaAcquisto sa = new SchedaAcquisto(resultSet.getInt("id"), d, resultSet.getString("data"));
                    l.add(sa);
                } else {
                    Cliente c = overClock.getClienteCarta(resultSet.getInt("cliente"));
                    SchedaAcquisto sa = new SchedaAcquisto(resultSet.getInt("id"), c, d, resultSet.getString("data"));
                    l.add(sa);
                }
            }
            overClock.setAcquisti(l);
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void setID(int id) {
        try {
            String queryString = "UPDATE acquisto SET cliente=? WHERE cliente=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, null);
            ptmt.setInt(2, id);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

}
