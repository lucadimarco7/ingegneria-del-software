package gui;

import classi.Dispositivo;
import classi.Dispositivo.tipoArticolo;
import classi.DispositivoDAO;
import classi.Distributore;
import classi.OverClock;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

public class InserisciDispositivo extends javax.swing.JPanel {

    private OverClock overClock;
    private OperazioneRiuscita frameOperazioneRiuscita;
    private ConfermaInserimentoDispositivo frameConfermaInserimentoDispositivo;

    public InserisciDispositivo(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        marca = new javax.swing.JTextField();
        prezzo = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        quantita = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        nome = new javax.swing.JTextField();
        codiceModello = new javax.swing.JTextField();
        pulsanteInserisciDispostivo = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        selezionaTipo = new javax.swing.JComboBox<>();
        List<Distributore> l =overClock.RicercaDistributori("");
        String[] distributori=new String[l.size()];
        for(int i=0;i<l.size();i++){
            distributori[i]=l.get(i).getNome();
        }
        selezionaDistributore = new javax.swing.JComboBox<>();
        jLabelErrori = new javax.swing.JLabel();
        prezzoAcquisto = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();

        jLabel4.setText("Prezzo");

        jLabel5.setText("Quantità");

        jLabel1.setText("Nome");

        jLabel2.setText("CodiceModello");

        pulsanteInserisciDispostivo.setText("Conferma");
        pulsanteInserisciDispostivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteInserisciDispostivoActionPerformed(evt);
            }
        });

        jLabel3.setText("Marca");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel6.setText("Inserisci Articolo");

        jLabel7.setText("Tipo");

        jLabel8.setText("Distributore");

        selezionaTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PC", "Tablet", "Smartphone", "Pezzo di Ricambio" }));

        if(distributori!=null)
        selezionaDistributore.setModel(new javax.swing.DefaultComboBoxModel<>(distributori));

        jLabelErrori.setForeground(new java.awt.Color(255, 0, 0));

        prezzoAcquisto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prezzoAcquistoActionPerformed(evt);
            }
        });

        jLabel9.setText("Prezzo Acquisto");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel5))
                                .addGap(35, 35, 35)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(quantita, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(prezzo, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(marca, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(nome, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(codiceModello, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(prezzoAcquisto)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8))
                                .addGap(60, 60, 60)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(selezionaDistributore, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(selezionaTipo, 0, 225, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelErrori, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pulsanteInserisciDispostivo)))
                        .addGap(50, 50, 50))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel6)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(codiceModello, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(nome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(marca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prezzo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prezzoAcquisto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(quantita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(selezionaTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selezionaDistributore, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pulsanteInserisciDispostivo)
                    .addComponent(jLabelErrori, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void pulsanteInserisciDispostivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteInserisciDispostivoActionPerformed
        DispositivoDAO dispositivoDAO = new DispositivoDAO();
        Dispositivo dispositivo;
        tipoArticolo tipo = null;
        Distributore d = null;
        if (nome.getText().equals("")) {
            nome.setBackground(new java.awt.Color(255, 153, 102));
        } else {
            nome.setBackground(Color.white);
        }
        if (codiceModello.getText().equals("")) {
            codiceModello.setBackground(new java.awt.Color(255, 153, 102));
        } else {
            codiceModello.setBackground(Color.white);
        }
        if (marca.getText().equals("")) {
            marca.setBackground(new java.awt.Color(255, 153, 102));
        } else {
            marca.setBackground(Color.white);
        }
        try {
            Float.parseFloat(prezzo.getText());
            if (prezzo.getText().equals("")) {
                prezzo.setBackground(new java.awt.Color(255, 153, 102));
            } else {
                prezzo.setBackground(Color.white);
            }
        } catch (NumberFormatException e) {
            prezzo.setBackground(new java.awt.Color(255, 153, 102));
        }
        try {
            Float.parseFloat(prezzoAcquisto.getText());
            if (prezzoAcquisto.getText().equals("")) {
                prezzoAcquisto.setBackground(new java.awt.Color(255, 153, 102));
            } else {
                prezzoAcquisto.setBackground(Color.white);
            }
        } catch (NumberFormatException e) {
            prezzoAcquisto.setBackground(new java.awt.Color(255, 153, 102));
        }

        try {
            Float.parseFloat(quantita.getText());
            if (quantita.getText().equals("")) {
                quantita.setBackground(new java.awt.Color(255, 153, 102));
            } else {
                quantita.setBackground(Color.white);
            }
        } catch (NumberFormatException e) {
            quantita.setBackground(new java.awt.Color(255, 153, 102));
        }
        switch (selezionaTipo.getSelectedIndex()) {
            case 0 ->
                tipo = tipoArticolo.pc;
            case 1 ->
                tipo = tipoArticolo.tablet;
            case 2 ->
                tipo = tipoArticolo.smartphone;
            case 3 ->
                tipo = tipoArticolo.pezzo;
        }
        if (selezionaDistributore.getSelectedIndex() == -1) {
            jLabelErrori.setText("La lista di distributori è vuota, inserirne uno prima di proseguire");
        } else {
            d = overClock.RicercaDistributori(String.valueOf(selezionaDistributore.getSelectedItem())).get(0);
            try {
                if (!nome.getText().equals("") && !codiceModello.getText().equals("") && !marca.getText().equals("") && !prezzo.getText().equals("") && !prezzoAcquisto.getText().equals("") && !quantita.getText().equals("")) {
                    Dispositivo cercaCodice = overClock.getDispositivo(codiceModello.getText());
                    List<Dispositivo> lista = overClock.getListaDispositiviByNome(nome.getText());
                    if (cercaCodice != null) {
                        frameConfermaInserimentoDispositivo = new ConfermaInserimentoDispositivo(cercaCodice.getNome(), cercaCodice.getQuantita() + Integer.parseInt(quantita.getText()));
                        frameConfermaInserimentoDispositivo.setVisible(true);
                        frameConfermaInserimentoDispositivo.setDefaultCloseOperation(HIDE_ON_CLOSE);
                        lista = null;
                        cercaCodice = null;
                        jLabelErrori.setText("");
                        nome.setText("");
                        codiceModello.setText("");
                        marca.setText("");
                        prezzo.setText("");
                        prezzoAcquisto.setText("");
                        quantita.setText("");
                    } else if (!lista.isEmpty()) {
                        jLabelErrori.setText("Il nome è già associato ad un altro codice modello");
                        jLabelErrori.setVisible(true);
                        lista = null;
                        cercaCodice = null;
                    } else {
                        jLabelErrori.setVisible(false);
                        dispositivo = new Dispositivo(nome.getText(), codiceModello.getText(), marca.getText(), Float.parseFloat(prezzo.getText()), Float.parseFloat(prezzoAcquisto.getText()), Integer.parseInt(quantita.getText()), tipo, d, true);
                        dispositivoDAO.add(dispositivo);
                        frameOperazioneRiuscita = new OperazioneRiuscita();
                        Timer timer;
                        timer = new Timer(1000, (ActionEvent e) -> {
                            frameOperazioneRiuscita.dispose();
                        });
                        timer.setRepeats(false);
                        timer.start();
                        frameOperazioneRiuscita.setVisible(true);
                        frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                        jLabelErrori.setText("");
                        nome.setText("");
                        codiceModello.setText("");
                        marca.setText("");
                        prezzo.setText("");
                        prezzoAcquisto.setText("");
                        quantita.setText("");
                    }
                } else {
                    jLabelErrori.setText("Riempire tutti i campi correttamente");
                }
            } catch (NumberFormatException e) {

            }
        }
    }//GEN-LAST:event_pulsanteInserisciDispostivoActionPerformed

    private void prezzoAcquistoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prezzoAcquistoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_prezzoAcquistoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField codiceModello;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelErrori;
    private javax.swing.JTextField marca;
    private javax.swing.JTextField nome;
    private javax.swing.JTextField prezzo;
    private javax.swing.JTextField prezzoAcquisto;
    private javax.swing.JButton pulsanteInserisciDispostivo;
    private javax.swing.JTextField quantita;
    private javax.swing.JComboBox<String> selezionaDistributore;
    private javax.swing.JComboBox<String> selezionaTipo;
    // End of variables declaration//GEN-END:variables
}
