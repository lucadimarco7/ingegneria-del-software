package tests.classi;

import classi.CartaFedelta;
import classi.Cliente;
import classi.Promozione;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClienteTest {

    Cliente c = new Cliente("nome", "cognome", "email@mail.it", "3807662947", null);
    CartaFedelta cf = new CartaFedelta(10, 100);
    Cliente c2 = new Cliente("nome2", "cognome2", "email2@mail.it", "3807662948", cf);

    public ClienteTest() {
    }

    @Test
    public void testGetNome() {
        String expResult = "nome";
        assertEquals(expResult, c.getNome());
    }

    @Test
    public void setNome() {
        String expResult = "nuovonome";
        c.setNome("nuovonome");
        assertEquals(expResult, c.getNome());
    }

    @Test
    public void getCartaFedelta() {
        CartaFedelta expResult = cf;
        CartaFedelta expResult2 = null;
        assertEquals(expResult, c2.getCartaFedelta());
        assertEquals(expResult2, c.getCartaFedelta());
    }

    @Test
    public void getCognome() {
        String expResult = "cognome";
        assertEquals(expResult, c.getCognome());
    }

    @Test
    public void setCognome() {
        String expResult = "nuovocognome";
        c.setCognome("nuovocognome");
        assertEquals(expResult, c.getCognome());
    }

    @Test
    public void getEmail() {
        String expResult = "email@mail.it";
        assertEquals(expResult, c.getEmail());
    }

    @Test
    public void setEmail() {
        String expResult = "nuovomail@mail.it";
        c.setEmail("nuovomail@mail.it");
        assertEquals(expResult, c.getEmail());
    }

    @Test
    public void getNumero() {
        String expResult = "3807662947";
        assertEquals(expResult, c.getNumero());
    }

    @Test
    public void setNumero() {
        String expResult = "3807666666";
        c.setNumero("3807666666");
        assertEquals(expResult, c.getNumero());
    }

    @Test
    public void setCartaFedelta() {
        CartaFedelta expResult = cf;
        CartaFedelta expResult2 = null;
        c2.setCartaFedelta(null);
        c.setCartaFedelta(cf);
        assertEquals(expResult, c.getCartaFedelta());
        assertEquals(expResult2, c2.getCartaFedelta());
    }
}
