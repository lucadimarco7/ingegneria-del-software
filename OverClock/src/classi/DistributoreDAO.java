package classi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DistributoreDAO implements InterfacciaDAO {

    private OverClock overClock = OverClock.getIstanza();
    public List<Distributore> l = overClock.getIstanzaDistributori();
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public DistributoreDAO() {
    }

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = Connessione.getInstance().getConnection();
        return conn;
    }

    public void add(Distributore d) {
        try {
            String queryString = "INSERT INTO distributore(id,nome,email) VALUES(?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, d.getId());
            ptmt.setString(2, d.getNome());
            ptmt.setString(3, d.getEmail());
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void delete(String mail) {
        try {
            String queryString = "DELETE FROM distributore WHERE email=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, mail);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void update(String nome, int quantita) {

    }

    public void updateNome(String nome, int id) {
        try {
            String queryString = "UPDATE distributore SET nome=? WHERE id=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, nome);
            ptmt.setInt(2, id);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void updateEmail(String mail, int id) {
        try {
            String queryString = "UPDATE distributore SET email=? WHERE id=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, mail);
            ptmt.setInt(2, id);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void findAll() {

        List<Distributore> listaDistributori = new ArrayList();
        try {
            String queryString = "SELECT * FROM distributore";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {

                Distributore d = new Distributore(resultSet.getInt("ID"), resultSet.getString("nome"), resultSet.getString("email"));
                listaDistributori.add(d);
            }
            overClock.setListaDistributori(listaDistributori);

        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }

    }

}
