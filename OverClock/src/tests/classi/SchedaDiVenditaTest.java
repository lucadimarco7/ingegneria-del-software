package tests.classi;

import classi.CartaFedelta;
import classi.Cliente;
import classi.Dispositivo;
import classi.Distributore;
import classi.RigaDiVendita;
import classi.SchedaDiVendita;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class SchedaDiVenditaTest {

    Cliente c = new Cliente("nome", "cognome", "email@mail.it", "3807662947", null);
    CartaFedelta cf = new CartaFedelta(10, 100);
    Cliente c2 = new Cliente("nome2", "cognome2", "email2@mail.it", "3807662948", cf);
    Distributore distributore = new Distributore(0, "apple", "apple@gmail.it");
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    Dispositivo d2 = new Dispositivo("Iphone 12", "IPHONE12", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    Dispositivo d3 = new Dispositivo("Iphone 13", "IPHONE13", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    RigaDiVendita rgv = new RigaDiVendita(d);
    RigaDiVendita rgv2 = new RigaDiVendita(d2);
    RigaDiVendita rgv3 = new RigaDiVendita(d);
    RigaDiVendita rgv4 = new RigaDiVendita(d2);
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    String data = dtf.format(now);
    private List<RigaDiVendita> listaRigheDiVendita = new ArrayList<>();
    SchedaDiVendita sdv;

    public SchedaDiVenditaTest() {
        rgv.setQuantita(3);
        listaRigheDiVendita.add(rgv);
        rgv2.setQuantita(2);
        listaRigheDiVendita.add(rgv2);
        this.sdv = new SchedaDiVendita(0, rgv.getSubTotale() + rgv2.getSubTotale(), data);
        this.sdv.setListaRigheDiVendita(listaRigheDiVendita);
        this.sdv.setCartaFedelta(cf);
    }

    @Test
    public void testAggiungiDispositivo() {

        List<RigaDiVendita> explistaRigheDiVendita = listaRigheDiVendita;
        RigaDiVendita rgv3 = new RigaDiVendita(d3);
        explistaRigheDiVendita.add(rgv3);
        sdv.aggiungiDispositivo(d3);
        SchedaDiVendita expsdv = sdv;
        expsdv.setListaRigheDiVendita(explistaRigheDiVendita);
        assertEquals(expsdv, sdv);
    }

    @Test
    public void testRimuoviDispositivo() {
        SchedaDiVendita expsdv = sdv;
        expsdv.getListaRigheDiVendita().remove(1);
        String nome = "Iphone 12";
        sdv.rimuoviDispositivo(nome);
        sdv.rimuoviDispositivo(nome);
        assertEquals(expsdv, sdv);
    }

    @Test
    public void testSetID() {

        int expResult = 10;

        sdv.setID(10);
        assertEquals(expResult, sdv.getID());

    }

    @Test
    public void testGetListaRigheDiVendita() {

        List<RigaDiVendita> result = sdv.getListaRigheDiVendita();
        assertEquals(listaRigheDiVendita, result);
    }

    @Test
    public void testGetData() {
        String result = sdv.getData();
        assertEquals(data, result);
    }

    @Test
    public void testGetTotale() {
        float expResult = 5 * 699.99F;
        float result = sdv.getTotale();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testSetData() {

        String data = "10/07/1993";
        sdv.setData("10/07/1993");
        assertEquals("10/07/1993", sdv.getData());
    }

    @Test
    public void testSetTotale() {
        float expResult = 10.0F;
        sdv.setTotale(10.0f);
        assertEquals(expResult, sdv.getTotale(), 0.0);
    }

    @Test
    public void testSetListaRigheDiVendita() {
        List<RigaDiVendita> expResult = null;
        sdv.setListaRigheDiVendita(null);
        assertEquals(null, sdv.getListaRigheDiVendita());
    }

    @Test
    public void testGetCartaFedelta() {
        CartaFedelta expResult = cf;
        CartaFedelta result = sdv.getCartaFedelta();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetID() {
        int expResult = 0;
        int result = sdv.getID();
        assertEquals(expResult, result);
    }
}
