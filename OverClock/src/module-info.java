module OverClock {
    requires java.desktop;
    requires java.logging;
    requires java.mail;
    requires activation;
    requires java.sql;
    requires junit;
    requires org.hamcrest;
    exports tests.classi to junit;
}
