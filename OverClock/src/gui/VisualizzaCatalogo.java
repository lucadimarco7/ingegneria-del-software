package gui;

import classi.Cliente;
import classi.Dispositivo;
import classi.Dispositivo.tipoArticolo;
import classi.DispositivoDAO;
import classi.Distributore;
import classi.OverClock;
import classi.Prenotazione;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

public class VisualizzaCatalogo extends javax.swing.JPanel {

    private OverClock overClock;
    private ModificaDispositivo frameModificaDispositivo;
    private OperazioneRiuscita frameOperazioneRiuscita = new OperazioneRiuscita();
    private DefaultTableModel dim;
    private DispositivoDAO dispositivoDAO = new DispositivoDAO();

    public VisualizzaCatalogo(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ricerca = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        filtro = new javax.swing.JComboBox<>();
        Elimina = new javax.swing.JButton();
        errore = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        ricerca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ricercaActionPerformed(evt);
            }
        });

        jButton1.setText("Ricerca Dispositivo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Catalogo");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Dispositivo", "Marca", "Codice", "Prezzo", "Disponibilità"
            }
        ));
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        DefaultTableModel dim=(DefaultTableModel) jTable1.getModel();
        List<Dispositivo> listaDispositivo=overClock.visualizzaListaDispositivi("");
        for (int i = listaDispositivo.size() - 1; i >= 0; i--) {

            if (listaDispositivo.get(i).getReperibilita() == false) {

                listaDispositivo.remove(i);
            }
        }

        for (int i = 0; i < listaDispositivo.size(); i++) {
            if (listaDispositivo.get(i).getTipo() != tipoArticolo.pezzo) {
                dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
            }
        }
        jTable1.setDefaultEditor(Object.class, null);
        jScrollPane6.setViewportView(jTable1);

        jLabel2.setText("Filtra per:");

        filtro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tutti", "Nuovi", "Usati - Come Nuovi", "Usati - Danni Leggeri", "Usati - Danni Evidenti", "Non Reperibili" }));
        filtro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtroActionPerformed(evt);
            }
        });

        Elimina.setBackground(new java.awt.Color(255, 102, 102));
        Elimina.setText("Escludi Dispositivo Selezionato");
        Elimina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminaActionPerformed(evt);
            }
        });

        errore.setVisible(false);

        jButton2.setText("Modifica Dispositivo Selezionato");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 498, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(Elimina, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(filtro, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(errore, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ricerca)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton1)))
                        .addGap(45, 45, 45))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                        .addGap(20, 20, 20))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(5, 5, 5)
                        .addComponent(filtro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(Elimina)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)
                        .addGap(18, 18, 18)
                        .addComponent(errore)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ricercaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ricercaActionPerformed

    }//GEN-LAST:event_ricercaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Dispositivo> listaDispositivo = overClock.visualizzaListaDispositivi(ricerca.getText());
        for (int i = listaDispositivo.size() - 1; i >= 0; i--) {

            if (listaDispositivo.get(i).getReperibilita() == false) {

                listaDispositivo.remove(i);
            }
        }

        for (int i = 0; i < listaDispositivo.size(); i++) {
            if (listaDispositivo.get(i).getTipo() != tipoArticolo.pezzo) {
                dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void filtroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filtroActionPerformed
        String s = overClock.getDanniFormattati(String.valueOf(filtro.getSelectedItem()));
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Dispositivo> listaDispositivo = overClock.visualizzaListaDispositivi(ricerca.getText());
        switch (s) {

            case "nuovo" -> {
                for (int i = listaDispositivo.size() - 1; i >= 0; i--) {

                    if (listaDispositivo.get(i).getNome().contains("danniLeggeri")
                            || listaDispositivo.get(i).getNome().contains("danniEvidenti")
                            || listaDispositivo.get(i).getNome().contains("comeNuovo")
                            || (listaDispositivo.get(i).getReperibilita() == false
                            && listaDispositivo.get(i).getQuantita() == 0)) {
                        listaDispositivo.remove(i);
                    }
                }
                for (int i = 0; i < listaDispositivo.size(); i++) {
                    if (listaDispositivo.get(i).getTipo() != tipoArticolo.pezzo) {
                        dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
                    }
                }
            }

            case "danniLeggeri" -> {

                for (int i = listaDispositivo.size() - 1; i >= 0; i--) {

                    if (!listaDispositivo.get(i).getNome().contains("danniLeggeri")
                            || listaDispositivo.get(i).getQuantita() == 0) {
                        listaDispositivo.remove(i);
                    }
                }

                for (int i = 0; i < listaDispositivo.size(); i++) {
                    if (listaDispositivo.get(i).getTipo() != tipoArticolo.pezzo) {
                        dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
                    }
                }
            }

            case "danniEvidenti" -> {

                for (int i = listaDispositivo.size() - 1; i >= 0; i--) {

                    if (!listaDispositivo.get(i).getNome().contains("danniEvidenti")
                            || listaDispositivo.get(i).getQuantita() == 0) {
                        listaDispositivo.remove(i);
                    }
                }

                for (int i = 0; i < listaDispositivo.size(); i++) {
                    if (listaDispositivo.get(i).getTipo() != tipoArticolo.pezzo) {
                        dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
                    }
                }
            }

            case "comeNuovo" -> {

                for (int i = listaDispositivo.size() - 1; i >= 0; i--) {

                    if (!listaDispositivo.get(i).getNome().contains("comeNuovo")
                            || listaDispositivo.get(i).getQuantita() == 0) {
                        listaDispositivo.remove(i);
                    }
                }

                for (int i = 0; i < listaDispositivo.size(); i++) {
                    if (listaDispositivo.get(i).getTipo() != tipoArticolo.pezzo) {
                        dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
                    }
                }
            }
            case "nonReperibili" -> {
                for (int i = listaDispositivo.size() - 1; i >= 0; i--) {

                    if (listaDispositivo.get(i).getReperibilita() != false) {

                        listaDispositivo.remove(i);
                    }
                }

                for (int i = 0; i < listaDispositivo.size(); i++) {
                    if (listaDispositivo.get(i).getTipo() != tipoArticolo.pezzo) {
                        dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
                    }
                }

            }
            case "Tutti" -> {
                for (int i = listaDispositivo.size() - 1; i >= 0; i--) {

                    if (listaDispositivo.get(i).getReperibilita() == false) {

                        listaDispositivo.remove(i);
                    }
                }

                for (int i = 0; i < listaDispositivo.size(); i++) {
                    if (listaDispositivo.get(i).getTipo() != tipoArticolo.pezzo) {
                        dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
                    }
                }

            }
        }

    }//GEN-LAST:event_filtroActionPerformed

    private void EliminaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminaActionPerformed
        dim = (DefaultTableModel) jTable1.getModel();
        boolean flag = false;
        errore.setVisible(false);
        if (jTable1.getSelectedRow() != -1) {
            String codice = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 2);
            Dispositivo d = overClock.getDispositivo(codice);
            if (d.getQuantita() != 0) {
                errore.setText("Impossibile escludere un dipositivo disponibile in magazzino");
                errore.setVisible(true);
            } else {
                List<Prenotazione> l = overClock.visualizzaPrenotazioni("");
                for (int i = 0; i < l.size(); i++) {
                    if (l.get(i).getDispositivo().getCodice().equals(codice)) {
                        errore.setText("Impossibile escludere dal catalogo un dipositivo con preordini associati");
                        errore.setVisible(true);
                        flag = true;
                    }
                }
                if (flag == false) {
                    dispositivoDAO.updateReperibilita((String) jTable1.getValueAt(jTable1.getSelectedRow(), 2));
                    dim.removeRow(jTable1.getSelectedRow());
                    frameOperazioneRiuscita = new OperazioneRiuscita();
                    Timer timer;
                    timer = new Timer(1000, (ActionEvent e) -> {
                        frameOperazioneRiuscita.dispose();
                    });
                    timer.setRepeats(false);
                    timer.start();
                    frameOperazioneRiuscita.setVisible(true);
                    frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
                }
            }
        }

    }//GEN-LAST:event_EliminaActionPerformed
    public void update() {
        String s = overClock.getDanniFormattati(String.valueOf(filtro.getSelectedItem()));

        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Dispositivo> listaDispositivo = overClock.visualizzaListaDispositivi(ricerca.getText());
        for (int i = listaDispositivo.size() - 1; i >= 0; i--) {

            if (listaDispositivo.get(i).getReperibilita() == false) {

                listaDispositivo.remove(i);
            }
        }

        for (int i = 0; i < listaDispositivo.size(); i++) {
            if (listaDispositivo.get(i).getTipo() != tipoArticolo.pezzo) {
                dim.addRow(new Object[]{listaDispositivo.get(i).getNome(), listaDispositivo.get(i).getMarca(), listaDispositivo.get(i).getCodice(), listaDispositivo.get(i).getPrezzo(), listaDispositivo.get(i).getQuantita()});
            }
        }

    }
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (jTable1.getSelectedRow() != -1) {
            String codice = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 2);
            Dispositivo d = overClock.getDispositivo(codice);
            frameModificaDispositivo = new ModificaDispositivo(overClock, d, this);
            frameModificaDispositivo.setVisible(true);
            frameModificaDispositivo.setDefaultCloseOperation(HIDE_ON_CLOSE);
            frameModificaDispositivo.pack();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Elimina;
    private javax.swing.JLabel errore;
    private javax.swing.JComboBox<String> filtro;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField ricerca;
    // End of variables declaration//GEN-END:variables
}
