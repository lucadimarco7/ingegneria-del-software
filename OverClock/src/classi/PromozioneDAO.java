package classi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class PromozioneDAO implements InterfacciaDAO {

    private OverClock overClock = OverClock.getIstanza();
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;
    String data;

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = Connessione.getInstance().getConnection();
        return conn;
    }

    public void add(Dispositivo d, int sconto) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        this.data = dtf.format(now);
        int id = -1;
        for (int i = 0; i < overClock.getIstanzaPromozioni().size(); i++) {
            if (i != overClock.getIstanzaPromozioni().get(i).getID()) {
                id = i;
                break;
            }
        }
        if (id == -1) {
            id = overClock.getIstanzaPromozioni().size();
        }
        try {
            String queryString = "INSERT INTO promozione(id,dispositivo,sconto,data) VALUES(?,?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            ptmt.setString(2, d.getCodice());
            ptmt.setInt(3, sconto);
            ptmt.setString(4, data);
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void delete(Promozione p) {
        try {
            String queryString = "DELETE FROM promozione WHERE id=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, p.getID());
            ptmt.executeUpdate();
            p.getDispostivo().setPrezzo((p.getDispostivo().getPrezzo() * 100) / (100 - p.getSconto()));
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public void findAll() {
        List<Promozione> l = new ArrayList();
        try {
            String queryString = "SELECT * FROM promozione";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                Dispositivo d = overClock.getDispositivo(resultSet.getString("dispositivo"));
                Promozione p = new Promozione(resultSet.getInt("id"), d,
                        resultSet.getInt("sconto"), resultSet.getString("data"));
                d.setPrezzo(d.getPrezzo() - d.getPrezzo() / 100 * resultSet.getInt("sconto"));
                l.add(p);
            }
            overClock.setPromozioni(l);
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }
}
