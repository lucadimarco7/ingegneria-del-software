package classi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class SchedaDiVenditaDAO implements InterfacciaDAO {

    private OverClock overClock = OverClock.getIstanza();
    private String data;
    Connection connection = null;
    PreparedStatement ptmt = null;
    ResultSet resultSet = null;

    public SchedaDiVenditaDAO() {
    }

    public Connection getConnection() throws SQLException {
        Connection conn;
        conn = Connessione.getInstance().getConnection();
        return conn;
    }

    public void add(List<RigaDiVendita> l, int idCarta, float totale) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        this.data = dtf.format(now);
        int id = -1;
        for (int i = 0; i < overClock.getIstanzaVendite().size(); i++) {
            if (i != overClock.getIstanzaVendite().get(i).getID()) {
                id = i;
                break;
            }
        }
        if (id == -1) {
            id = overClock.getIstanzaVendite().size();
        }
        try {
            String queryString = "INSERT INTO vendita(id,totale,carta,data) VALUES(?,?,?,?)";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            ptmt.setFloat(2, totale);
            ptmt.setInt(3, idCarta);
            ptmt.setString(4, data);
            ptmt.executeUpdate();
            System.out.println("Data Added Successfully");
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
        for (int i = 0; i < l.size(); i++) {
            try {
                String queryString = "INSERT INTO articoliVenduti(idVendita,nome,quantita,subTotale) VALUES(?,?,?,?)";
                connection = getConnection();
                ptmt = connection.prepareStatement(queryString);
                ptmt.setInt(1, id);
                ptmt.setString(2, l.get(i).getDispositivo().getNome());
                ptmt.setInt(3, l.get(i).getQuantita());
                ptmt.setFloat(4, l.get(i).getSubTotale());
                ptmt.executeUpdate();
                System.out.println("Data Added Successfully");
                findAll();
            } catch (SQLException e) {
            } finally {
                try {
                    if (ptmt != null) {
                        ptmt.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                } catch (Exception e) {
                }
            }
        }
    }

    public void update(String nome, int quantita) {

    }

    public void findAll() {
        List<SchedaDiVendita> l = new ArrayList();
        try {
            String queryString = "SELECT * FROM vendita";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                if (resultSet.getString("carta") == null) {
                    SchedaDiVendita sdv = new SchedaDiVendita(resultSet.getInt("id"), resultSet.getFloat("totale"),
                            resultSet.getString("data"));
                    l.add(sdv);
                } else {
                    SchedaDiVendita sdv = new SchedaDiVendita(resultSet.getInt("id"), resultSet.getFloat("totale"),
                            resultSet.getInt("carta"), resultSet.getString("data"));
                    l.add(sdv);
                }
            }
            overClock.setVendite(l);
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    public List<RigaDiVendita> findArticoli(int id) {
        List<RigaDiVendita> l = new ArrayList();
        try {
            String queryString = "SELECT * FROM articolivenduti WHERE idVendita=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setInt(1, id);
            resultSet = ptmt.executeQuery();
            while (resultSet.next()) {
                Dispositivo d = overClock.getDispositivoByNome(resultSet.getString("nome"));
                RigaDiVendita rdv = new RigaDiVendita(d, resultSet.getFloat("subTotale"));
                l.add(rdv);
            }
            return l;
        } catch (SQLException e) {
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
        return null;
    }

    public void setID(int id) {
        try {
            String queryString = "UPDATE vendita SET carta=? WHERE carta=?";
            connection = getConnection();
            ptmt = connection.prepareStatement(queryString);
            ptmt.setString(1, null);
            ptmt.setInt(2, id);
            ptmt.executeUpdate();
            findAll();
        } catch (SQLException e) {
        } finally {
            try {
                if (ptmt != null) {
                    ptmt.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }
}
