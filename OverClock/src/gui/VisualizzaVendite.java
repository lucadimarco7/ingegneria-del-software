package gui;

import classi.OverClock;
import classi.RigaDiVendita;
import classi.SchedaDiVendita;
import classi.SchedaDiVenditaDAO;
import java.util.List;
import java.util.ListIterator;
import javax.swing.table.DefaultTableModel;

public class VisualizzaVendite extends javax.swing.JPanel {

    private OverClock overClock;
    private SchedaDiVenditaDAO schedaDiVenditaDAO = new SchedaDiVenditaDAO();

    public VisualizzaVendite(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ricerca = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        ricerca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ricercaActionPerformed(evt);
            }
        });

        jButton1.setText("Ricerca Dispositivo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Registro Vendite");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Articoli", "Importo e Sconti", "Carta Fedeltà", "Data"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        DefaultTableModel dim=(DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<SchedaDiVendita> listaVendite=overClock.visualizzaVendite("");
        String idCarta="";
        ListIterator venditeIterator=listaVendite.listIterator();
        for(int i=0;i<listaVendite.size();i++){
            String dati="";
            List<RigaDiVendita> articoli=schedaDiVenditaDAO.findArticoli(listaVendite.get(i).getID());
            for (int j = 0; j < articoli.size(); j++) {
                dati = dati + articoli.get(j).getDispositivo().getNome() + " x" + articoli.get(j).getQuantita()+" = "+articoli.get(j).getSubTotale()+";  ";
            }
            if(listaVendite.get(i).getCartaFedelta()!=null)
            idCarta=String.valueOf(listaVendite.get(i).getCartaFedelta().getID());
            else
            idCarta="Nessuna";
            dim.addRow(new Object[]{listaVendite.get(i).getID(),dati,listaVendite.get(i).getTotale(),idCarta,listaVendite.get(i).getData()});
        }
        jTable1.setDefaultEditor(Object.class, null);
        jTable1.getColumnModel().getColumn(1).setCellRenderer(new MultiLineTableCellRenderer());
        jScrollPane6.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 421, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 562, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ricerca)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)))
                        .addGap(50, 50, 50))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 348, Short.MAX_VALUE)
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void ricercaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ricercaActionPerformed

    }//GEN-LAST:event_ricercaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        String idCarta;
        List<SchedaDiVendita> listaVendite = overClock.visualizzaVendite(ricerca.getText());
        for (int i = 0; i < listaVendite.size(); i++) {
            String dati = "";
            List<RigaDiVendita> articoli = schedaDiVenditaDAO.findArticoli(listaVendite.get(i).getID());
            for (int j = 0; j < articoli.size(); j++) {
                dati = dati + articoli.get(j).getDispositivo().getNome() + " x" + articoli.get(j).getQuantita() + " = " + articoli.get(j).getSubTotale();
            }
            if (listaVendite.get(i).getCartaFedelta() != null) {
                idCarta = String.valueOf(listaVendite.get(i).getCartaFedelta().getID());
            } else {
                idCarta = "Nessuna";
            }
            dim.addRow(new Object[]{listaVendite.get(i).getID(), dati, listaVendite.get(i).getTotale(), idCarta, listaVendite.get(i).getData()});
        }
        jTable1.getColumnModel().getColumn(1).setCellRenderer(new MultiLineTableCellRenderer());
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField ricerca;
    // End of variables declaration//GEN-END:variables
}
