package gui;

import classi.Cliente;
import classi.Dispositivo;
import classi.DispositivoDAO;
import classi.Distributore;
import classi.DistributoreDAO;
import classi.OverClock;
import classi.Prenotazione;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;

public class VisualizzaDistributori extends javax.swing.JPanel {

    private OverClock overClock;
    private DefaultTableModel dim;
    private frameModificaDistributore frameModificaDistributore;
    private DistributoreDAO distributoreDAO = new DistributoreDAO();
    private DispositivoDAO dispositivoDAO = new DispositivoDAO();
    private OperazioneRiuscita frameOperazioneRiuscita;

    public VisualizzaDistributori(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        ricerca = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jButton2 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Distributori");

        jButton1.setText("Ricerca Distributore");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID", "Nome", "E-Mail"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        DefaultTableModel dim=(DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Distributore> distributori=overClock.RicercaDistributori("");
        for(int i=0;i<distributori.size();i++){
            dim.addRow(new Object[]{distributori.get(i).getId(),distributori.get(i).getNome(),distributori.get(i).getEmail()});
        }
        jScrollPane1.setViewportView(jTable1);

        jButton2.setBackground(new java.awt.Color(255, 102, 102));
        jButton2.setForeground(new java.awt.Color(0, 0, 0));
        jButton2.setText("Elimina Distribuotre Selezionato");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminaActionPerformed(evt);
            }
        });

        jButton4.setText("Modifica Distributore Selezionato");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton4)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ricerca)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)))
                        .addGap(50, 50, 50))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ricerca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        DefaultTableModel dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Distributore> distributori = overClock.RicercaDistributori(ricerca.getText());
        for (int i = 0; i < distributori.size(); i++) {
            dim.addRow(new Object[]{distributori.get(i).getId(), distributori.get(i).getNome(), distributori.get(i).getEmail()});
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void eliminaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminaActionPerformed
        dim = (DefaultTableModel) jTable1.getModel();
        if (jTable1.getSelectedRow() != -1) {
            String mail = (String) jTable1.getValueAt(jTable1.getSelectedRow(), 2);
            Distributore d = overClock.getDistributore((int) jTable1.getValueAt(jTable1.getSelectedRow(), 0));
            List<Dispositivo> l = overClock.dispositiviByIdDistributore(d.getId());
            if (l != null) {
                for (int i = 0; i < l.size(); i++) {
                    dispositivoDAO.updateDistributore(d.getId());
                }
            }
            distributoreDAO.delete(mail);
            dim.removeRow(jTable1.getSelectedRow());
            frameOperazioneRiuscita = new OperazioneRiuscita();
            Timer timer;
            timer = new Timer(1000, (ActionEvent e) -> {
                frameOperazioneRiuscita.dispose();
            });
            timer.setRepeats(false);
            timer.start();
            frameOperazioneRiuscita.setVisible(true);
            frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);

        }
    }//GEN-LAST:event_eliminaActionPerformed

    private void modificaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificaActionPerformed
        if (jTable1.getSelectedRow() != -1) {
            int id = (int) jTable1.getValueAt(jTable1.getSelectedRow(), 0);
            Distributore distributore = overClock.getDistributore(id);
            frameModificaDistributore = new frameModificaDistributore(overClock, distributore, this);
            frameModificaDistributore.setVisible(true);
            frameModificaDistributore.setDefaultCloseOperation(HIDE_ON_CLOSE);
            frameModificaDistributore.pack();
        }
    }//GEN-LAST:event_modificaActionPerformed

    public void update() {
        dim = (DefaultTableModel) jTable1.getModel();
        dim.setRowCount(0);
        List<Distributore> ld = overClock.getListaDistributori();
        for (int i = 0; i < ld.size(); i++) {
            dim.addRow(new Object[]{ld.get(i).getId(), ld.get(i).getNome(),
                ld.get(i).getEmail()});
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField ricerca;
    // End of variables declaration//GEN-END:variables
}
