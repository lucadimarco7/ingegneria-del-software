package tests.classi;

import classi.Dispositivo;
import classi.Distributore;
import classi.RigaDiVendita;
import org.junit.Test;
import static org.junit.Assert.*;

public class RigaDiVenditaTest {

    Distributore distributore = new Distributore(0, "apple", "apple@gmail.it");
    Dispositivo.tipoArticolo tipo = Dispositivo.tipoArticolo.smartphone;
    Dispositivo d = new Dispositivo("Iphone 11", "IPHONE11", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    Dispositivo d2 = new Dispositivo("Iphone 12", "IPHONE12", "apple", (float) 699.99, 500, 100, tipo, distributore, true);
    RigaDiVendita rgv = new RigaDiVendita(d);

    public RigaDiVenditaTest() {
        rgv.setQuantita(3);
    }

    @Test
    public void testGetSubTotale() {

        float expResult = 3 * 699.99f;
        float result = rgv.getSubTotale();
        assertEquals(expResult, result, 0.0);
    }

    @Test
    public void testGetQuantita() {
        int expResult = 3;
        int result = rgv.getQuantita();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDispositivo() {

        Dispositivo expResult = d;
        Dispositivo result = rgv.getDispositivo();
        assertEquals(expResult, result);

    }

    @Test
    public void testSetDispositivo() {
        System.out.println("setDispositivo");
        Dispositivo expResult = d2;
        rgv.setDispositivo(d2);
        Dispositivo result = rgv.getDispositivo();
        assertEquals(expResult, result);
    }

}
