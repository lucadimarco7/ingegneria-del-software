package gui;

import classi.Cliente;
import classi.Dispositivo;
import classi.Dispositivo.tipoArticolo;
import classi.OverClock;
import classi.SchedaRiparazioneDAO;
import java.awt.event.ActionEvent;
import javax.swing.Timer;
import static javax.swing.WindowConstants.HIDE_ON_CLOSE;

public class NuovoPreventivo extends javax.swing.JPanel {

    private OverClock overClock;
    private SchedaRiparazioneDAO schedaRiparazioneDAO = new SchedaRiparazioneDAO();
    private Cliente c = null;
    private Dispositivo d = null;
    private Boolean erroriCliente = true;
    private Boolean erroriDispositivo = true;
    private OperazioneRiuscita frameOperazioneRiuscita;

    public NuovoPreventivo(OverClock overClock) {
        this.overClock = overClock;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cliente = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        dispositivo = new javax.swing.JTextField();
        pulsanteConferma = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("Nuovo Preventivo");

        jLabel2.setText("E-Mail o Numero Cliente");

        jLabel3.setText("Codice Modello");

        pulsanteConferma.setText("Conferma");
        pulsanteConferma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulsanteConfermaActionPerformed(evt);
            }
        });

        jLabel4.setForeground(new java.awt.Color(255, 0, 0));
        jLabel4.setText("jLabel4");
        jLabel4.setVisible(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addGap(35, 35, 35)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cliente, javax.swing.GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
                            .addComponent(dispositivo)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pulsanteConferma)))
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(dispositivo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pulsanteConferma)
                    .addComponent(jLabel4))
                .addContainerGap(14, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void pulsanteConfermaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulsanteConfermaActionPerformed
        if (overClock.getClienteMail(cliente.getText()) != null) {
            c = overClock.getClienteMail(cliente.getText());
            cliente.setBackground(new java.awt.Color(204, 255, 153));
            erroriCliente = false;
        } else {
            c = null;
            cliente.setBackground(new java.awt.Color(255, 153, 102));
            jLabel4.setText("Cliente non registrato, registrare prima di proseguire");
            erroriCliente = true;
        }
        if (overClock.getDispositivo(dispositivo.getText()) != null
                && overClock.getDispositivo(dispositivo.getText()).getTipo() != tipoArticolo.pezzo) {
            d = overClock.getDispositivo(dispositivo.getText());
            dispositivo.setBackground(new java.awt.Color(204, 255, 153));
            erroriDispositivo = false;
        } else {
            d = null;
            dispositivo.setBackground(new java.awt.Color(255, 153, 102));
            jLabel4.setText("Dispositivo non presente nel catalogo");
            erroriDispositivo = true;
        }
        if (c == null && d == null) {
            jLabel4.setText("Riempire i campi correttamente");
        }
        if (!erroriDispositivo && !erroriCliente) {
            schedaRiparazioneDAO.add(c, d);
            cliente.setBackground(java.awt.Color.white);
            dispositivo.setBackground(java.awt.Color.white);
            frameOperazioneRiuscita = new OperazioneRiuscita();
            Timer timer;
            timer = new Timer(1000, (ActionEvent e) -> {
                frameOperazioneRiuscita.dispose();
            });
            timer.setRepeats(false);
            timer.start();
            frameOperazioneRiuscita.setVisible(true);
            frameOperazioneRiuscita.setDefaultCloseOperation(HIDE_ON_CLOSE);
            cliente.setText("");
            dispositivo.setText("");
            jLabel4.setVisible(false);
            c = null;
            d = null;
        } else {
            jLabel4.setVisible(true);
        }
    }//GEN-LAST:event_pulsanteConfermaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField cliente;
    private javax.swing.JTextField dispositivo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JButton pulsanteConferma;
    // End of variables declaration//GEN-END:variables
}
